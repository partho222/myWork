# -*- coding: utf-8 -*-
import base64


def file_to_b64_byte(file_object):
    """Convert file object to Base64 encoded byte string
    :param file_object: object
    :return: byte string
    """
    file_byte = file_object.read()
    return base64.b64encode(file_byte)

# -*- coding: utf-8 -*-
import math


def get_distance_on_earth(lat1, lng1, lat2, lng2):
    '''
    Take two latitude & longitude and calculate distance between them in Meter(M) on earth
    Using 'Haversine formula' https://en.wikipedia.org/wiki/Haversine_formula#Formulation
    :param lat1: float
    :param lng1: float
    :param lat2: float
    :param lng2: float
    :return: float
    '''
    EarthRadius = 6378137   # earth radius (R) in meter
    Dlat = math.radians(lat2 - lat1)
    Dlng = math.radians(lng2 - lng1)
    a = math.sin(Dlat/2) * math.sin(Dlat/2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(Dlng/2) * math.sin(Dlng/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = EarthRadius * c
    return d

/**
 * Created by tariq on 11/29/2016.
 */

var localNodeJsPath = '../../../../../lib/nodeJS/node_modules';
var using = require(localNodeJsPath + '/jasmine-data-provider');

var value = require('../data/StaticValues.js');
var domSelectors = require('../selector_constrain/CommonDOMSelector.js');
var testData = require('../data/TestDataSet.js');

var helper = require('../helper/ProtractorHelper.js');
var service = require('../service/CommonService.js');

var helperInstance = new helper();
var serviceInstance = new service();

describe('Trade.IX BDD Testing', function () {
    using(testData['bidCreateTestData'], function (data) {
        describe('Bid Create Test', function () {
            it('Browse URL', function () {
                helperInstance.browseURL('/#/login');
                expect(helperInstance.findAndGetText(domSelectors.INDEX_PAGE_DETERMINER), '').toBe('Login');
            });

            it('Login as Supplier using - "' + data.username + '", "' + data.password + '"', function () {
                helperInstance.findAndSetValue(domSelectors.LOGIN_PAGE_USERNAME_FIELD, 'model', data.username);
                helperInstance.findAndSetValue(domSelectors.LOGIN_PAGE_PASSWORD_FIELD, 'model', data.password);
                helperInstance.findAndClick(domSelectors.LOGIN_BUTTON);
                expect(helperInstance.findAndGetText(domSelectors.CURRENT_WINDOW_NAME_FIELD, '')).toBe(data.name);
            });

            it('Create Offer Test', function () {
                helperInstance.findAndClick(domSelectors.CREATE_OFFER_BUTTON);
                expect(helperInstance.findAndGetText(domSelectors.CREATE_OFFER_PAGE_DETERMINER)).toBe(value.CREATE_OFFER_MESSAGE);

                var randomCCY = serviceInstance.getRandomValue(value.ccyValues);
                helperInstance.findAndClick(domSelectors.CCY_DROPDOWN, 'model');
                serviceInstance.dropDownSelector(domSelectors.CCY_DROPDOWN_VALUE, '', randomCCY);
                helperInstance.findAndClick(domSelectors.CCY_OK_BUTTON);

                serviceInstance.getValueAndSelect(domSelectors.BUYER_LIST_SELECTOR, 'model', data.NO_OF_BUYERS);
                expect(helperInstance.findAndGetText(domSelectors.TOTAL_NO_OF_BUYERS, 'xpath')).toBe(String(data.NO_OF_BUYERS));
                helperInstance.findAndClick(domSelectors.BUYER_NEXT_BUTTON, 'xpath');

                // work and expectation to do
                serviceInstance.getValueAndSelect(domSelectors.INVOICE_LIST_SELECTOR, '', data.NO_OF_INVOICES);
                expect(helperInstance.findAndGetText(domSelectors.TOTAL_NO_OF_INVOICES, 'xpath')).toBe(String(data.NO_OF_INVOICES));
                helperInstance.findAndClick(domSelectors.INVOICE_NEXT_BUTTON, 'xpath');

                serviceInstance.getValueAndSelect(domSelectors.FUNDER_LIST_SELECTOR, 'model', data.NO_OF_FUNDERS);
                expect(helperInstance.findAndGetText(domSelectors.TOTAL_NO_OF_FUNDERS, 'xpath')).toBe(String(data.NO_OF_FUNDERS));
                helperInstance.findAndClick(domSelectors.FUNDER_OFFER_VIEW_BUTTON, 'xpath');

                helperInstance.resetInputField(domSelectors.OFFER_VALIDITY_BOX, 'model');
                helperInstance.findAndSetValue(domSelectors.OFFER_VALIDITY_BOX, 'model', data.HOURS_OFFER_VALID);
                helperInstance.findAndClick(domSelectors.OFFER_CREATE_BUTTON);
                helperInstance.wait(helperInstance.find(domSelectors.CONFIRMATION_BOX));
                expect(helperInstance.findAndGetText(domSelectors.CONFIRMATION_BOX)).toBe(value.OFFER_CREATE_SUCCESS_MESSAGE);
            });
        });
    });
});
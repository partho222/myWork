var localNodeJsPath = '../../../../../lib/nodeJS/node_modules';
var using = require(localNodeJsPath + '/jasmine-data-provider');

var testData = require('../data/TestDataSet.js');
var domSelectors = require('../selector_constrain/CommonDOMSelector.js');
var helper = require('../helper/ProtractorHelper.js');

var helperInstance = new helper();

describe('Trade.IX BDD Testing', function () {
    using(testData['userTestData'], function (data) {
        describe('User Login-Logout Test', function () {
            it('Browse URL', function () {
                helperInstance.browseURL('/#/login');
                expect(helperInstance.findAndGetText(domSelectors.INDEX_PAGE_DETERMINER), '').toBe('Login');
            });

            it('User Login Test using - "' + data.username + '", "' + data.password + '"', function () {
                helperInstance.findAndSetValue(domSelectors.LOGIN_PAGE_USERNAME_FIELD, 'model', data.username);
                helperInstance.findAndSetValue(domSelectors.LOGIN_PAGE_PASSWORD_FIELD, 'model', data.password);
                helperInstance.findAndClick(domSelectors.LOGIN_BUTTON);
                expect(helperInstance.findAndGetText(domSelectors.CURRENT_WINDOW_NAME_FIELD, '')).toBe(data.name);
            });

            it('Logout From Current User', function () {
                helperInstance.findAndClick(domSelectors.DROPDOWN_MENU);
                helperInstance.findAndClick(domSelectors.LOGOUT_BUTTON);
                expect(helperInstance.findAndGetText(domSelectors.INDEX_PAGE_DETERMINER)).toBe('Login');
            });
        });
    });
});
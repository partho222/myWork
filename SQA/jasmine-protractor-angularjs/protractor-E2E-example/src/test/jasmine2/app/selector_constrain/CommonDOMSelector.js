/**
 * Created by tariq on 11/28/2016.
 */

'use strict';

var data = {
    LOGIN_PAGE_USERNAME_FIELD: 'loginData.userName',
    LOGIN_PAGE_PASSWORD_FIELD: 'loginData.password',
    LOGIN_BUTTON: '[data-ng-click="login()"]',
    CURRENT_WINDOW_NAME_FIELD: '.top_heading .ng-binding',
    INDEX_PAGE_DETERMINER: '.login_container .form-login-heading',
    LOGOUT_BUTTON: '[ng-click="logout()"]',
    DROPDOWN_MENU: '.dropdown-toggle',
    CREATE_OFFER_BUTTON: '.button.create_offer_button',
    CREATE_OFFER_PAGE_DETERMINER: '.select_currency > h3',
    BUYER_LIST_SELECTOR: 'buyerSelected',
    INVOICE_LIST_SELECTOR: '[ng-click="updateInvoiceSelection(chosenBuyer)"]',
    FUNDER_LIST_SELECTOR: 'FunderSelected',
    CCY_DROPDOWN: 'currency',
    CCY_DROPDOWN_VALUE: '[ng-model="currency"] > option',
    CCY_OK_BUTTON: '[ng-click="vm.ok(currency)"]',
    BUYER_NEXT_BUTTON: "//button[contains(@ng-click,'displayInvoices()')][text()='Next']",
    INVOICE_NEXT_BUTTON: "//button[contains(@ng-click,'displayFunders()')][text()='Next']",
    FUNDER_OFFER_VIEW_BUTTON: "//button[contains(@ng-click,'createOffer()')][text()='View Offer']",
    OFFER_VALIDITY_BOX: 'vm.bidValidFor',
    OFFER_CREATE_BUTTON: '[ng-click="vm.createOffer(offerDetails)"]',
    INVOICE_DATA_TABLE: '.invoice_selection .data_table.row_selectable_table',
    CONFIRMATION_BOX: '.modal-content .modal-body .message_heading',
    TOTAL_NO_OF_BUYERS: "//div[contains(@selectedbuyers,'vm.selectedBuyers')]/p[1]",
    TOTAL_NO_OF_INVOICES: "//div[contains(@selectedinvoices,'vm.selectedInvoices')]/p[1]",
    TOTAL_NO_OF_FUNDERS: "//div[contains(@selectedfunders,'vm.selectedFunders')]/p[1]"
};

module.exports = data;
/**
 * Created by tariq on 11/28/2016.
 */
'use strict';

var data = function () {
    var self = this;

    self.find = function (elementName, elementType) {
        if (elementType === 'model') {
            return element(by.model(elementName));
        } else if (elementType === 'xpath') {
            return element(by.xpath(elementName));
        } else {
            return element(by.css(elementName));
        }
    };

    self.findAll = function (elementName, elementType) {
        if (elementType === 'model') {
            return element.all(by.model(elementName));
        } else if (elementType === 'xpath') {
            return element.all(by.xpath(elementName));
        } else {
            return element.all(by.css(elementName));
        }
    };

    self.findAndSetValue = function (elementName, elementType, value) {
        return self.find(elementName, elementType).sendKeys(value);
    };

    self.findAndClick = function (elementName, elementType) {
        return self.find(elementName, elementType).click();
    };

    self.findAndGetText = function (elementName, elementType) {
        return self.find(elementName, elementType).getText();
    };

    self.browseURL = function (url) {
        browser.get(url);
    };

    self.resetInputField = function (elementName, elementType) {
        var object = self.find(elementName, elementType);
        object.clear();
    };

    self.wait = function (element) {
        var until = protractor.ExpectedConditions;
        browser.wait(until.visibilityOf(element), 10000, 'Element taking too long to appear in the DOM');
    };
};

module.exports = data;

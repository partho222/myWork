/**
 * Created by tariq on 11/29/2016.
 */
'use strict';

var data = function () {
    var self = this;
    var domSelectors = require('../selector_constrain/CommonDOMSelector.js');
    var helper = require('../helper/ProtractorHelper.js');
    var helperInstance = new helper();

    self.getValueAndSelect = function (elementName, elementType, noOfSelection) {
        var elements = helperInstance.findAll(elementName, elementType);
        elements.count().then(function (count) {
            expect(noOfSelection).not.toBeGreaterThan(count, 'No of selection can\'t be greater than number of element. Setting selection to no of max element');
            if (noOfSelection > count) {
                noOfSelection = count;
            }
            for (var i = 0; i < noOfSelection; i++) {
                elements.get(i).click();
            }
        });
    };

    self.dropDownSelector = function (elementName, elementType, value) {
        var elements = helperInstance.findAll(elementName, elementType);
        elements.then(function (currentElement) {
            currentElement.forEach(function (data, key) {
                data.getAttribute('label').then(function (label) {
                    if (label == value) {
                        currentElement[key].click();
                    }
                });
            });
        });
    };

    self.getRandomValue = function (dataArray) {
        return dataArray[Math.floor(Math.random() * dataArray.length)];
    }
};

module.exports = data;

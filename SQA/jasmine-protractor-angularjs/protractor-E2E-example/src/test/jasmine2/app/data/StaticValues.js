/**
 * Created by tariq on 12/1/2016.
 */

'use strict';

var data = {
    ccyValues: ['GBP', 'USD', 'EUR'],
    CREATE_OFFER_MESSAGE: 'To Create an Offer',
    OFFER_CREATE_SUCCESS_MESSAGE: 'Your Offer has been created successfully and sent to the selected funders'
};

module.exports = data;
/**
 * Created by tariq on 11/28/2016.
 */

'use strict';

var data = {
    'userTestData': [
        {
            username: 'Admin',
            password: 'tradeix~1',
            name: 'Administrator'
        }, {
            username: 'Supplier1',
            password: 'tradeix~1',
            name: 'Altenwerth Inc and Sons (S1)'
        }, {
            username: 'Funder1',
            password: 'tradeix~1',
            name: 'Swift, Dooley and Gutkowski (F1)'
        }],
    'bidCreateTestData': [
        {
            username: 'Supplier4',
            password: 'tradeix~1',
            name: 'Maggio-Will (S4)',
            NO_OF_BUYERS: 1,
            NO_OF_INVOICES: 1,
            NO_OF_FUNDERS: 1,
            HOURS_OFFER_VALID: 48
        }]
};

module.exports = data;
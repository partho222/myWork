var localNodeJsPath = '../../../lib/nodeJS/node_modules';
var htmlReporter = require(localNodeJsPath + '/protractor-jasmine2-html-reporter');
require(localNodeJsPath + '/cli');

exports.config = {
    onPrepare: function () {
        jasmine.getEnv().addReporter(new htmlReporter({
                savePath: '../../../report/',
                filePrefix: 'TXC_Report',
                takeScreenshots: true,
                screenshotsFolder: 'screenshots'
            })
        );
    },
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome'
    },
    framework: 'jasmine2',
    specs: ['../jasmine2/TXC/specs/TXCB*.js'],
    jasmineNodeOpts: {
        realtimeFailure: true,
        onComplete: null,
        isVerbose: true,
        showColors: true,
        includeStackTrace: false,
        defaultTimeoutInterval: 2 * 60 * 1000
    },
    baseUrl: 'http://localhost:56574'
};
Before running the test --

## Add dependencies into - 'lib/nodejs' folder:

1. cli
2. jasmine-data-provider
3. protractor-jasmine2-html-reporter
4. webdriver-manager

Add from global - node link {module_name} 
or locally using - node install {module_name}

## Run test from - 'src/test/protractor':
using - protractor conf.js

## Make sure to run webdriver manager at 'http://localhost:4444/wd/hub':
using - webdriver-manager start --seleniumPort=4444

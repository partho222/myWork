var localNodeJsPath = '../../../lib/nodeJS/node_modules';
var htmlReporter = require(localNodeJsPath + '/protractor-jasmine2-html-reporter');
var cliDebugger = require(localNodeJsPath + '/cli');

exports.config = {
    onPrepare: function () {
        // Add html reporter with screenshot
        jasmine.getEnv().addReporter(new htmlReporter({
                savePath: '../../../report/',
                takeScreenshots: true,
                screenshotsFolder: 'screenshots'
            })
        );
    },
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome'
    },
    framework: 'jasmine2',
    specs: ['../specs/*.js'],
    jasmineNodeOpts: {
        // onComplete will be called just before the driver quits.
        onComplete: null,
        // If true, display spec names.
        isVerbose: true,
        // If true, print colors to the terminal.
        showColors: true,
        // If true, include stack traces in failures.
        includeStackTrace: true,
        // Default time to wait in ms before a test fails.
        defaultTimeoutInterval: 10000
    }
};
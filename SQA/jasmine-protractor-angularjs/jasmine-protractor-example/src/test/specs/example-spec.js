describe('Sample Application Test', function () {

    var testUser = {
        firstName: 'tariq',
        lastName: 'khan',
        username: 'tariq',
        password: '12345'
    };

    it('Browse Site', function () {
        browser.get('http://localhost/angular/authentication/index.html');
    });

    it('Register Test', function () {
        var webElement = element(by.css('.form-actions .btn.btn-link'));
        webElement.click();
        element(by.css('#firstName')).sendKeys(testUser.firstName);
        element(by.css('#Text1')).sendKeys(testUser.lastName);
        element(by.css('#username')).sendKeys(testUser.username);
        element(by.css('#password')).sendKeys(testUser.password);
        element(by.css('.btn.btn-primary')).click();
        expect(element(by.css('.alert.alert-success')).getText()).toEqual('Registration successful');
    });

    it('Login Test', function () {
        browser.get('http://localhost/angular/authentication/#/login');
        element(by.css('#username')).sendKeys(testUser.username);
        element(by.css('#password')).sendKeys(testUser.password);
        element(by.css('.btn.btn-primary')).click();
        expect(element(by.css('.ng-binding')).getText()).toEqual('Hi ' + testUser.username + '!');
    });

    it('Remove users', function () {
        var data = element.all(by.css('.ng-binding.ng-scope > a'));

        data.count().then(function (count) {
            for (var i = 0; i < count; i++) {
                data.get(i).click();
            }
        });

        browser.get('http://localhost/angular/authentication/');
        data = element.all(by.css('.ng-binding.ng-scope > a'));
        expect(data.count()).toEqual(0);
    })
});
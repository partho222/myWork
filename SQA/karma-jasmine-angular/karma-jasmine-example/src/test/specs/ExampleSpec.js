/**
 * Created by tariq on 12/4/2016.
 */
describe('Authentication App : ', function () {
    var scope, $location, userService, flashService, authenticationService, $controller, Q, timeout;
    /** Load Controller and Service **/
    beforeEach(function () {
        angular.mock.module('app');
        angular.mock.inject(function ($injector) {
            $controller = $injector.get('$controller');
            Q = $injector.get('$q');
            userService = $injector.get('UserService');
            authenticationService = $injector.get('AuthenticationService');
            flashService = $injector.get('FlashService');
            $location = $injector.get('$location');
            scope = $injector.get('$rootScope');
            timeout = $injector.get('$timeout');
        });
    });
    /** Ends Here **/
    /** ################################################################################################# **/

    describe('Register Controller Test', function () {
        it('Checking Functionality', function () {
            var controller = $controller('RegisterController as vm', {$scope: scope});

            spyOn(userService, "Create").and.callFake(function () {
                var deferred = Q.defer();
                return deferred.promise;
            });
            controller.register();
            expect(userService.Create).toHaveBeenCalled();
        });
    });


    /** ################################################################################################# **/
    // need to fix promise catch
    describe('User Service Test', function () {
        it('Check Service Existance', function () {
            expect(userService).toBeDefined();
        });

        xit('Checking Spy Call', function () {
            var user = {
                username: 'tariq',
                password: '12345'
            };

            var deferred = Q.defer();
            var promise = deferred.promise;
            var resolvedValue;
            promise.then(function (value) {
                resolvedValue = value;
            });

            spyOn(userService, 'Create');
            var response = userService.Create(user);

            // expect(response).toBeResolvedWith('something');

        });
    });
});
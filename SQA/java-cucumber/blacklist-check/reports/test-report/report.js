$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("1file.feature");
formatter.feature({
  "line": 3,
  "name": "File Operation",
  "description": "",
  "id": "file-operation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Clean-File"
    }
  ]
});
formatter.before({
  "duration": 2698002999,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Only Run First Time",
  "description": "",
  "id": "file-operation;only-run-first-time",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Cleaning Files",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.setFiles()"
});
formatter.result({
  "duration": 98331539,
  "status": "passed"
});
formatter.after({
  "duration": 944190304,
  "status": "passed"
});
formatter.uri("2check.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# Gherkin Language"
    }
  ],
  "line": 4,
  "name": "Testing Black List",
  "description": "",
  "id": "testing-black-list",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@blacklist-check"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Checking Domain BlackList",
  "description": "",
  "id": "testing-black-list;checking-domain-blacklist",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "browse RBL",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "set URL \u003curl\u003e to check box",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "click check",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "write result \u003curl\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "testing-black-list;checking-domain-blacklist;",
  "rows": [
    {
      "cells": [
        "url"
      ],
      "line": 14,
      "id": "testing-black-list;checking-domain-blacklist;;1"
    },
    {
      "cells": [
        "bork.webmascot.com"
      ],
      "line": 15,
      "id": "testing-black-list;checking-domain-blacklist;;2"
    },
    {
      "cells": [
        "mx6.webmascot.com"
      ],
      "line": 16,
      "id": "testing-black-list;checking-domain-blacklist;;3"
    },
    {
      "cells": [
        "mx7.webmascot.com"
      ],
      "line": 17,
      "id": "testing-black-list;checking-domain-blacklist;;4"
    },
    {
      "cells": [
        "Bleep.bitmascot.com"
      ],
      "line": 18,
      "id": "testing-black-list;checking-domain-blacklist;;5"
    },
    {
      "cells": [
        "cahill.webmascot.com"
      ],
      "line": 19,
      "id": "testing-black-list;checking-domain-blacklist;;6"
    },
    {
      "cells": [
        "kewell.webmascot.com"
      ],
      "line": 20,
      "id": "testing-black-list;checking-domain-blacklist;;7"
    },
    {
      "cells": [
        "liddy.webmascot.com"
      ],
      "line": 21,
      "id": "testing-black-list;checking-domain-blacklist;;8"
    },
    {
      "cells": [
        "zimbra.webmascot.com"
      ],
      "line": 22,
      "id": "testing-black-list;checking-domain-blacklist;;9"
    },
    {
      "cells": [
        "bindi.webmascot.com"
      ],
      "line": 23,
      "id": "testing-black-list;checking-domain-blacklist;;10"
    }
  ],
  "keyword": "Examples",
  "tags": [
    {
      "line": 12,
      "name": "@url-check"
    }
  ]
});
formatter.before({
  "duration": 2412399634,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Checking Domain BlackList",
  "description": "",
  "id": "testing-black-list;checking-domain-blacklist;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@blacklist-check"
    },
    {
      "line": 12,
      "name": "@url-check"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "browse RBL",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "set URL bork.webmascot.com to check box",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "click check",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "write result bork.webmascot.com",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.browseURL()"
});

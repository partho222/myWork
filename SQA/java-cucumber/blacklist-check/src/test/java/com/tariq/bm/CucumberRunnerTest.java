package com.tariq.bm;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by tariq on 10/23/2016.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "src/test/resources/"
        },
        glue = {

        },
        format={
                "pretty",
                "html:reports/test-report"
        },
        tags = {

        }
)
public class CucumberRunnerTest {

}
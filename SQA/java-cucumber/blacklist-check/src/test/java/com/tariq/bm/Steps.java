package com.tariq.bm;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by tariq on 10/23/2016.
 */
public class Steps {

    WebDriver driver;
    private final String CHROME_DRIVER = "webdriver.chrome.driver";
    private final String LOCAL_CHROME_DRIVER_LOCATION = "resources/windows/chromedriver_v2.27.exe";

    @Before
    public void setup() {
        System.setProperty(CHROME_DRIVER, LOCAL_CHROME_DRIVER_LOCATION);    // Autorun browser driver
        try {
            driver = new ChromeDriver();
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
    }

    @Given("^Cleaning Files$")
    public void setFiles () {
        String serverFileLocation = "results\\black-list.txt";
        String mxFileLocation = "results\\Mx-black-list.txt";

        try {
            new OutputStreamWriter(new FileOutputStream(serverFileLocation, false), "UTF-8");
            new OutputStreamWriter(new FileOutputStream(mxFileLocation, false), "UTF-8");
        } catch ( Exception ex ) {
            System.out.println("File Error : " + ex.getMessage());
        }
    }

    @Given("^browse RBL$")
    public void browseURL() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://www.anti-abuse.org/multi-rbl-check-results/");
    }

    @When("^set URL ([^\"]*) to check box$")
    public void setURL(String url) {
        WebElement inputBox = driver.findElement(By.name("host"));
        inputBox.sendKeys(url);
    }

    @Then("^click check$")
    public void clickButton() {
        WebElement clickCheck = driver.findElement(By.className("checksubmit"));
        clickCheck.click();
    }

    @Then("^write result ([^\"]*)$")
    public void extractResult(String url) {
        List<WebElement> result = driver.findElements(By.className("blocked"));

        String fileLocation = "results\\black-list.txt";
        WriteToFile.openFile(fileLocation);
        WriteToFile.appendToFile("Server : " + url + "\r\n");

        for ( int i=0; i < result.size(); i++) {
            System.out.println( result.get(i).getText() );
            WriteToFile.appendToFile( "\r\n" + result.get(i).getText() );
        }
        WriteToFile.appendToFile("\r\n\r\n############################################\r\n");
    }

    /** Get MX Server Black List Results **/

    @Given("^browse MXTOOLBOX$")
    public void browseMxURL() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://mxtoolbox.com/SuperTool.aspx");
    }

    @When("^set Mx URL ([^\"]*) to check box$")
    public void setMxURL(String url) {
        WebElement inputBox = driver.findElement(By.id("txtInput2"));
        inputBox.sendKeys(url);
    }

    @Then("^click check for MX$")
    public void clickButtonMx() {
        /** JavaScript Executor to execute javascript **/
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById(\"hiddenCommand\").textContent=\"blacklist\";");
        js.executeScript("document.getElementById(\"btnAction3\").textContent=\"Blacklist Check\";");
        /** JavaScript Ends Here **/

        WebElement clickCheck = driver.findElement(By.id("btnAction3"));
        clickCheck.click();
    }

    @Then("^write MX result ([^\"]*)$")
    public void extractMxResult(String url) {
        List<WebElement> result = driver.findElements(By.className("tool-blacklist-reason"));

        String fileLocation = "results\\Mx-black-list.txt";
        WriteToFile.openFile(fileLocation);
        WriteToFile.appendToFile("Server : " + url + "\r\n");

        for ( int i=0; i < result.size(); i++) {
            if ( result.get(i).getText() != null && ! result.get(i).getText().equals("") ) {
                String[] parts = result.get(i).getText().split(" ");
                WriteToFile.appendToFile( "\r\n" +  parts[0] );
            }
        }
        WriteToFile.appendToFile("\r\n\r\n############################################\r\n");
    }

    @After
    public void closeFileAndBrowser() {
        WriteToFile.closeFile();
        driver.quit();
    }
}

package com.tariq.bm;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by tariq on 10/26/2016.
 */

public class WriteToFile {
    static OutputStreamWriter writer = null;
    static BufferedWriter bufferedWriter = null;

    public static void appendToFile( String line ) {
        try {
            bufferedWriter.write(line);
        } catch (Exception ex) {
            System.out.println("File Error : " + ex.getMessage());
        }
    }

    public static void openFile (String fileLocation)  {
        try {
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileLocation, true), "UTF-8");
            bufferedWriter = new BufferedWriter(writer);
        } catch ( Exception ex ) {
            System.out.println("File Error : " + ex.getMessage());
        }
    }

    public static void closeFile () {
        try {
            bufferedWriter.close();
        } catch (Exception ex) {
            System.out.println("File Error : " + ex.getMessage());
        }
    }
}
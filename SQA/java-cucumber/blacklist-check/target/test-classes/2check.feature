# Gherkin Language
@blacklist-check

Feature: Testing Black List

  Scenario Outline: Checking Domain BlackList
    Given browse RBL
    When set URL <url> to check box
    Then click check
    Then write result <url>

@url-check
    Examples:
    |url|
    |bork.webmascot.com|
    |mx6.webmascot.com|
    |mx7.webmascot.com|
    |Bleep.bitmascot.com|
    |cahill.webmascot.com|
    |kewell.webmascot.com|
    |liddy.webmascot.com|
    |zimbra.webmascot.com|
    |bindi.webmascot.com|


  Scenario Outline: Checking MX Server BlackList

    Given browse MXTOOLBOX
    When set Mx URL <url> to check box
    Then click check for MX
    Then write MX result <url>

@mx-check
    Examples:
    |url|
    |bork.webmascot.com|
    |mx6.webmascot.com|
    |mx7.webmascot.com|
    |Bleep.bitmascot.com|
    |cahill.webmascot.com|
    |kewell.webmascot.com|
    |liddy.webmascot.com|
    |zimbra.webmascot.com|
    |bindi.webmascot.com|
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.gl.E;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by tariq on 10/23/2016.
 */
public class Steps extends HelperFramework {

    WebDriver driver;

    @Before
    public void setup() {
        startDriver();
    }

    @Given("^browse URL$")
    public void browseURL() {
        browseURL("https://www.facebook.com/", 5);
    }

    @When("^set ([^\"]*)$")
    public void setUserName(String name) {
        try {
            WebElement emailInputField = driver.findElement(By.id("dd"));
            emailInputField.sendKeys(name);
        } catch ( Exception ex) {
            System.out.println("\n Error while selecting Email Input Field \n");
        }

        WebElement passInputField = driver.findElement(By.id("pass"));
        passInputField.sendKeys("bitmascot@123");
        WebElement loginButton = driver.findElement(By.id("loginbutton"));
        loginButton.click();
    }

    @And("^set birth ([^\"]*)$")
    public void i_set_birth_month (String month) {

    }

    @Then("check login")
    public void loginCheck() {

    }
}

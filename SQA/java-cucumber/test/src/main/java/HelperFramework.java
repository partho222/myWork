import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by tariq on 10/26/2016.
 */
public class HelperFramework {
    static WebDriver driver;
    static String CHROME_DRIVER = "webdriver.chrome.driver";
    static String DRIVER_LOCATION = "C:\\Users\\tariq\\Desktop\\lib\\driver\\chromedriver.exe";

    public static void init () {
        System.setProperty(CHROME_DRIVER, DRIVER_LOCATION);
    }

    public static void startDriver () {
        init();
        try {
            driver = new RemoteWebDriver( new URL( "http://localhost:9515" ), DesiredCapabilities.chrome() );
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
    }

    public static void stopDriver() {
        driver.quit();
    }

    /**
     *
     * @param url String
     * @param waitTime Integer
     */
    public static void browseURL ( String url, Integer waitTime ) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
        driver.get(url);
    }

    /**
     *
     * @param elementName String
     * @return WebElement
     *
     * #elementName -> id
     * .elementName -> class
     * @elementName -> tagName
     * elementName  -> name
     */
    public static WebElement findElement ( String elementName ) {
        WebElement element = null;

        if ( elementName.charAt(0) == '#' ) {
            String id = elementName.substring(1);
            element = driver.findElement(By.id(id));
        } else if ( elementName.charAt(0) == '.' ) {
            String className = elementName.substring(1);
            element = driver.findElement(By.className(className));
        } else if ( elementName.charAt(0) == '@' ) {
            String tagName = elementName.substring(1);
            element = driver.findElement(By.tagName(tagName));
        } else {
            element = driver.findElement(By.name(elementName));
        }
        return element;
    }

    /**
     *
     * @param elementName String
     * @param value String
     */
    public static void setText ( String elementName, String value ) {
        WebElement webElement = findElement(elementName);
        webElement.sendKeys(value);
    }

    /**
     *
     * @param elementName String
     */
    public static void pushClick ( String elementName ) {
        WebElement webElement = findElement(elementName);
        webElement.click();
    }

    /**
     *
     * @param elementName String
     * @param value String
     *
     * #elementName -> id
     * .elementName -> class
     * @elementName -> tagName
     * elementName  -> name
     */
    public static void setContentText ( String elementName, String value ) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        if ( elementName.charAt(0) == '#' ) {
            String id = elementName.substring(1);
            js.executeScript("document.getElementById(\""+ id +"\").textContent=\"" + value + "\";");
        } else if ( elementName.charAt(0) == '.' ) {
            String className = elementName.substring(1);
            js.executeScript("document.getElementByClassName(\""+ className +"\").textContent=\"" + value + "\";");
        } else if ( elementName.charAt(0) == '@' ) {
            String tagName = elementName.substring(1);
            js.executeScript("document.getElementByTagName(\""+ tagName +"\").textContent=\"" + value + "\";");
        } else {
            js.executeScript("document.getElementByName(\""+ elementName +"\").textContent=\"" + value + "\";");
        }
    }

    /**
     *
     * @param elementName String
     * @return String
     */
    public static String getText ( String elementName ) {
        WebElement webElement = findElement(elementName);
        return webElement.getText();
    }

    /**
     *
     * @param elementName String
     * @param attributeName String
     * @return String
     */
    public static String getAttributeValue ( String elementName, String attributeName ) {
        WebElement webElement = findElement(elementName);
        return webElement.getAttribute(attributeName);
    }

    /**
     *
     * @param elementName String
     * @param cssFieldName String
     * @return String
     */
    public static String getCSSValue ( String elementName, String cssFieldName ) {
        WebElement webElement = findElement(elementName);
        return webElement.getCssValue(cssFieldName);
    }
}

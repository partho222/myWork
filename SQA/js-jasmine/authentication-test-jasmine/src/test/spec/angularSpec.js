describe("Unit: Testing Angular JS App", function () {
    var $controller, userService, authenticationService, Q;
    // Adding Controllers, service, $q service
    beforeEach(function () {
        module('app');
        inject(function ($injector) {
            $controller = $injector.get('$controller');
            Q = $injector.get('$q');
            // Services
            userService = $injector.get('UserService');
            authenticationService = $injector.get('AuthenticationService');
        });
    });

    describe('Register Controller test', function () {
        it('Controller function name check', function () {
            var $scope = {};
            var controller = $controller('RegisterController as vm', {$scope: $scope});

            expect($scope.vm.register.name).toEqual('register');
        });

        it('Controller function call test', function () {
            var $scope = {};
            var controller = $controller('RegisterController as vm', {$scope: $scope});
            spyOn(controller, 'register');
            expect(controller.register).not.toHaveBeenCalled();
        });
    });

    describe('User Service Test', function () {
        var testUser = {
            firstName: 'tariq',
            lastName: 'khan',
            username: 'tariq',
            password: '12345'
        };

        it('User Service Function Call test', function () {
            spyOn(userService, 'Create');
            expect(userService.Create).not.toHaveBeenCalled();
        });

        /* Added Register controller and
         * call 'register' function with parameter
         * to test User Service -> Create function
         * using fake call to return promise
         * */
        it('User Service\'s method call test from controller', function () {
            var $scope = {};
            var controller = $controller('RegisterController as vm', {$scope: $scope});

            spyOn(userService, 'Create').and.callFake(function () {
                var deferred = Q.defer();
                return deferred.promise;
            });
            $scope.vm.register(testUser);     // can use -> controller.register(testUser);
            expect(userService.Create).toHaveBeenCalled();
        });
    });
});
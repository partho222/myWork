describe("Example script test", function () {
    var JScript;
    beforeEach(function () {
        JScript = new example();
    });

    describe("Testing script functions", function () {

        it("Should calculate the factorial of 5", function () {
            expect(JScript.factorial(5)).toEqual(120);
        });

        it("Testing a spy call", function () {
            var box = new Rectangle(5, 12);
            spyOn(box, "calculateArea");
            box.getArea();
            expect(box.calculateArea).toHaveBeenCalled();
        });
    });
});
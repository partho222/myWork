example = function () {
};

example.prototype.factorial = function (number) {
    if (number < 0) {
        throw new Error("Factorial Can't be calculated for negative value");
    } else {
        var result = 1;
        if (number > 1) {
            result = number * this.factorial(number - 1);
        }
        return result;
    }
};

// Class Object
var Rectangle = (function () {
    // Constructor
    function Rectangle(height, width) {
        this.height = height;
        this.width = width;
    }

    // Method
    Rectangle.prototype.calculateArea = function () {
        return this.height * this.width;
    }

    // Method
    Rectangle.prototype.getArea = function () {
        return "Total area : " + this.calculateArea() + " sq";
    }
    return Rectangle;
})();
package com.bitmascot.plugin.product_custom_information

import grails.transaction.Transactional

class ProductCustomInformationService {

    @Transactional
    Map saveCustomInformation(Map params) {
        ProductCustomField productCustomField;

        if (params.id) {
            productCustomField = ProductCustomField.get(params.id as Long);
        } else {
            productCustomField = new ProductCustomField();
        }

        if (!isExist(params.title as String, params.type as String)) {
            productCustomField.title = params.title as String;
            productCustomField.type = params.type as String;
            productCustomField.save();
            return [
                    id   : productCustomField.id,
                    title: productCustomField.title,
                    type : productCustomField.type
            ];
        } else {
            throw new Exception("custom.information.exist.error");
        }
    }

    @Transactional
    boolean removeCustomInformation(Long informationID) {
        try {
            ProductCustomField productCustomField = ProductCustomField.get(informationID);
            List<ProductCustomFiledValue> productCustomFiledValueList = ProductCustomFiledValue.findAllByField(productCustomField);
            if (productCustomFiledValueList != null && productCustomFiledValueList.size() > 0) {
                productCustomFiledValueList.each {
                    it.delete();
                }
            }
            productCustomField.delete();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Transactional
    void saveCustomInformationValue(Map response, Map parameters) {
        if ((parameters.loyaltyPoint.target).equals("product")) {
            saveProductCustomInformationValue(parameters);
        } else if ((parameters.loyaltyPoint.target).equals("variation")) {
            saveVariationCustomInformationValue(parameters);
        }
    }

    private static void saveProductCustomInformationValue(Map parameters) {
        parameters.customInformation.each { key, value ->
            ProductCustomField productCustomField = ProductCustomField.get(key as int);
            ProductCustomFiledValue productCustomFiledValue = ProductCustomFiledValue.findByFieldAndEntityTypeAndEntityId(productCustomField, "product", parameters.id as int);
            if (!productCustomFiledValue) {
                productCustomFiledValue = new ProductCustomFiledValue();
                productCustomFiledValue.value = value;
                productCustomFiledValue.field = productCustomField;
                productCustomFiledValue.entityType = "product";
                productCustomFiledValue.entityId = parameters.id as int;
            } else {
                productCustomFiledValue.value = value;
            }
            productCustomFiledValue.save();
        }
    }

    private void saveVariationCustomInformationValue(Map parameters) {
        parameters.params.customInformation.each { key, value ->
            ProductCustomField productCustomField = ProductCustomField.get(key as int);
            ProductCustomFiledValue productCustomFiledValue = ProductCustomFiledValue.findByFieldAndEntityTypeAndEntityId(productCustomField, "variation", parameters.variationId as int);
            if (!productCustomFiledValue) {
                productCustomFiledValue = new ProductCustomFiledValue();
                productCustomFiledValue.value = value;
                productCustomFiledValue.field = productCustomField;
                productCustomFiledValue.entityType = "variation";
                productCustomFiledValue.entityId = parameters.variationId as int;
            } else {
                productCustomFiledValue.value = value;
            }
            productCustomFiledValue.save();
        }
        removeNonExistVariationCustomInformation(parameters as Map);
    }

    private void removeNonExistVariationCustomInformation(Map params) {
        List<ProductCustomFiledValue> productCustomFiledValueList = getProductCustomFiledValueList("variation", params.variationId as int);
        for (int i = 0; i < productCustomFiledValueList.size(); i++) {
            if (params.params.customInformation != null) {
                if (!(params.params.customInformation.containsKey(productCustomFiledValueList.get(i).fieldId as String))) {
                    productCustomFiledValueList.get(i).delete();
                }
            } else {
                productCustomFiledValueList.get(i).delete();
            }
        }
    }

    private static boolean isExist(String title, String type) {
        return ProductCustomField.findByTitleAndType(title, type);
    }

    List<ProductCustomField> getProductCustomInformationList() {
        return ProductCustomField.list();
    }

    List<ProductCustomFiledValue> getProductCustomFiledValueList() {
        return ProductCustomFiledValue.list();
    }

    List<ProductCustomFiledValue> getProductCustomFiledValueList(String entityType) {
        return ProductCustomFiledValue.findAllByEntityType(entityType);
    }

    List<ProductCustomFiledValue> getProductCustomFiledValueList(String entityType, int entityId) {
        return ProductCustomFiledValue.findAllByEntityTypeAndEntityId(entityType, entityId);
    }
}

package com.bitmascot.plugin.product_custom_information

class ProductCustomInfoTagLib {
    ProductCustomInformationService productCustomInformationService;
    static namespace = "productCustomInfo"
    private String entityType;
    private int entityId;
    private List<ProductCustomFiledValue> productCustomFiledValueListToShow;

    def productPageSettings = { Map attrs, body ->
        out << body()
        out << g.include(view: "/plugins/product_custom_information/productPageSettings.gsp", model: [informations: productCustomInformationService.getProductCustomInformationList()])
    }

    def productAdvancedInformation = { Map attrs, body ->
        out << body()
        if ((attrs.entityType).equals("product")) {
            this.entityId = pageScope.product.id as int;
            out << g.include(view: "/plugins/product_custom_information/productAdvancedInformation.gsp", model: [informations: productCustomInformationService.getProductCustomInformationList(), values: productCustomInformationService.getProductCustomFiledValueList(attrs.entityType as String, this.entityId), entityType: attrs.entityType]);
        } else if ((attrs.entityType).equals("variation")) {
            this.entityId = pageScope.variationId as int;

            List<ProductCustomField> productCustomFieldList = productCustomInformationService.getProductCustomInformationList();
            List<ProductCustomFiledValue> productCustomFiledValueList = productCustomInformationService.getProductCustomFiledValueList(attrs.entityType as String, this.entityId);
            List<ProductCustomFiledValue> newProductCustomFiledValueList = new ArrayList<>();

            for (int i = 0; i < productCustomFieldList.size(); i++) {
                boolean flag = false;
                for (int j = 0; j < productCustomFiledValueList.size(); j++) {
                    if (productCustomFieldList[i].id == productCustomFiledValueList[j].fieldId) {
                        newProductCustomFiledValueList.add(productCustomFiledValueList[j]);
                        flag = true;
                    }
                }
                if (!flag) {
                    newProductCustomFiledValueList.add(ProductCustomFiledValue.findByFieldAndEntityTypeAndEntityId(productCustomFieldList[i], "product", pageScope.product.id as int));
                }
            }
            out << g.include(view: "/plugins/product_custom_information/productAdvancedInformation.gsp", model: [informations: productCustomFieldList, values: newProductCustomFiledValueList, entityType: attrs.entityType]);
        }
    }

    def productTabInformationHeader = { Map attrs, body ->
        out << body()
        productCustomFiledValueListToShow = new ArrayList<>();
        try {
            if ((pageScope.productData.variationModel).equals("enterprise")) {
                this.entityType = "variation";
                this.entityId = pageScope.productData.productVariationId as int;

                List<ProductCustomField> productCustomFieldList = productCustomInformationService.getProductCustomInformationList();
                List<ProductCustomFiledValue> productCustomFiledValueList = productCustomInformationService.getProductCustomFiledValueList(this.entityType, this.entityId);

                for (int i = 0; i < productCustomFieldList.size(); i++) {
                    boolean flag = false;
                    for (int j = 0; j < productCustomFiledValueList.size(); j++) {
                        if (productCustomFieldList[i].id == productCustomFiledValueList[j].fieldId) {
                            this.productCustomFiledValueListToShow.add(productCustomFiledValueList[j]);
                            flag = true;
                        }
                    }
                    if (!flag) {
                        this.productCustomFiledValueListToShow.add(ProductCustomFiledValue.findByFieldAndEntityTypeAndEntityId(productCustomFieldList[i], "product", pageScope.product.id as int));
                    }
                }
            }
        } catch (Exception ex) {
            this.entityType = "product";
            this.entityId = pageScope.product.id as int;
            this.productCustomFiledValueListToShow = productCustomInformationService.getProductCustomFiledValueList(this.entityType, this.entityId);
        }

        for (int i = 0; i < this.productCustomFiledValueListToShow.size(); i++) {
            if (this.productCustomFiledValueListToShow.get(i) != null) {
                out << '<div class="bmui-tab-header" tab-id="customInformation-' + (this.productCustomFiledValueListToShow.get(i).id) + '">'
                out << '<span class="title">' + g.message(code: this.productCustomFiledValueListToShow.get(i).field.title) + '</span>'
                out << '</div>'
            }
        }
    }

    def productTabInformationBody = { Map attrs, body ->
        out << body()
        for (int i = 0; i < this.productCustomFiledValueListToShow.size(); i++) {
            if (this.productCustomFiledValueListToShow.get(i) != null) {
                out << '<div id="bmui-tab-customInformation-' + (this.productCustomFiledValueListToShow.get(i).id) + '" class="bmui-tab-panel" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">';
                out << '<span class="description">' + g.message(code: this.productCustomFiledValueListToShow.get(i).value) + '</span>'
                out << '</div>'
            }
        }
    }
}

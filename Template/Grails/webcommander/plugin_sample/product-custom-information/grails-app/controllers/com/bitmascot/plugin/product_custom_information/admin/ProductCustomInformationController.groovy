package com.bitmascot.plugin.product_custom_information.admin

import com.bitmascot.plugin.product_custom_information.ProductCustomInformationService
import grails.converters.JSON

import javax.xml.bind.ValidationException

class ProductCustomInformationController {
    ProductCustomInformationService productCustomInformationService;

    def saveCustomFields() {
        try {
            Map result = productCustomInformationService.saveCustomInformation(params);
            String message;
            if (params.id) {
                message = g.message(code: "custom.information.update.success");
            } else {
                message = g.message(code: "custom.information.save.success");
            }
            result.status = "success";
            result.message = message;
            render(result as JSON)
        } catch (ValidationException ex) {
            render([status: "error", message: g.message(code: ex.message)] as JSON)
        } catch (Exception ex) {
            render([status: "error", message: g.message(code: ex.message)] as JSON)
        }
    }

    def removeCustomField() {
        if (productCustomInformationService.removeCustomInformation(params.informationID as Long)) {
            render([status: "success", message: g.message(code: "custom.information.remove.success")] as JSON)
        } else {
            render([status: "error", message: g.message(code: "custom.information.remove.error")] as JSON)
        }
    }
}

package com.bitmascot.plugin.product_custom_information

class ProductCustomFiledValue {
    Long id

    String value
    String entityType
    int entityId
    ProductCustomField field

    static mapping = {
        value sqlType: "text"
        sort field: "asc"
    }
}

import com.bitmascot.plugin.GrailsPluginBase
import com.bitmascot.plugin.PluginMeta

class ProductCustomInformationGrailsPlugin extends GrailsPluginBase {
    String version = "1.0.0";
    {
        grailsVersion = "2.4 > *"
        title = "Product Custom Information" // Headline display name of the _plugin
        author = "Tariq Ahmed Khan"
        authorEmail = "tariq@bitmascot.com"
        description = '''Show Custom Information of Product'''
        documentation = "http://grails.org/plugin/product-custom-information"
        _plugin = new PluginMeta(identifier: "product-custom-information", name: title, loader: ProductCustomInformationGrailsPlugin.classLoader);

        hooks = [
                productPageSettings: [taglib: "productCustomInfo", callable: "productPageSettings"],
                productAdvancedInformation: [taglib: "productCustomInfo", callable: "productAdvancedInformation"],
                saveCategoryAdvancedData: [bean: "productCustomInformationService", callable: "saveCustomInformationValue"],
                productInfoTabHeader: [taglib: "productCustomInfo", callable: "productTabInformationHeader"],
                productInfoTabBody: [taglib: "productCustomInfo", callable: "productTabInformationBody"]
        ]
    }
}
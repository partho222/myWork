package com.tariq.system.helper;

/**
 * Created by tariq on 26/10/2016.
 */

import com.google.common.base.Function;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.bitmascot.system.helper.SystemConstant.*;
import static java.util.concurrent.TimeUnit.SECONDS;

public class SeleniumHelper {

    public static WebDriver webDriver;

    private static void init(String driverType) {
        if (driverType.equals("chrome")) {
            System.setProperty(SystemConstant.CHROME_DRIVER, ConfigLoader.getPropertiesById(SystemConstant.LOCAL_CHROME_DRIVER_LOCATION, SystemConstant.CHROME_DRIVER));
        } else if (driverType.equals("firefox")) {
            System.setProperty(SystemConstant.GECKO_DRIVER, ConfigLoader.getPropertiesById(SystemConstant.LOCAL_GECKO_DRIVER_LOCATION, SystemConstant.GECKO_DRIVER));
        }
    }

    public static void setup() {
        final String driverType = ConfigLoader.getPropertiesById(SystemConstant.DRIVER_TYPE);
        init(driverType);

        String isRemote = ConfigLoader.getPropertiesById(SystemConstant.IS_REMOTE, "no");
        if (isRemote.equals("yes")) {
            try {
                webDriver = new RemoteWebDriver(new URL(ConfigLoader.getPropertiesById(SystemConstant.REMOTE_BROWSER_URL, SystemConstant.LOCAL_BROWSER_URL)), DesiredCapabilities.chrome());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            if (driverType.equals("chrome")) {
                webDriver = new ChromeDriver();
            } else if (driverType.equals("firefox")) {
                webDriver = new FirefoxDriver();
            }
        }
    }

    public static void exit() {
        webDriver.quit();
    }

    public void close() {
        webDriver.quit();
    }

    private boolean isXpath(String elementName) {
        String genericName = elementName.substring(1);
        if (elementName.startsWith("/") || genericName.startsWith("/") || elementName.startsWith("(")) {
            return true;
        }
        return false;
    }

    public WebElement find(String elementName, String userMessage) {
        if (elementName == null || elementName.equals("")) {
            return null;
        } else {
            try {
                if (!isXpath(elementName)) {
                    String genericName = elementName.substring(1);
                    String[] elements = elementName.split(" ");
                    if (elements.length > 1) {
                        return webDriver.findElement(By.cssSelector(elementName));
                    } else if (elementName.startsWith("#")) {
                        return webDriver.findElement(By.id(genericName));
                    } else if (elementName.startsWith(".")) {
                        return webDriver.findElement(By.className(genericName));
                    } else {
                        return webDriver.findElement(By.name(elementName));
                    }
                } else {
                    return webDriver.findElement(By.xpath(elementName));
                }
            } catch (Exception e) {
                findExceptionHandler(e, userMessage);
                return null;
            }
        }
    }

    public WebElement find(WebElement webElement, String elementName, String userMessage) {
        if (elementName == null || elementName.equals("")) {
            return webElement;
        } else {
            try {
                if (!isXpath(elementName)) {
                    String genericName = elementName.substring(1);
                    String[] elements = elementName.split(" ");
                    if (elements.length > 1) {
                        return webElement.findElement(By.cssSelector(elementName));
                    } else if (elementName.startsWith("#")) {
                        return webElement.findElement(By.id(genericName));
                    } else if (elementName.startsWith(".")) {
                        return webElement.findElement(By.className(genericName));
                    } else {
                        return webElement.findElement(By.name(elementName));
                    }
                } else {
                    return webElement.findElement(By.xpath(elementName));
                }
            } catch (Exception e) {
                findExceptionHandler(e, userMessage);
                return null;
            }
        }
    }

    private void findExceptionHandler(Exception e, String userMessage) {
        String message = e.getMessage();
        if (SystemConstant.isDebug) {
            e.printStackTrace();
        }
        if (userMessage != null || !userMessage.equals("")) {
            message = userMessage;
        }
        Assert.fail(message);
    }

    public List<WebElement> findAll(String elementName, String userMessage) {
        if (elementName == null || elementName.equals("")) {
            return null;
        } else {
            try {
                if (!isXpath(elementName)) {
                    String genericName = elementName.substring(1);
                    String[] elements = elementName.split(" ");
                    if (elements.length > 1) {
                        return webDriver.findElements(By.cssSelector(elementName));
                    } else if (elementName.startsWith("#")) {
                        return webDriver.findElements(By.id(genericName));
                    } else if (elementName.startsWith(".")) {
                        return webDriver.findElements(By.className(genericName));
                    } else {
                        return webDriver.findElements(By.name(elementName));
                    }
                } else {
                    return webDriver.findElements(By.xpath(elementName));
                }
            } catch (Exception e) {
                findExceptionHandler(e, userMessage);
                return null;
            }
        }
    }

    public List<WebElement> findAll(WebElement webElement, String elementName, String userMessage) {
        if (elementName == null || elementName.equals("")) {
            return null;
        } else {
            try {
                if (!isXpath(elementName)) {
                    String genericName = elementName.substring(1);
                    String[] elements = elementName.split(" ");
                    if (elements.length > 1) {
                        return webElement.findElements(By.cssSelector(elementName));
                    } else if (elementName.startsWith("#")) {
                        return webElement.findElements(By.id(genericName));
                    } else if (elementName.startsWith(".")) {
                        return webElement.findElements(By.className(genericName));
                    } else {
                        return webElement.findElements(By.name(elementName));
                    }
                } else {
                    return webElement.findElements(By.xpath(elementName));
                }
            } catch (Exception e) {
                findExceptionHandler(e, userMessage);
                return null;
            }
        }
    }

    public void findAndSetValue(String elementName, String value) {
        findAndSetValue(elementName, value, null);
    }

    public void findAndSetValue(String elementName, String value, String message) {
        WebElement webElement = find(elementName, message);
        scrollToElement(webElement);
        webElement.sendKeys(value);
    }

    public void findAndTriggerClick(String elementName) {
        findAndTriggerClick(elementName, null);
    }

    public void findAndTriggerClick(String elementName, String message) {
        WebElement webElement = find(elementName, message);
        scrollToElement(webElement);
        webElement.click();
    }

    public void findAndTriggerClick(WebElement webElement, String elementName) {
        findAndTriggerClick(webElement, elementName, null);
    }

    public void findAndTriggerClick(WebElement webElement, String elementName, String message) {
        WebElement currentElement = find(webElement, elementName, message);
        scrollToElement(currentElement);
        currentElement.click();
    }

    public String findAndGetText(String elementName) {
        return findAndGetText(elementName, null);
    }

    public String findAndGetText(String elementName, String message) {
        WebElement webElement = find(elementName, message);
        return webElement.getText();
    }

    public String findAndGetText(WebElement webElement, String elementName, String message) {
        WebElement currentElement = find(webElement, elementName, message);
        return currentElement.getText();
    }

    public String findAndGetAttributeValue(String elementName, String attributeName) {
        return findAndGetAttributeValue(elementName, attributeName, null);
    }

    public String findAndGetAttributeValue(String elementName, String attributeName, String message) {
        WebElement webElement = find(elementName, message);
        return webElement.getAttribute(attributeName);
    }

    public String findAndGetAttributeValue(WebElement webElement, String elementName, String attributeName) {
        return findAndGetAttributeValue(webElement, elementName, attributeName, null);
    }

    public String findAndGetAttributeValue(WebElement webElement, String elementName, String attributeName, String message) {
        webElement = find(webElement, elementName, message);
        return webElement.getAttribute(attributeName);
    }

    public void browseURL(String url) {
        if (url != null && !url.equals("")) {
            if (webDriver.toString().contains("null")) {
                SeleniumHelper.setup();
            }
            webDriver.manage().window().maximize();
            waitNSecond(5);
            webDriver.get(url);
        }
    }

    public void browseURL(String url, int waitTime) {
        if (url != null && !url.equals("")) {
            if (webDriver.toString().contains("null")) {
                SeleniumHelper.setup();
            }
            webDriver.manage().window().maximize();
            waitNSecond(waitTime);
            webDriver.get(url);
        }
    }

    public void waitUntilVisible(double waitTime) {
        waitTime *= 1000;
        try {
            Thread.sleep((int) waitTime);
        } catch (Exception e) {
            Log.error(e.getMessage());
        }
    }

    public void pageLoadTimeout(double waitTime) {
        waitTime *= 1000;
        webDriver.manage().timeouts().pageLoadTimeout((int) waitTime, TimeUnit.MILLISECONDS);
    }

    public void waitNSecond(int n) {
        webDriver.manage().timeouts().implicitlyWait(n, SECONDS);
    }

    public void waitForElementLoad(int waitTime, String elementName) {
        WebDriverWait wait = new WebDriverWait(webDriver, waitTime);
        if (!isXpath(elementName)) {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementName)));
        } else {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementName)));
        }
    }

    public WebElement fluentWait(final String elementName) {
        return fluentWait(elementName, 40, 2);
    }

    public WebElement fluentWait(final String elementName, int waitTime, int pollTime) {
        Wait<WebDriver> wait = new FluentWait<>(webDriver)
                .withTimeout(waitTime, SECONDS)
                .pollingEvery(pollTime, SECONDS)
                .ignoring(NoSuchElementException.class, AssertionError.class);

        WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return find(elementName, ELEMENT_MESSAGE);
            }
        });
        return webElement;
    }

    public void isEqual(Object expected, Object actual, String message) {
        try {
            Assert.assertEquals(message, expected, actual);
        } catch (Exception ex) {
            Assert.fail(message);
        }
    }

    public String[] getValueFromList(List<WebElement> list) {
        return getValueFromList(list, "value");
    }

    public String[] getValueFromList(List<WebElement> list, String attribute) {
        List<String> value = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            WebElement currentElement = list.get(i);
            if (!(currentElement.getAttribute(attribute).equals("") || currentElement.getAttribute(attribute) == null)) {
                value.add(currentElement.getAttribute(attribute));
            }
        }
        return value.toArray(new String[value.size()]);
    }

    public void resetText(String elementName) {
        WebElement webElement = find(elementName, ELEMENT_MESSAGE);
        webElement.clear();
    }

    public String findAndReplace(String string, String oldWord, String newWord) {
        return string.replace(oldWord, newWord);
    }

    public void hoverCursor(WebElement webElement) {
        Actions builder = new Actions(webDriver);
        builder.moveToElement(webElement).build().perform();
    }

    public void scrollToElement(WebElement webElement) {
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement);
        actions.perform();
    }

    public void setClipBoardData(String data) {
        StringSelection stringSelection = new StringSelection(data);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
    }

    public void setFileNameForUpload(String fileName) throws Exception {
        setClipBoardData(fileName);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    public Boolean regexSearch(String parentString, String regexPattern) {
        return parentString.matches(regexPattern);
    }

    public Boolean isStringContain(String parentString, String searchString) {
        return parentString.contains(searchString);
    }
}
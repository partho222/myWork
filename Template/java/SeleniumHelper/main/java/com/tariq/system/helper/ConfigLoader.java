package com.tariq.system.helper;

import java.io.*;
import java.util.Properties;

public class ConfigLoader {


    public static String configProperties = "resources/config.properties";

    public static String getPropertiesById(String name){
        return getPropertiesById(name,null);
    }

    public static String getPropertiesById(String name, String defaultData){
        Properties properties = new Properties();
        File file = new File(configProperties);
       // Log.info(file.getAbsolutePath());
        if (!file.exists()){
            return defaultData;
        }else{
            try {
                InputStream inputStream = new FileInputStream(file);
                properties.load(inputStream);
                inputStream.close();
                return properties.getProperty(name,defaultData);
            } catch (FileNotFoundException e) {
                Log.error("File Not Found Exception From Config Loader");
            } catch (IOException e) {
                Log.error("IOException From Config Loader");
            }
        }
        return defaultData;
    }

    public static String BASE_URL(){
        return getPropertiesById(SystemConstant.BASE_URL,null);
    }
}

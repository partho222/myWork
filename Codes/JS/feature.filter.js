//region Filter-view initialization
app.tabs.filter = function () {
    app.tabs.filter._super.constructor.apply(this, arguments);

    this.text = $.i18n.prop("filter");
    this.tip = $.i18n.prop("manage.filter");
    this.ui_class = "filter";
    this.ajax_url = app.baseUrl + "filterAdmin/loadAppView";
    this.left_panel_url = "filterAdmin/loadLeftPanel";
    this.right_panel_views = {
        "default": {
            ajax_url: "filterAdmin/explorerView"
        }
    };
};

app.ribbons.web_commerce.push({
    text: $.i18n.prop("filter"),
    processor: app.tabs.filter,
    ui_class: "filter",
    license: "allow_filter_feature"
});

app.tabs.filter.inherit(app.TwoPanelExplorerTab);
var _f = app.tabs.filter.prototype;

_f.menu_entries = [
    {
        text: $.i18n.prop("edit"),
        ui_class: "edit",
        action: "edit"
    },
    {
        text: $.i18n.prop("set.as.default"),
        ui_class: "set.default",
        action: "set.default"
    },
    {
        text: $.i18n.prop("copy"),
        ui_class: "copy",
        action: "copy"
    },
    {
        text: $.i18n.prop("remove"),
        ui_class: "delete",
        action: "delete"
    }
];

_f.onActionClick = function (action, data) {
    switch (action) {
        case "edit" :
            this.createProfilePopup(data.id, data.name);
            break;
        case "set.default" :
            this.setProfileAsDefault(data.id);
            break;
        case "copy" :
            this.copyProfile(data.id);
            break;
        case "delete":
            this.removeProfile(data.id);
            break;
    }
};

_f.init = function () {
    var _self = this;
    app.tabs.filter._super.init.call(this);
};

_f.initLeftPanel = function (leftPanel) {
    var _self = this;
    app.tabs.filter._super.initLeftPanel.apply(this, arguments);
    this.body.find(".create-profile").on("click", function () {
        _self.createProfilePopup()
    });
};

_f.initRightPanel = function () {
    var _self = this;
    _self.body.find(".right-panel .panel-header .create-filter").on("click", function () {
        var _this = $(this);
        _self.addFilter(_this.parents(".right-panel").find(".body").attr("profile-id"));
    });
    _self.body.find(".right-panel .body .assigned-filter .remove").on("click", function () {
        var _this = $(this);
        _self.removeFilterFromProfile(_this.parents(".right-panel").find(".body").attr("profile-id"), _this.parents(".assigned-filter").attr("filter-id"));
    });
    app.tabs.filter._super.initRightPanel.apply(this, arguments);
};
//endregion

//region profile selection
_f.createProfilePopup = function (id, name) {
    var _self = this, title = id ? "edit.filter.profile" : "add.new.profile";
    bm.editPopup(app.baseUrl + 'filterAdmin/createProfileForm', $.i18n.prop(title), name , {id: id},  {
        width: 600,
        success: function(resp) {
            _self.reload()
        }
    });
};

_f.setProfileAsDefault = function (id) {
    bm.ajax({
        url: app.baseUrl + "filterAdmin/setDefaultFilterProfile",
        data: { id: id },
        success: function () {
            
        }
    });
};

_f.removeProfile = function (id) {
    var _self = this;
    bm.ajax({
        url: app.baseUrl + "filterAdmin/deleteFilterProfile",
        data: { id: id },
        success: function () {
            _self.reload()
        }
    });
};

_f.copyProfile = function (id) {
    var _self = this;
    bm.ajax({
        url: app.baseUrl + "filterAdmin/copyFilterProfile",
        data: { id: id },
        success: function () {
            _self.reload()
        }
    });
};

_f.removeFilterFromProfile = function (profileId, filterId) {
    var _self = this;
    bm.confirm($.i18n.prop("confirm.delete.filter.from.profile"), function() {
        bm.ajax({
            url: app.baseUrl + "filterAdmin/removeFilterFromProfile",
            data: {
                profileId: profileId,
                filterId: filterId
            },
            success: function () {
                _self.reload(true);
            }
        });
    }, function(){});
};
//endregion


//region filter selection
_f.addFilter = function (id) {
    var _self = this;
    bm.editPopup(app.baseUrl + "filterAdmin/addFilterPopup", $.i18n.prop("assign.filter"), null , {profile_id: id}, {
        width: 600,
        events: {
            content_loaded: function () {
                var _this = this;
                _this.find(".filter-tile-view").on("click", ".filter-tile", function () {
                    var $this = $(this);
                    if($this.hasClass("selected")) {
                        $this.removeClass("selected");
                        $this.find("[name='selected_filter']").remove();
                    }
                    else {
                        $this.addClass("selected");
                        $this.append($("<input type='hidden' name='selected_filter'/>").val($this.attr("entity-id")));
                    }
                });
            }
        },
        success: function () {
            _self.reload(true);
        }
    });
};
//endregion
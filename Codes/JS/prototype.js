Function.prototype.inherit = function (superFunction) {
    var mixins = [];
    if (arguments.length > 1) {
        mixins = Array.prototype.slice.call(arguments, 1);
    }
    var middleMan = function () {
    };
    middleMan.prototype = superFunction.prototype;
    var oldProto = this.prototype;
    var newProto = this.prototype = new middleMan();
    $.extend(newProto, bm.filterOwnProperties(oldProto));
    if (mixins.length) {
        var extendProto = newProto;
        middleMan = function () {
        };
        middleMan.prototype = newProto;
        newProto = this.prototype = new middleMan();
        mixins.every(function () {
            $.each(this.prototype || ($.isPlainObject(this) ? this : {}), function (k) {
                if (this.override) {
                    extendProto[k] = this;
                } else if (typeof this == "function" && typeof extendProto[k] == "function") {
                    extendProto[k] = this.blend(extendProto[k])
                } else {
                    extendProto[k] = this;
                }
            })
        });
        this._super = middleMan.prototype;
        this._super.constructor = function () {
            superFunction.apply(this, arguments);
            var _this = this;
            var _arg = arguments;
            mixins.every(function () {
                if (typeof this == "function") {
                    this.apply(_this, _arg)
                }
            })
        };
    } else {
        this._super = superFunction.prototype;
        this._super.constructor = superFunction;
    }
    newProto.constructor = this;
    return newProto;
};

/**
 * this() then extend()
 * @param extend
 * @returns {Function}
 */
Function.prototype.blend = function (extend) {
    var _extendFunc = this;
    var _protoFunc = extend;
    return function () {
        var oex = _extendFunc.order || 0;
        var opr = _extendFunc.order || 0;
        var to_return
        if (oex >= opr) {
            if (!_extendFunc.virtual || _protoFunc.virtual) {
                delete _extendFunc.original_return
                to_return = _extendFunc.apply(this, arguments)
            }
            if (!_protoFunc.virtual) {
                _protoFunc.original_return = to_return
                to_return = _protoFunc.apply(this, arguments)
            }
        } else {
            if (!_protoFunc.virtual || _extendFunc.virtual) {
                delete _protoFunc.original_return
                to_return = _protoFunc.apply(this, arguments)
            }
            if (!_extendFunc.virtual) {
                _extendFunc.original_return = to_return
                to_return = _extendFunc.apply(this, arguments)
            }
        }
        return to_return
    }
};

/**
 * extend() then this()
 * @param extend
 * @returns {Function}
 */
Function.prototype.intercept = function (extend) {
    var _this = this;
    return function () {
        extend.apply(this, arguments);
        return _this.apply(this, arguments)
    }
};

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (item) {
        var i = 0;
        var L = this.length;
        while (i < L) {
            if (this[i] === item) {
                return i;
            }
            ++i;
        }
        return -1;
    }
}

if (!Array.prototype.contains) {
    Array.prototype.contains = function (item) {
        return this.indexOf(item) > -1;
    }
}

if (browser.ie && browser.version <= 8) {
    var org_splice = Array.prototype.splice;
    Array.prototype.splice = function () {
        if (arguments.length == 1) {
            return org_splice.apply(this, [arguments[0], this.length - arguments[0]]);
        }
        return org_splice.apply(this, arguments);
    }
}

$.extend(Array.prototype, {
    and: function () {
        for (h in this) {
            if (!this[h]) {
                return false;
            }
        }
        return true;
    },
    /**
     * returns new collection using the value for given property of each entity/return value of calling function
     * @param prop {String | Function}
     * @returns {Array}
     */
    collect: function (prop) {
        var ret = [];
        if ($.isFunction(prop)) {
            $.each(this, function () {
                ret.push(prop.call(this))
            })
        } else {
            $.each(this, function () {
                ret.push(this[prop])
            })
        }
        return ret;
    },
    count: function (condition) {
        var found = 0;
        this.every(function () {
            if (eval(condition) === true) {
                found++;
            }
        });
        return found;
    },
    distinct: function () {
        var res = [];
        for (var i = 0; i < this.length; i++) {
            var itm = this[i];
            if ($.inArray(itm, res) === -1) {
                res.push(itm);
            }
        }
        return res;
    },
    every: function (loopHandler) {
        $.each(this, loopHandler)
    },
    /**
     * reverses of string exSplit process
     * @returns {String}
     */
    exJoin: function (joinChar) {
        joinChar = joinChar || ',';
        var result = "";
        this.every(function (i) {
            result += (i > 0 ? joinChar : '') + (this.indexOf(joinChar) > -1 || this.charAt(0) == "(" ? "(" + this + ")" : this);
        });
        return result;
    },
    native_filter: Array.prototype.filter || function (filter, _this) {
        var _self = this;
        _this = _this || window;
        var found = [];
        this.every(function (index) {
            if (filter.call(_this, this, index, _self) === true) {
                found.push(this);
            }
        });
        return found;
    },
    filter: function (filter) {
        if ($.isFunction(filter)) {
            return this.native_filter.apply(this, arguments)
        }
        var found = [];
        this.every(function () {
            if (eval(filter) === true) {
                found.push(this);
            }
        });
        return found;
    },
    /**
     * return boolean - whether found or not
     * @param condition [String | Function]
     * @returns {number} index of the entry that is matched
     */
    find: function (condition) {
        var foundIndex = -1;
        this.every(function (i) {
            if (($.isFunction(condition) ? condition.call(this) : eval(condition)) === true) {
                foundIndex = i;
                return false;
            }
        });
        return foundIndex;
    },
    intersect: function (array) {
        if (!array) {
            return this;
        }
        var result = [];
        var distinctArray = this.distinct();
        for (var i = 0; i < distinctArray.length; i++) {
            var item = distinctArray[i];
            var shouldAddToResult = true;
            for (var j = 0; j < arguments.length; j++) {
                var array2 = arguments[j];
                if (array2.length == 0) return [];
                if ($.inArray(item, array2) === -1) {
                    shouldAddToResult = false;
                    break;
                }
            }
            if (shouldAddToResult) {
                result.push(item);
            }
        }
        return result;
    },
    /**
     * returns first matched entry
     * @param condition
     * @returns {*} matched entry
     */
    iterate: function (iterator) {
        var c_ind = 0;
        var obj = this;
        var iter_caller = function () {
            if (c_ind == obj.length) {
                return;
            }
            iterator.call(obj[c_ind], {
                next: function () {
                    c_ind++;
                    setTimeout(iter_caller, 1)
                }
            }, c_ind)
        };
        iter_caller()
    },
    lookup: function (condition) {
        var found = null;
        this.every(function () {
            if (($.isFunction(condition) ? condition.call(this) : eval(condition)) === true) {
                found = this;
                return false;
            }
        });
        return found;
    },
    or: function () {
        for (h in this) {
            if (this[h]) {
                return true;
            }
        }
        return false;
    },
    pushAll: function (pushArray) {
        this.push.apply(this, pushArray)
    },
    /**
     * performs a reverse each
     * @param loopHandler {Function (index, reversedIndex, element) - context element}
     */
    reach: function (loopHandler) {
        var total = this.length - 1;
        for (var t = total; t >= 0; t--) {
            loopHandler.call(this[t], t, total - t, this[t])
        }
    },
    remove: function (entry) {
        var index = this.indexOf(entry);
        if (index > -1) {
            this.splice(index, 1);
        }
    },
    removeAll: function (filter) {
        var _self = this;
        this.filter(filter).every(function () {
            _self.remove(this)
        })
    },
    sum: function (iterator) {
        var count = this.length;
        var result = 0;
        if (bm.isString(iterator)) {
            iterator = new Function("return " + iterator)
        }
        for (var g = 0; g < count; g++) {
            result += iterator.call(this[g], g)
        }
        return result;
    }
});

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) === 0;
    };
}

if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (str) {
        var reducableCharCount = this.length - str.length;
        if (reducableCharCount < 0) {
            return false;
        }
        return this.substring(reducableCharCount) == str;
    };
}

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return $.trim(this);
    }
}

if (!String.prototype.contains) {
    String.prototype.contains = function (match) {
        return this.indexOf(match) > -1;
    }
}

$.extend(String.prototype, {
    capitalize: function () {
        return this.length == 0 ? "" : this[0].toUpperCase() + this.substring(1)
    },
    htmlEncode: function () {
        return this.replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/\r\n/g, '<br>')
            .replace(/\n/g, '<br>')
            .replace(/  /g, ' &nbsp;');
    },
    /**
     * if seperator charactor is expected in a value then that value must be enclosed within ( and ). e.g. for 1 and 2,3 and 4 - 1,(2,3),4
     */
    exSplit: function (seperatorChar) {
        seperatorChar = seperatorChar || ',';
        var params = [];
        var resultIndex = 0;
        var skipUntil = false;
        var bufferCount = 0;
        params[0] = "";
        for (var i = 0; i < this.length; i++) {
            var char = this.charAt(i);
            if (!skipUntil && char == seperatorChar) {
                params[++resultIndex] = "";
                bufferCount = 0;
            } else {
                if (char == '(' && !bufferCount) {
                    skipUntil = true;
                } else {
                    if (char == ')' && skipUntil && (i + 1 == this.length || this.charAt(i + 1) == seperatorChar)) {
                        skipUntil = false;
                    } else {
                        bufferCount++;
                        params[resultIndex] += char;
                    }
                }
            }
        }
        return params;
    },
    reader: function (shouldThroughError) {
        var index = 0;
        var _self = this;
        return {
            read: function () {
                var c = _self.charAt(index++);
                if (!c) {
                    if (shouldThroughError) {
                        throw "EOF"
                    }
                    return -1;
                }
                return c;
            }
        }
    },
    replaceAll: function (search, replace) {
        var reg = new RegExp(search, "g");
        return this.replace(reg, replace)
    },
    hash: function () {
        var hash = 0;
        if (this.length == 0) return hash;
        var i;
        for (i = 0; i < this.length; i++) {
            var ch = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + ch;
            hash = hash & hash;
        }
        return hash;
    },
    equals: function (c, matchCase) {
        if (arguments.length == 1) {
            matchCase = true;
        }
        var a = matchCase ? this + "" : this.toLowerCase();
        var b = matchCase ? c + "" : c.toLowerCase();
        return a == b;
    },
    int: function (value) {
        if (/^\s*\d+(.\d*)?\s*$/.test(value)) {
            return +value;
        }
        return 0;
    },
    bool: function () {
        if (/^\s*(true|yes)\s*$/.test(this)) {
            return true;
        }
        if (/^\s*(false|no)\s*$/.test(this)) {
            return false;
        }
        return false;
    },
    minusCase: function () {
        var writer = new StringWriter();
        var reader = this.reader();
        var data;
        while ((data = reader.read()) != -1) {
            if (data > '@' && data < '[') {
                writer.plus("-");
                writer.plus(data.toLowerCase())
            } else {
                writer.plus(data)
            }
        }
        return writer.toString()
    },
    dotCase: function () {
        var writer = new StringWriter();
        var reader = this.reader();
        var data;
        while ((data = reader.read()) != -1) {
            if (data > '@' && data < '[') {
                writer.plus(".");
                writer.plus(data.toLowerCase())
            } else {
                writer.plus(data)
            }
        }
        return writer.toString()
    },
    camelCase: function (initial) {
        var writer = new StringWriter();
        var reader = this.reader();
        var data;
        if (initial === undefined) {
            initial = true
        }
        var nextUpper = initial;
        while ((data = reader.read()) != -1) {
            if ((data > '`' && data < '{') || (data > '@' && data < '[') || (data > '/' && data < ':')) {
                if (nextUpper) {
                    writer.plus(data.toUpperCase());
                    nextUpper = false
                } else {
                    writer.plus(data)
                }
            } else {
                nextUpper = true
            }
        }
        return writer.toString()
    },
    byteCount: function () {
        var result = 0;
        for (var n = 0; n < this.length; n++) {
            var charCode = this.charCodeAt(n);
            if (charCode < 128) {
                result = result + 1;
                if (charCode == 10) {
                    result = result + 1;
                }
            } else if (charCode < 2048) {
                result = result + 2;
            } else if (charCode < 65536) {
                result = result + 3;
            } else if (charCode < 2097152) {
                result = result + 4;
            } else if (charCode < 67108864) {
                result = result + 5;
            } else {
                result = result + 6;
            }
        }
        return result;
    },
    textify: function () {
        return this.replace(/<[^>]*>/g, "")
    },
    sanitize: function () {
        return this.trim().toLowerCase().replace(/\s/g, "-").replace(/[^a-z0-9-\._]+/g, "-")
    }
});

var $_height = $.prototype.height;
var $_width = $.prototype.width;
var $_text = $.prototype.text;
var $_show = $.prototype.show;
var $_hide = $.prototype.hide;
$.extend($.prototype, {
    outer: function () {
        return this[0].outerHTML
    },
    show: function () {
        var showHandler;
        var afterShow = function () {
            $(this).find("*").each(function () {
                $(this).triggerHandler("show");
            });
            if (showHandler) {
                showHandler.apply(this, arguments)
            }
        };
        if (arguments.length == 1) {
            if ($.isPlainObject(arguments[0])) {
                showHandler = arguments[0].complete;
                arguments[0].complete = afterShow
            }
        }
        var argumentPos = -1;
        $.each(arguments, function (i) {
            if (typeof this == "function") {
                showHandler = this;
                argumentPos = i;
            }
        });
        if (argumentPos > -1) {
            arguments[argumentPos] = afterShow
        }
        $_show.apply(this, arguments);
        if (!showHandler) {
            afterShow.call(this[0])
        }
        return this;
    },
    hide: function () {
        $(this).find("*").each(function () {
            $(this).triggerHandler("hide");
        });
        return $_hide.apply(this, arguments)
    },
    /**
     * stores current display information and then hide. requires for cshow
     */
    chide: function () {
        return this.each(function () {
            var display = this.style ? this.style.display : undefined;
            $(this).data("display-cache", display).css({display: "none"});
        })
    },
    /**
     * restores previous display information that stored by chide
     */
    cshow: function () {
        return this.each(function () {
            var display = $(this).data("display-cache");
            this.style.display = display;
            $(this).removeData("display-cache");
        })
    },
    /**
     * removes inline display property
     */
    xshow: function () {
        return this.each(function () {
            this.style.display = ''
        })
    },
    acss: function (prop) {
        if (browser.ff || browser.gc) {
            try {
                return this.chide()[0].ownerDocument.defaultView.getComputedStyle(this[0])[prop]
            } finally {
                this.cshow()
            }
        } else {
            return this.css(prop)
        }
    },
    leafs: function () {
        return this.find("*").filter(function () {
            return $(this).children().length == 0
        })
    },
    triggerWithPropagation: function () {
        var eventName = arguments[0];
        var params = arguments[1];
        this.each(function () {
            var event = $.Event(eventName);
            event.target = this;
            var parents = $(this).parents().andSelf();
            var length = parents.length;
            for (var k = length - 1; k >= 0; k--) {
                if (parents.eq(k).triggerHandler(event, params) === false) {
                    return false;
                }
            }
        })
    },
    copyEvents: function (to, keepContext) {
        var node = this;
        var expando = this[0][$_priv_data.expando];
        if (expando) {
            var events = expando.events;
            if (events) {
                $.each(events, function (key, ev) {
                    $.each(ev, function () {
                        var _event = this;
                        var evKey = key;
                        if (this.namespace) {
                            evKey = key + "." + this.namespace;
                        }
                        var handler = keepContext ? function () {
                            if (_event.selector) {
                                _event.handler.apply(node.find(_event.selector), arguments)
                            } else {
                                _event.handler.apply(node, arguments)
                            }
                        } : this.handler;
                        if (this.selector) {
                            to.on(evKey, this.selector, handler);
                        } else {
                            to.on(evKey, handler);
                        }
                    })
                })
            }
        }
    },
    copyDeepEvents: function (to, keepContext) {
        this.copyEvents(to, keepContext);
        function repeat(from, to) {
            var toChildren = to.children();
            var thisChildren = from.children();
            if (toChildren.length == thisChildren.length) {
                thisChildren.each(function (ind) {
                    $(this).copyEvents(toChildren.eq(ind), keepContext);
                    repeat($(this), toChildren.eq(ind))
                })
            }
        }

        repeat(this, to);
    },
    changeTag: function (tag) {
        var replacement = $('<' + tag + '>');
        var attributes = {};
        $.each(this.get(0).attributes, function (index, attribute) {
            attributes[attribute.name] = attribute.value;
        });
        replacement.attr(attributes);
        replacement.data(this.data());
        var contents = this.children().clone(true, true);
        replacement.append(contents);
        this.replaceWith(replacement);
        return replacement;
    },
    /**
     * extends jquery width function to set width considering element padding, margin
     */
    width: function (val/*Number(optional)*/, isOuter/*Boolean(optional)*/, includesMargin/*Boolean(optional)*/) {
        if (arguments.length < 2 || !(isOuter || includesMargin)) {
            return $_width.apply(this, arguments);
        } else {
            var reduceWidth = this.outerWidth(includesMargin || false) - this.width();
            return $_width.call(this, val - reduceWidth);
        }
    },
    /**
     * extends jquery height function to set height considering element padding, margin
     */
    height: function (val/*Number(optional)*/, isOuter/*Boolean(optional)*/, includesMargin/*Boolean(optional)*/) {
        if (arguments.length < 2 || !(isOuter || includesMargin)) {
            return $_height.apply(this, arguments);
        } else {
            var reduceHeight = this.outerHeight(includesMargin || false) - this.height();
            return $_height.call(this, val - reduceHeight);
        }
    },
    topRib: function (margin, border, padding) {
        return this.dirRib("top", margin, border, padding)
    },
    bottomRib: function (margin, border, padding) {
        return this.dirRib("bottom", margin, border, padding)
    },
    leftRib: function (margin, border, padding) {
        return this.dirRib("left", margin, border, padding)
    },
    rightRib: function (margin, border, padding) {
        return this.dirRib("right", margin, border, padding)
    },
    dirRib: function (dir, margin, border, padding) {
        var doList = [];
        if (padding === undefined) {
            padding = true;
        }
        if (border === undefined) {
            border = true;
        }
        if (margin === undefined) {
            margin = true;
        }
        if (padding) {
            doList.push("padding")
        }
        if (border) {
            doList.push("border")
        }
        if (margin) {
            doList.push("margin")
        }
        var num = function (value) {
            try {
                return parseInt(value, 10)
            } catch (t) {
                return 0
            }
        };
        var _this = this;
        return doList.sum(function () {
            return num(_this.css(this + '-' + dir + (this == 'border' ? '-width' : '')))
        })
    },
    /**
     * extends jquery text function. It makes a space preceeded by space as &nbsp;
     */
    text: function (text) {
        if (typeof text == "string" && (text.indexOf("\n") != -1 || text.indexOf("  ") != -1)) {
            text = text.htmlEncode();
            return this.each(function () {
                this.innerHTML = text;
            })
        }
        return $_text.apply(this, arguments);
    },
    /**
     * removes input restrictions from a input element
     */
    free: function () {
        return this.each(function () {
            var input = $(this);
            if (!input.is("input")) {
                return;
            }
            input.unbind("keydown.restrict")
        });
    },
    /**
     * makes a input only positive numeric values supported
     */
    numeric: function () {
        return this.each(function () {
            $(this).bind("keydown.restrict", function (e) {
                var key = e.keyCode;
                if (e.shiftKey) {
                    return browser.key.isArrows(key);
                }
                return key == browser.key.BACKSPACE || key == browser.key.TAB || key == browser.key.DELETE || browser.key.isArrows(key) || browser.key.isDigit(key) || (e.ctrlKey && (browser.key.is('a', key) || browser.key.is('A', key) || browser.key.is('c', key) || browser.key.is('v', key) || browser.key.is('x', key) || browser.key.is('C', key) || browser.key.is('V', key) || browser.key.is('X', key)))
            }).on("paste", function () {
                var _self = this;
                var oldValue = this.value;
                setTimeout(function () {
                    var newValue = _self.value;
                    if (isNaN(newValue) || newValue.contains(".") || newValue < 0) {
                        _self.value = oldValue
                    }
                }, 0)
            })
        });
    },
    /**
     * makes a input only signed numeric values supported
     */
    signed_numeric: function () {
        return this.each(function () {
            var input = $(this);
            input.bind("keydown.restrict", function (e) {
                var key = e.keyCode;
                if (e.shiftKey) {
                    return browser.key.isArrows(key);
                }
                if (key == browser.key.MINUS || key == browser.key.NUM_MINUS) {
                    if (input.val().charAt(0) != "-") {
                        input.val("-" + input.val())
                    }
                    return false;
                }
                return key == browser.key.BACKSPACE || key == browser.key.TAB || key == browser.key.DELETE || browser.key.isArrows(key) || browser.key.isDigit(key) || (e.ctrlKey && (browser.key.is('a', key) || browser.key.is('A', key) || browser.key.is('c', key) || browser.key.is('v', key) || browser.key.is('x', key) || browser.key.is('C', key) || browser.key.is('V', key) || browser.key.is('X', key)))
            }).on("paste", function () {
                var _self = this;
                var oldValue = this.value;
                setTimeout(function () {
                    var newValue = _self.value;
                    if (isNaN(newValue) || newValue.contains(".")) {
                        _self.value = oldValue
                    }
                }, 0)
            })
        });
    },
    /**
     * makes a input only positive decimal values supported
     */
    decimal: function () {
        return this.each(function () {
            var input = $(this);
            input.bind("keydown.restrict", function (e) {
                var key = e.keyCode;
                if (e.shiftKey) {
                    return browser.key.isArrows(key);
                }
                if (key == browser.key.POINT || key == browser.key.NUM_POINT) {
                    return input.val().indexOf(".") == -1
                }
                return key == browser.key.BACKSPACE || key == browser.key.TAB || key == browser.key.DELETE || browser.key.isArrows(key) || browser.key.isDigit(key) || (e.ctrlKey && (browser.key.is('a', key) || browser.key.is('A', key) || browser.key.is('c', key) || browser.key.is('v', key) || browser.key.is('x', key) || browser.key.is('C', key) || browser.key.is('V', key) || browser.key.is('X', key)))
            }).on("paste", function () {
                var _self = this;
                var oldValue = this.value;
                setTimeout(function () {
                    if (isNaN(_self.value)) {
                        _self.value = oldValue
                    }
                }, 0)
            });
        })
    },
    signed_decimal: function () {
        return this.each(function () {
            var input = $(this);
            input.bind("keydown.restrict", function (e) {
                var key = e.keyCode;
                if (e.shiftKey) {
                    return browser.key.isArrows(key);
                }
                if (key == browser.key.MINUS || key == browser.key.NUM_MINUS) {
                    if (input.val().charAt(0) != "-") {
                        input.val("-" + input.val())
                    }
                    return false;
                }
                if (key == browser.key.POINT || key == browser.key.NUM_POINT) {
                    return input.val().indexOf(".") == -1
                }
                return key == browser.key.BACKSPACE || key == browser.key.TAB || key == browser.key.DELETE || browser.key.isArrows(key) || browser.key.isDigit(key) || (e.ctrlKey && (browser.key.is('a', key) || browser.key.is('A', key) || browser.key.is('c', key) || browser.key.is('v', key) || browser.key.is('x', key) || browser.key.is('C', key) || browser.key.is('V', key) || browser.key.is('X', key)))
            }).on("paste", function () {
                var _self = this;
                var oldValue = this.value;
                setTimeout(function () {
                    if (isNaN(_self.value)) {
                        _self.value = oldValue
                    }
                }, 0)
            });
        })
    },
    swap: function (to) {
        var aParent = this.parent();
        var aIndex = this.index();
        var bParent = to.parent();
        var bIndex = to.index();
        if (aParent[0] == bParent[0]) {
            if (aIndex < bIndex) {
                this.before(to);
                var eAtPos = bParent.find(">*:eq(" + bIndex + ")");
                if (eAtPos[0] == this[0]) {
                    return;
                }
                if (eAtPos.length) {
                    eAtPos.after(this);
                } else {
                    bParent.append(this);
                }
            } else {
                this.after(to);
                var eAtPos = bParent.find(">*:eq(" + bIndex + ")");
                if (eAtPos[0] == this[0]) {
                    return;
                }
                if (eAtPos.length) {
                    eAtPos.before(this);
                } else {
                    bParent.append(this);
                }
            }
        } else {
            this.before(to);
            var eAtPos = bParent.find(">*:eq(" + bIndex + ")");
            if (eAtPos.length) {
                eAtPos.before(this);
            } else {
                bParent.append(this);
            }
        }
    },
    ichange: function (pauseDuration, handler) {
        return this.on("ichange", pauseDuration, handler);
    },
    obj: function (type) {
        var data = this.data();
        if (!data) {
            return null;
        }
        if (typeof type == "string") {
            return data[type];
        }
        if (!$.isFunction(type)) {
            type = null;
        }
        var obj = null;
        $.each(data, function (key) {
            if (type) {
                if (this instanceof type) {
                    obj = this;
                    return false;
                }
                return true;
            }
            if (key.endsWith("Inst")) {
                obj = this;
                return false;
            }
            if (key.startsWith("wcui")) {
                obj = this;
                return false;
            }
        });
        return obj;
    },
    config: function (type, updates) {
        var cacheKey = "attr-parsed-cache#" + type;
        var map = this.data(cacheKey);
        if (!map) {
            map = {};
            this.data(cacheKey, map);
            var cutLength = type.length + 1, autoTypeExclude = this.attr("auto-type-exclude");
            autoTypeExclude = autoTypeExclude ? autoTypeExclude.split(",") : [];
            $.each(this[0].attributes, function () {
                if (this.name.startsWith(type + "-")) {
                    var key = this.name.substring(cutLength);
                    map[key] = autoTypeExclude.indexOf(key) != -1 ? this.value : bm.autoType(this.value);
                }
            })
        }
        if (updates) {
            if (typeof updates == "string") {
                return map[updates]
            } else {
                var obj = $(this);
                $.each(updates, function (k, v) {
                    map[k] = v;
                    obj.attr(type + "-" + k, v)
                });
                return this;
            }
        }
        return map;
    },
    //analogous to prop function - analogy created to be used for custom properties
    store: function (name, value) {
        if (arguments.length == 2) {
            this.each(function () {
                this[name] = value
            });
            return this;
        }
        return this.length ? this[0][name] : null;
    },
    pairuncheck: function () {
        this.after(function () {
            var check = $(this);
            var input = $("<input style='display: none' class='single' type='checkbox' name='" + check.attr("name") + "' value='" + check.attr("uncheck-value") + "'>");
            check.bind("change.form", function () {
                if (!this.checked) {
                    input[0].checked = true;
                } else {
                    input[0].checked = false;
                }
            });
            check.triggerHandler("change.form");
            return input;
        });
    },
    /**
     * takes data from an form and generates an json object using those data
     * */
    serializeObject: function () {
        var o = {};
        var a = (this.is("form") ? this : this.find(":input")).serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!$.isArray(o[this.name])) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    },
    sortablecolumn: function (config) {
        var defaults = {
            empty: "always-below"
        };

        var functions = {
            resetSortState: function () {
                return this.removeAttr("sort-dir").removeClass("sort-down").removeClass("sort-up");
            }
        };

        if (typeof config == "string") {
            return functions[config].call(this);
        }

        $.extend(config, defaults);

        function sortIt(th, dir) {
            var index = th.index();
            var table = th.closest("table");
            var rows = table.find("tr").not(":first");
            rows.sort(function (a, b) {
                var keyA = $('td:eq(' + index + ')', a).text().trim().toUpperCase();
                var keyB = $('td:eq(' + index + ')', b).text().trim().toUpperCase();
                if (keyA == keyB) {
                    return 0;
                }
                if (keyA == "") {
                    if (config.empty == "always-below") {
                        return 1;
                    } else if (config.empty == "always-above") {
                        return -1;
                    }
                }
                if (keyB == "") {
                    if (config.empty == "always-below") {
                        return -1;
                    } else if (config.empty == "always-above") {
                        return 1;
                    }
                }
                if ((dir == "up" && keyA > keyB) || (dir == "down" && keyA < keyB)) {
                    return 1;
                }
                return -1;
            });
            table.append(rows.remove());
        }

        var th = $(this);
        th.addClass("sortable").click(function () {
            var _self = $(this);
            var dir = _self.attr("sort-dir") || "down";
            if (dir == "down") {
                _self.attr("sort-dir", dir = "up");
                _self.addClass("sort-up").removeClass("sort-down");
            } else {
                _self.attr("sort-dir", dir = "down");
                _self.addClass("sort-down").removeClass("sort-up");
            }
            sortIt(_self, dir);
            _self.closest("table").trigger("sort");
            _self.closest("table").trigger("change");
        });
        var table = th.closest("table");
        table.bind("row-update", function () {
            var _self = $(this).find("th.sortable");
            _self.each(function () {
                var th = $(this);
                var dir = th.attr("sort-dir");
                if (dir) {
                    sortIt(th, dir);
                }
            });
            _self.trigger("sort");
            table.trigger("change")
        });
        return this;
    },
    reset: function () {
        this.each(function () {
            var input = $(this);
            var next = this.nextSibling;
            var parent = input.parent();
            var form = $("<form></form>");
            form.append(input);
            form[0].reset();
            if (next) {
                $(next).before(input);
            } else {
                parent.append(input);
            }
        })
    },
    //works only for single namespace
    isBound: function (evType, namespace) {
        var expando = this[0][$_priv_data.expando];
        if (!expando) {
            return false;
        }
        var events = expando.events;
        var matchedEvents;
        var matched = false;
        if (events && (matchedEvents = events[evType])) {
            $.each(matchedEvents, function () {
                if (!namespace || this.namespace == namespace) {
                    matched = true;
                }
                return !matched;
            })
        }
        return matched;
    },
    isChildOf: function (parent) {
        return this.closest(parent).length > 0;
    },
    isParentOf: function (child) {
        return this.find(child).length > 0;
    },
    loader: function (show) {
        if (show && typeof show == "string") {
            this.addClass(show);
        }
        if (show === false) {
            this.unmask()
        } else {
            bm.mask(this, '<div><span class="loader"></span></div>');
        }
        return this
    },
    mask: function (maskHtml) {
        maskHtml = maskHtml ? maskHtml : "<div></div>";
        bm.mask(this, maskHtml);
    },
    unmask: function () {
        this.removeClass("updating");
        this.find(".div-mask").detach();
    },
    clearCache: function (onClear) {
        if (!this.is("img")) {
            throw $.error("This function is only applicable for img tag")
        }
        var url = this.attr("src");
        var _self = this;
        var iframe = $("<iframe style='position: absolute; left: -20000px; top: -20000px;'></iframe>");
        iframe.appendTo(document.body);
        iframe.one("load", function () {
            iframe.one("load", function () {
                iframe.remove();
                _self.attr("src", url);
                if (onClear) {
                    onClear()
                }
            })[0].contentWindow.location.reload();
        }).attr("src", url).bind("error", function () {
            iframe.remove();
        });
    },
    scrollHere: function (scroller) {
        if (!scroller) {
            var _document = this[0].ownerDocument;
            scroller = bm.getTopScroller(_document)
        }
        var top = scroller.scrollTop();
        var left = scroller.scrollLeft();
        var to_offset = this.offset();
        var to_top = to_offset.top;
        var to_left = to_offset.left;
        var height = scroller.height();
        var width = scroller.width();
        var top_dist = top - to_top;
        var bottom_dist = to_top - top - height;
        if (top_dist > 0) {
            $(scroller).animate({scrollTop: to_top - Math.min(height / 3, 150)}, 800)
        } else if (bottom_dist > 0) {
            $(scroller).animate({scrollTop: to_top - Math.max(2 * height / 3, height - 150)}, 800)
        }
        var left_dist = left - to_left;
        var right_dist = to_left - left - width;
        if (left_dist > 0) {
            $(scroller).animate({scrollTop: to_left - Math.min(width / 3, 100)}, 800)
        } else if (right_dist > 0) {
            $(scroller).animate({scrollTop: to_left - Math.max(2 * width / 3, width - 150)}, 800)
        }
        return this;
    },
    tip: function (config) {
        if (!config) {
            config = {};
        }
        this.mouseenter(function (ev) {
            $.global_tip_mouseevent_reference = ev;
            var item = $(this);
            var _config = $.extend({}, config);
            _config.text = _config.text || item.attr("tip-text") || item.attr("title");
            item.removeAttr("title");
            item.trigger("tip-show", [_config]);
            $.global_tip_timer_reference = setTimeout(function () {
                if ($.global_tip_inst_reference) {
                    $.global_tip_inst_reference.close()
                }
                $.global_tip_timer_reference = null;
                $.global_tip_inst_reference = new POPUP({
                    content: _config.text,
                    template: "<div><div class='content'></div></div>",
                    clazz: "tip-popup",
                    modal: false,
                    is_always_up: true,
                    auto_active: false,
                    auto_active_on_focus: false,
                    ui_position: {
                        my: "left bottom",
                        at: "right top",
                        of: $.global_tip_mouseevent_reference
                    }
                });
                if (typeof config.render === 'function') {
                    config.render($.global_tip_inst_reference.content)
                }
            }, _config.delay || 350)
        }).mouseleave(function (ev) {
            var pop = $.global_tip_inst_reference;
            if (pop && config.sustain_on_hover) {
                var popdom = pop.getDom();
                var offset = popdom.offset();
                offset.right = offset.left + popdom.outerWidth();
                offset.bottom = offset.top + popdom.outerHeight();
                if (ev.pageX > offset.left && ev.pageX < offset.right && ev.pageY > offset.top && ev.pageY < offset.bottom) {
                    pop.getDom().mouseleave(function () {
                        pop.close();
                        $.global_tip_inst_reference = null;
                    });
                    return;
                }
            }
            if ($.global_tip_timer_reference) {
                clearTimeout($.global_tip_timer_reference);
                $.global_tip_timer_reference = null;
            }
            if (pop) {
                pop.close();
                $.global_tip_inst_reference = null;
            }
        }).mousemove(function (ev) {
            $.global_tip_mouseevent_reference = ev
        });
        return this;
    },
    replaceClass: function (a, b) {
        return this.removeClass(a).addClass(b)
    },
    flash: function () {
        function Flash(el) {
            this.opacity = 0;
            this.el = el
        }

        Flash.prototype.start = function () {
            var _self = this;
            this.interval = setInterval(function () {
                _self.el.animate({opacity: _self.opacity}, 600);
                _self.opacity = _self.opacity ? 0 : 1
            }, 600);
            this.isFlashing = true
        };
        Flash.prototype.stop = function () {
            if (this.isFlashing) {
                var _self = this;
                this.opacity = 1;
                clearInterval(_self.interval);
                this.el.animate({opacity: 1}, 600);
                this.isFlashing = false
            }
        };
        var flash = this.data("flash-inst");
        if (!flash) {
            flash = new Flash(this);
            this.on("click.flash", function () {
                if (flash.isFlashing) {
                    flash.stop();
                }
            });
            this.on("remove", function () {
                flash.stop();
            });
            this.data("flash-inst", flash);
        }
        if (!flash.isFlashing) {
            flash.start();
        }
    },
    /**
     * @param offset [boolean | {left: [number], top: [number]}] if boolean then it will be considered as includeMargin
     * @param includeMargin
     * @returns {*}
     */
    rect: function (offset, includeMargin) {
        var rect = $.extend({}, this[0].getBoundingClientRect());
        var _document = this[0].ownerDocument;
        var scroller = $(/webkit/i.test(navigator.userAgent) || _document.compatMode == 'BackCompat' ? _document.body : _document.documentElement);
        var s_top = scroller.scrollTop();
        var s_left = scroller.scrollLeft();
        rect.top += s_top;
        rect.left += s_left;
        rect.bottom += s_top;
        rect.right += s_left;
        if (arguments.length == 0) {
            return rect
        }
        if (offset instanceof Boolean || typeof offset == "boolean") {
            includeMargin = offset;
            offset = undefined
        }
        if (offset instanceof $) {
            offset = offset.offset();
            offset.left *= -1;
            offset.top *= -1
        }
        if (offset) {
            rect.left += offset.left;
            rect.top += offset.top;
            rect.right += offset.left;
            rect.bottom += offset.top
        }
        if (includeMargin) {
            var marginL = parseInt(this.css("margin-left"));
            var marginR = parseInt(this.css("margin-right"));
            var marginT = parseInt(this.css("margin-top"));
            var marginB = parseInt(this.css("margin-bottom"));
            rect.left -= marginL;
            rect.top -= marginT;
            rect.right += marginR;
            rect.bottom += marginB;
            rect.width += marginL + marginR;
            rect.height += marginT + marginB
        }
        return rect
    },
    pposition: function (which, size) {
        var pos = this.position();
        if (which == "left" || which == "top") {
            return pos[which]
        }
        var parent = $(this[0].offsetParent);
        if (!which || which == "right") {
            pos.right = parent.innerWidth() - pos.left - this.outerWidth(true)
        }
        if (!which || which == "bottom") {
            pos.bottom = parent.innerHeight() - pos.top - this.outerHeight(true)
        }
        if ((!which && size) || which == "width" || which == "height") {
            if (which) {
                pos[which] = this["outer" + which.capitalize()](true)
            } else {
                pos.width = this.outerWidth(true);
                pos.height = this.outerHeight(true)
            }
        }
        return which ? pos[which] : pos
    },
    intercept: function (ev, handler) {
        return this.on("before_" + ev, handler)
    },
    hasparent: function (selector) {
        return this.parent().is(selector)
    },
    htmlCodeEditor: function () {
        var $this = this, editor, mixedMode = {
            name: "htmlmixed",
            scriptTypes: [{
                matches: /\/x-handlebars-template|\/x-mustache/i,
                mode: null
            },
                {
                    matches: /(text|application)\/(x-)?vb(a|script)/i,
                    mode: "vbscript"
                }]
        };
        return CodeMirror.fromTextArea($this[0], {
            lineNumbers: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
            mode: mixedMode,
            theme: "eclipse"
        });

    },
    cssCodeEditor: function () {
        var $this = this;
        return CodeMirror.fromTextArea($this[0], {
            lineNumbers: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
            mode: "css",
            theme: "eclipse",
            highlightSelectionMatches: {showToken: /\w/},
            showCursorWhenSelecting: true,
            matchBrackets: true,
            autoCloseBrackets: true
        });
    },
    addAttrProp: function (attr, prop, newProp) {
        if (arguments.length < 2 || !this.is('[' + attr + ']')) {
            return this;
        } else if (arguments.length > 2) {
            return this.removeAttrProp(attr, prop).addAttrProp(attr, newProp);
        }
        var cache = this.attr(attr);
        if (cache.toLowerCase().contains(prop.toLowerCase())) {
            return this;
        }
        this.attr(attr, (cache + " " + prop).trim());
        return this;
    },
    removeAttrProp: function (attr, prop) {
        if (arguments.length < 2 || !this.is('[' + attr + ']')) {
            return this;
        }
        var cache = this.attr(attr);
        this.attr(attr, cache.replaceAll(prop, function (replace, idx, str) {
            var prev = idx ? str.substring(0, idx) : "";
            var nxt = str.substring(idx + replace.length, str.length);
            if (!(prev.contains("[") && nxt.contains("]"))) {
                return "";
            }
            return replace;
        }).trim());
        return this;
    },
    cleanWhitespace: function () {
        this.contents().filter(function () {
            if (this.nodeType != 3) {
                $(this).cleanWhitespace();
                return false;
            }
            else {
                this.textContent = $.trim(this.textContent);
                return !/\S/.test(this.nodeValue);
            }
        }).remove();
        return this;
    },
    positionFromBody: function () {
        var el = this.get(0);
        var _x = 0;
        var _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft + el.clientLeft;
            _y += el.offsetTop - el.scrollTop + el.clientTop;
            el = el.offsetParent;
        }
        return {top: _y, left: _x};
    },
    positionFromGivenElement: function (jRelativeEl) {
        var el = this.get(0);
        var relativeEl = jRelativeEl.get(0);
        var _x = 0;
        var _y = 0;
        while (el && el != relativeEl && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft + el.clientLeft;
            _y += el.offsetTop - el.scrollTop + el.clientTop;
            el = el.offsetParent;
        }
        return {top: _y, left: _x};
    },
    positionOffsetFromRelative: function () {
        var el = this.get(0);
        var _x = 0;
        var _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft + el.clientLeft;
            _y += el.offsetTop - el.scrollTop + el.clientTop;
            el = el.offsetParent;
            if (el != null) {
                if (getComputedStyle !== 'undefined')
                    valString = getComputedStyle(el, null).getPropertyValue('position');
                else
                    valString = el.currentStyle['position'];
                if (valString === "relative")
                    el = null;
            }
        }
        return {top: _y, left: _x};
    }
});

bm.parent_remove_handler = [];
$(document).on("DOMNodeRemoved", function (ev) {
    var elm = ev.target;
    bm.parent_remove_handler.every(function () {
        if ($(elm).find(this.element).length) {
            if (this.selector) {
                var _self = this;
                $(this.element).find(this.selector).each(function () {
                    _self.handler.call(this, ev)
                })
            } else {
                this.handler.call(this.element, ev)
            }
        }
    })
});

bm.jquery_data_clean_handler = [];
var original_clean = $.cleanData;
$.cleanData = function (elements, acceptData) {
    if (!(elements instanceof Array)) {
        elements = $.makeArray(elements)
    }
    bm.jquery_data_clean_handler.every(function () {
        if (elements.contains(this.element)) {
            if (this.selector) {
                var _self = this;
                $(this.element).find(this.selector).each(function () {
                    _self.handler.call(this, $.Event("jqclean"))
                })
            } else {
                this.handler.call(this.element, $.Event("jqclean"))
            }
        }
    });
    original_clean.apply(this, arguments, acceptData)
};

$.extend($.event.special, {
    //if multiple handler is bound then duration for successive handlers will be ignored
    ichange: {
        add: function (obj) {
            var selectors = obj.selector ? obj.selector.split(",") : ["no-selector"];
            var pauseDuration = obj.data || 400;
            var _self = $(this);
            var namespaces = obj.namespace ? obj.namespace.split(".") : ["no-namespace"];
            var focused;

            $.each(selectors, function (index, selector) {
                selector = selector.trim();
                var timer;
                var initialValue;

                var boundCollection = _self.data("ichange-bound");
                if (!boundCollection) {
                    boundCollection = {};
                    boundCollection[selector] = namespaces;
                    _self.data("ichange-bound", boundCollection)
                } else if (boundCollection[selector] != null) {
                    $.each(namespaces, function () {
                        boundCollection[selector].push(this)
                    });
                    return;
                }

                if (selector == "no-selector") {
                    selector = null;
                }

                function checkForChange() {
                    var input = selector ? _self.find(selector) : _self;
                    var currentValue = input.val();
                    if (currentValue != initialValue) {
                        var prevValue = initialValue;
                        initialValue = currentValue;
                        setTimeout(function () {
                            if (selector) {
                                _self.triggerHandler({type: "ichange", target: input[0]}, [currentValue, prevValue])
                            } else {
                                _self.triggerHandler("ichange", [currentValue, prevValue])
                            }
                        }, 5);
                    }
                }

                function startTimer() {
                    if (timer) {
                        clearInterval(timer);
                    }
                    timer = setInterval(checkForChange, pauseDuration);
                }

                var focusHandler = function () {
                    initialValue = this.value;
                    focused = this;
                    startTimer();
                };
                var blurHandler = function () {
                    if (timer) {
                        clearInterval(timer);
                    }
                    focused = null;
                    checkForChange();
                };
                var changeHandler = function (e, v1, v2) {
                    if (this == focused) {
                        return;
                    }
                    if (selector) {
                        _self.triggerHandler({type: "ichange", target: this}, [this.value, v2])
                    } else {
                        _self.triggerHandler("ichange", [this.value, v2])
                    }
                };

                if (selector) {
                    _self.on("focus.ichange-event", selector, focusHandler);
                    _self.on("blur.ichange-event", selector, blurHandler);
                    _self.on("keydown.ichange-event", selector, startTimer);
                    _self.on("change.ichange-event", selector, changeHandler);
                } else {
                    _self.on("focus.ichange-event", focusHandler);
                    _self.on("blur.ichange-event", blurHandler);
                    _self.on("keydown.ichange-event", startTimer);
                    _self.on("change.ichange-event", changeHandler);
                    _self.each(function () {
                        this.isilent = function (value) {
                            if (this == focused) {
                                initialValue = value
                            }
                            this.value = value;
                            return $(this)
                        }
                    })
                }
            })
        },
        remove: function (obj) {
            var selectors = obj.selector ? obj.selector.split(",") : ["no-selector"];
            var _self = $(this);
            var modifiedSelectors = "";
            $.each(selectors, function (index, selector) {
                selector = selector.trim();
                var boundCollection = _self.data("ichange-bound");
                if (!boundCollection) {

                } else if (boundCollection[selector] && obj.namespace) {
                    var namespaces = obj.namespace.split(".");
                    var addedNamespaces = boundCollection[selector];
                    $.each(namespaces, function (index, namespace) {
                        var indices = [];
                        $.each(addedNamespaces, function (_index) {
                            if (namespace == this) {
                                indices.push(_index);
                            }
                        });
                        $.each(indices, function (n) {
                            boundCollection[selector].splice(this - n, 1);
                        })
                    });
                    if (boundCollection[selector].length == 0) {
                        modifiedSelectors += " " + selector;
                        delete boundCollection[selector]
                    }

                } else {
                    modifiedSelectors += " " + selector
                }
            });
            if (modifiedSelectors == "") {

            } else if (modifiedSelectors != "no-selector") {
                _self.off("focus.ichange-event", modifiedSelectors);
                _self.off("blur.ichange-event", modifiedSelectors);
                _self.off("keydown.ichange-event", modifiedSelectors);
            } else {
                _self.off("focus.ichange-event");
                _self.off("blur.ichange-event");
                _self.off("keydown.ichange-event");
            }
        }
    },
    remove: {
        add: function (obj) {
            $(this).on("DOMNodeRemoved" + (obj.namespace ? "." + obj.namespace : ""), obj.selector, obj.handler);
            bm.parent_remove_handler.push({
                element: this,
                namespace: obj.namespace,
                selector: obj.selector,
                handler: obj.handler
            })
        },
        remove: function (obj) {
            var _self = this;
            $(this).off("DOMNodeRemoved" + (obj.namespace ? "." + obj.namespace : ""), obj.selector, obj.handler);
            bm.parent_remove_handler.removeAll(function (handle) {
                return _self == handle.element && (!obj.namespace || obj.namespace == handle.namespace) && (!obj.selector || obj.selector == handle.selector) && (!obj.handler || obj.handler == handle.handler)
            })
        }
    },
    jqclean: {
        add: function (obj) {
            bm.jquery_data_clean_handler.push({
                element: this,
                namespace: obj.namespace,
                selector: obj.selector,
                handler: obj.handler
            })
        },
        remove: function (obj) {
            var _self = this;
            bm.jquery_data_clean_handler.removeAll(function (handle) {
                return _self == handle.element && (!obj.namespace || obj.namespace == handle.namespace) && (!obj.selector || obj.selector == handle.selector) && (!obj.handler || obj.handler == handle.handler)
            })
        }
    }
});

for (var k in Array.prototype) {
    if (!$.prototype[k]) {
        $.prototype[k] = Array.prototype[k]
    }
}

var _to_fixed = Number.prototype.toFixed;
$.extend(Number.prototype, {
    /**
     * removes trailing 0s from fixed values if true is passed as trimmed
     */
    toFixed: function (count, trimmed) {
        var fixed = _to_fixed.call(this, count);
        if (trimmed) {
            return fixed.replace(/\.?0+$/, '');
        }
        return fixed;
    },
    /**
     * considers value as byte and returns its representation in KB/MB/GB whichever convenient
     */
    toByteNotation: function () {
        if (this < 1024) {
            return this.toFixed(3, true) + " B";
        }
        var kilo = this / 1024;
        if (kilo < 1024) {
            return kilo.toFixed(3, true) + " KB";
        }
        var mega = kilo / 1024;
        if (mega < 1024) {
            return mega.toFixed(3, true) + " MB";
        }
        var giga = mega / 1024;
        return giga.toFixed(3, true) + " GB";
    }
});

function StringWriter() {
    this.buffer = [];
}

StringWriter.prototype.plus = function (j) {
    this.buffer.push(j);
    return this;
};

StringWriter.prototype.toString = function () {
    return this.buffer.join("");
};

Date.prototype.gmt = function () {
    var gmtDate = new Date(this.getTime() + (this.getTimezoneOffset() * 60000));
    return gmtDate;
};

Date.prototype.toZone = function () {
    var gmtDate = new Date(this.getTime() - (this.getTimezoneOffset() * 60000));
    return gmtDate;
};

Object.defineProperty(String.prototype, "jq", {
    get: function () {
        return $("" + this)
    }
});

Object.defineProperty(Element.prototype, "jq", {
    get: function () {
        return $(this)
    }
});

var sizzle_expando = Object.keys($.expr.pseudos.not)[0];
$.extend($.expr.pseudos, {
    hasparent: (function () {
        var fn = function (selector) {
            return function (elem) {
                return $(elem.parentNode).is(selector);
            }
        };
        fn[sizzle_expando] = true;
        return fn;
    })()
});
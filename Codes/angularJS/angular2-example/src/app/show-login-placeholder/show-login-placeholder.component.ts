import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-login-placeholder',
  templateUrl: './show-login-placeholder.component.html',
  styleUrls: ['./show-login-placeholder.component.css']
})
export class ShowLoginPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

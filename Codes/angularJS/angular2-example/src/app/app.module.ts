import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ShowMessageComponent } from './show-message/show-message.component';
import { ShowLoginPlaceholderComponent } from './show-login-placeholder/show-login-placeholder.component';

@NgModule({
  declarations: [
    AppComponent,
    ShowMessageComponent,
    ShowLoginPlaceholderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

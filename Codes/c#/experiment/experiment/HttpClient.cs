﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace experiment {
    class HttpClient {
        public int responseStatusCode;
        private WebRequest webRequest;
        private WebResponse webResponse;

        public void setProxy(String proxyString) {
            try {
                this.webRequest.Proxy = new WebProxy( proxyString, false );
            } catch ( Exception ex ) {
                Console.WriteLine( "Proxy Error : " + ex.Message );
            }
        }

        public String HttpGet (String uri) {
            try {
                this.webRequest = WebRequest.Create( uri );
                this.webResponse = this.webRequest.GetResponse();
                this.responseStatusCode = (int)(( HttpWebResponse ) this.webResponse ).StatusCode;
                StreamReader streamReader = new StreamReader( this.webResponse.GetResponseStream() );
                return streamReader.ReadToEnd().Trim();
            } catch ( Exception ex ) {
                return ( "Error : " + ex.Message );
            }
        }

        public String HttpPost (String uri, String parameters) {
            try {
                this.webRequest = WebRequest.Create( uri );
                this.webRequest.ContentType = "application/x-www-form-urlencoded";
                this.webRequest.Method = "POST";
                byte[] bytes = Encoding.ASCII.GetBytes( parameters );
                this.webRequest.ContentLength = bytes.Length;
                Stream stream = this.webRequest.GetRequestStream();
                stream.Write( bytes, 0, bytes.Length );
                stream.Close();
                this.webResponse = webRequest.GetResponse();
                this.responseStatusCode = ( int ) ( ( HttpWebResponse ) this.webResponse ).StatusCode;
                if ( this.webResponse == null ) {
                    return null;
                }
                StreamReader streamReader = new StreamReader( this.webResponse.GetResponseStream() );
                return streamReader.ReadToEnd().Trim();
            } catch ( Exception ex ) {
                return ( "Error : " + ex.Message );
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace experiment {
    class Mailer {
        private List<String> mailReciepint = new List<String>();
        private MailMessage mail = new MailMessage();
        private String mailSender = null;
        private SmtpClient SmtpServer;

        public Mailer() {
            addRecipint( new String[] {"tariq@bitmascot.com"} );
            addSender( "bitmascot.dev@gmail.com" );
            setSMTPServer( "smtp.gmail.com" , 587);

            String subject = "(Auto Generated Mail) BM_Enrollment Application Fail Notification";
            String body = "This is an auto generated mail, please do not reply.\r\nBM_Enrollment Application is Failed to connect with Device.";
            createMail( subject, body );
        }

        public String sendMail () {
            try {
                setCredential( "bitmascot.dev@gmail.com", "bmakabaka" );
                this.SmtpServer.Send( this.mail );
                return "Mail sent successfully";
            } catch (Exception ex) {
                return "Problem while sending mail : " + ex.Message;
            }
        }

        private void addRecipint (String[] receipint) {
            for(int i=0; i<receipint.Length; i++ ) {
                this.mailReciepint.Add(receipint[i]);
            }
        }

        private void setSMTPServer (String client, int port) {
            this.SmtpServer = new SmtpClient( client );
            this.SmtpServer.Port = port;
            this.SmtpServer.EnableSsl = true;
        }

        private void setCredential (String userName, String password) {
            this.SmtpServer.Credentials = new NetworkCredential( userName, password );
        }

        private void addSender (String sender) {
            this.mailSender = sender;
        }

        private void createMail (String subject, String body) {
            this.mail.From = new MailAddress( this.mailSender );
            foreach(String recipint in this.mailReciepint) {
                this.mail.To.Add( recipint );
            }
            this.mail.Subject = subject;
            this.mail.Body = body;
        }
    }
}

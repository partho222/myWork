﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;

namespace experiment {
    class HttpServer {
        public HttpServer () {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add( "http://*:8080/" );
            try {
                listener.Start();
                Console.WriteLine( "Listening..." );
            }
            catch ( Exception ex ) {
                Console.WriteLine( "HTTP Server Error: " + ex.Message );
                return;
            }

            while ( true ) {
                HttpListenerContext ctx = listener.GetContext();
                new Thread( new Worker( ctx ).ProcessRequest ).Start();
            }
        }

        class Worker {
            private HttpListenerContext context;

            public Worker ( HttpListenerContext context ) {
                this.context = context;
            }

            public void ProcessRequest () {
                String message = null;
                HttpListenerRequest request = context.Request;
                if ( request.HttpMethod.Equals( "POST" ) ) {
                    message = getPostData( request );
                }
                else if ( request.HttpMethod.Equals( "GET" ) ) {
                    getData( request );
                }

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append( "<html><body><h1>" + message + "</h1>" );
                // show here
                stringBuilder.Append( "</body></html>" );

                byte[] b = Encoding.UTF8.GetBytes( stringBuilder.ToString() );
                context.Response.ContentLength64 = b.Length;
                context.Response.OutputStream.Write( b, 0, b.Length );
                context.Response.OutputStream.Close();
            }

            public static String getPostData ( HttpListenerRequest request ) {
                if ( !request.HasEntityBody ) {
                    Console.WriteLine( "No client data was sent with the request." );
                    return null;
                }

                Stream body = request.InputStream;
                Encoding encoding = request.ContentEncoding;
                StreamReader reader = new StreamReader( body, encoding );

                if ( request.ContentType != null ) {
                    Console.WriteLine( "Client data content length {0}", request.ContentLength64 );
                    String data = reader.ReadToEnd();
                    body.Close();
                    reader.Close();
                    return data;
                }
                return null;
            }

            public static void getData ( HttpListenerRequest request ) {
                String[] keys = request.QueryString.AllKeys;
                foreach ( String key in keys ) {
                    Console.WriteLine( key );
                }
                return;
            }
        }
    }
}
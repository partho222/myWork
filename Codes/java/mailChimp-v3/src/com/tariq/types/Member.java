package com.tariq.types;

import java.util.Map;

/**
 * Created by tariq on 06-Mar-17.
 */
public class Member {
    private String id;
    private String email_address;
    private String unique_email_id;
    private Map<String, String> merge_fields;
}

package com.tariq.utils;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by tariq on 06-Mar-17.
 */

public class JsonOperation {
    /**
     * Collect and return object/s of collectionType from JSON
     *
     * @param jsonData - String
     * @param selector - String
     * @param collectionType - Type
     * @return - Collection of object/s
     * @throws Exception
     */
    public static Collection getObjects(String jsonData, String selector, Type collectionType) throws Exception {
        Gson gson = new Gson();
        JSONArray jsonArray = (JSONArray) new JSONObject(jsonData).get(selector);
        return gson.fromJson(jsonArray.toString(), collectionType);
    }
}

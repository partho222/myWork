package com.tariq.utils

public class HttpUtil {

    private static URLConnection getGetConnection(String server, Map requestProperty = [:]) throws IOException {
        URL url = new URL(server);
        URLConnection conn = url.openConnection();
        requestProperty.each {
            conn.setRequestProperty(it.key, it.value);
        }
        conn.setDoInput(true);
        conn.setUseCaches(false);
        return conn;
    }

    private static URLConnection getPostConnection(String server, String data, Map requestProperty = [:]) throws IOException {
        URL url = new URL(server);
        URLConnection conn = url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        requestProperty.each {
            conn.setRequestProperty(it.key, it.value);
        }
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write(data);
        writer.flush();
        writer.close();
        return conn
    }

    private static String getResponseText(URLConnection conn) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = bufferedReader.readLine()) != null) {
            response.append(inputLine);
        }
        bufferedReader.close();
        return response.toString();
    }

    private static serializeMap(Map map) {
        if (map.size() == 0) {
            return 0;
        }
        StringBuilder builder = new StringBuilder()
        map.each {
            builder.append("&" + it.key + "=")
            if (it.value) {
                builder.append(URLEncoder.encode(it.value, "UTF8"))
            }
        }
        return builder.toString().substring(1)
    }

    public static String doGetRequest(String server, Map requestProperty = [:]) throws IOException {
        URLConnection urlConnection = getGetConnection(server, requestProperty);
        return getResponseText(urlConnection);
    }

    public static String doPostRequest(String server, String data, Map requestProperty = [:]) throws IOException {
        URLConnection connection = getPostConnection(server, data, requestProperty)
        return getResponseText(connection);
    }
}
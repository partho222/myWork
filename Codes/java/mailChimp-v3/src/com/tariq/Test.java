package com.tariq;

import com.tariq.config.AppUtil;
import com.tariq.config.ObjectDataSet;
import com.tariq.mailchimp.MailChimpClient;
import com.tariq.types.Member;
import com.tariq.utils.JsonOperation;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Created by tariq on 05-Mar-17.
 */
public class Test {
    public static void main(String[] args) {
        String apiKey = "21ae8644e3887e5bda8719d7419a2acf-us15";
        MailChimpClient mailChimpClient = new MailChimpClient(apiKey);

//        mailChimpClient.getLists();
//        mailChimpClient.getListMembers("fdc82d2fb4");
//        mailChimpClient.getMemberInfo("fdc82d2fb4", "6dced5a7e7d86a60ea138a990459a6cf");
//        AppUtil.bluePrintln(mailChimpClient.getCampaignList());
//        AppUtil.bluePrintln(mailChimpClient.sendCampaignEmail("bbdfacce5e"));

        Collection collection;
        try {
            Type collectionType = new TypeToken<Collection<Member>>() {}.getType();
            collection = JsonOperation.getObjects(mailChimpClient.getListMembers("fdc82d2fb4"), ObjectDataSet.MEMBER, collectionType);
            AppUtil.bluePrintln(collection.toString());
        } catch (Exception ex) {
            AppUtil.redPrintln(ex.getMessage());
        }
    }
}

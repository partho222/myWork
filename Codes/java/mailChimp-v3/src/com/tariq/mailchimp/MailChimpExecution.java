package com.tariq.mailchimp;

import com.tariq.config.AppUtil;
import com.tariq.utils.HttpUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tariq on 06-Mar-17.
 */
public class MailChimpExecution {
    private String apiAuthenticationKey;
    private Map<String, String> requestProperty;

    public MailChimpExecution(String apiAuthenticationKey) {
        this.apiAuthenticationKey = apiAuthenticationKey;
    }

    private void initRequestProperty() {
        this.requestProperty = new HashMap<>();
        this.requestProperty.put("User-Agent", "WebCommander/2.2.0");
        this.requestProperty.put("Authorization", "Basic " + this.apiAuthenticationKey);
    }

    public String execute(String httpRequestUrl, Map requestProperty) {
        initRequestProperty();
        requestProperty.forEach((key, value) ->
                this.requestProperty.put(key.toString(), value.toString())
        );
        try {
            return HttpUtil.doGetRequest(httpRequestUrl, this.requestProperty);
        } catch (Exception ex) {
            AppUtil.redPrintln(ex.getMessage());
        }
        return null;
    }

    public String execute(String httpRequestUrl, Map postData, Map requestProperty) {
        initRequestProperty();
        requestProperty.forEach((key, value) ->
                this.requestProperty.put(key.toString(), value.toString())
        );
        try {
            return HttpUtil.doPostRequest(httpRequestUrl, AppUtil.getJson(postData), this.requestProperty);
        } catch (Exception ex) {
            AppUtil.redPrintln(ex.getMessage());
        }
        return null;
    }
}

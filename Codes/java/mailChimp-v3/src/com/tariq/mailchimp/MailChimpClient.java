package com.tariq.mailchimp;

import com.tariq.config.AppUtil;

import java.util.HashMap;

/**
 * Created by tariq on 02-Mar-17.
 */
public class MailChimpClient {
    private String apiUrl = "https://<dc>.api.mailchimp.com/3.0";
    private String apiKey;
    private MailChimpExecution mailChimpExecution;

    public MailChimpClient(String apiKey) {
        this.apiKey = apiKey;
        mailChimpExecution = new MailChimpExecution(AppUtil.encodeBase64("anystring:" + this.apiKey));
        String dcPrefix = apiKey.substring(apiKey.indexOf("-") + 1);
        this.apiUrl = this.apiUrl.replace("<dc>", dcPrefix);
    }

    public String getLists() {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, "lists");
        return mailChimpExecution.execute(httpRequestUrl, new HashMap());
    }

    public String getListMembers(String listId) {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, AppUtil.concatURL("lists/" + listId, "members"));
        return mailChimpExecution.execute(httpRequestUrl, new HashMap());
    }

    public String getMemberInfo(String listId, String memberId) {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, AppUtil.concatURL("lists/" + listId, "members/" + memberId));
        return mailChimpExecution.execute(httpRequestUrl, new HashMap());
    }

    public String getCampaignList() {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, "campaigns");
        return mailChimpExecution.execute(httpRequestUrl, new HashMap());
    }

    public String getCampaignInfo(String campaignId) {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, "campaigns/" + campaignId);
        return mailChimpExecution.execute(httpRequestUrl, new HashMap());
    }

    public String sendCampaignEmail(String campaignId) {
        String httpRequestUrl = AppUtil.concatURL(this.apiUrl, AppUtil.concatURL("campaigns/" + campaignId, "actions/send"));
        return mailChimpExecution.execute(httpRequestUrl, new HashMap(), new HashMap());
    }
}

package com.tariq.utils;

import java.security.SecureRandom;

/**
 * Created by tariq on 10/3/2016.
 */
public class ByteGenerator {
    private static final String charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*-+";
    private static SecureRandom secureRandom = new SecureRandom();
    private static StringBuilder stringBuilder = null;

    public static String generateByteString ( int size ) {
        stringBuilder = new StringBuilder(size);
        for ( int i=0; i<size; i++) {
            stringBuilder.append( charSet.charAt( secureRandom.nextInt( charSet.length() ) ) );
        }
        return stringBuilder.toString();
    }
}

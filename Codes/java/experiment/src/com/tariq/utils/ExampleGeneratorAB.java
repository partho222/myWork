package com.tariq.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by tariq on 11/9/2016.
 */
public class ExampleGeneratorAB {

    static Integer counter = 168;
    static String[] status = new String[]{"ACTIVE", "INACTIVE"};
    static String[] productGroup = new String[]{"1", "2", "3", "4", "6", "7", "8", "9", "10", "11", "12", "13", "14"};
    static String[] productManager = new String[]{"2", "3", "7", "8"};
    static String[] currency = new String[]{"1", "5"};
    static String[] timePeriod = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
    static String[] itemRate = new String[]{"TRUE", "FALSE"};
    static String[] rateType = new String[]{"FIXED", "RECURRING", "USAGE"};
    static String[] priceGroup = new String[]{"1", "2", "3", "4"};
    static String[] priceType = new String[]{"FIXED", "PER_UNIT", "PER_VOLUME", "PER_TIER", "PER_STEP"};
    static String priceUnit = "1";
    static String[] ac_code = new String[]{"1", "4"};
    static String[] pricePeriod = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
    static String[] aggrQuantity = new String[]{"TRUE", "FALSE"};
    static String[] aggrLevel = new String[]{"ACCOUNT", "RESELLER", "MULTI_HOMED", "MULTI_LEVEL", "ORDER", "ORDER_ITEM"};

    static String exampleFileLoc = "exampleData.txt";
    static StringBuffer stringBuffer = new StringBuffer();
    static JSONObject jsonObject = new JSONObject();
    static String jsonFileLoc = "exampleJSON.json";

    public static void main(String[] args) {
        try {
            readJSON(jsonFileLoc);
        } catch (Exception ex) {
            System.out.println("JSON Read Error : " + ex.getMessage());
        }
        counter = Integer.valueOf(jsonObject.get("CurrentProduct").toString());
        Integer noOfExample = 10;
        String rate = "none";
        String type = "otp";
        List<String> example = generateExample(noOfExample, rate, type);
        try {
            writeExample(example);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static List<String> generateExample(Integer noOfExample, String rate, String type) {
        List<String> example = new ArrayList<>();
        example.add("| code | name | status | productGroup | productManager | productDescription | startDate | endDate | invoiceNote | currency | minQuantity | maxQuantity | timePeriod | itemRate | rateType | priceGroup | priceType | productPrice | productUnit | ac_code | pricePeriod | aggrQuantity | aggrLevel | commQuantity | apiURL | description |\n");
        for (int i = 0; i < noOfExample; i++) {
            String tmpExample = "| PRD-TEST-" + counter.toString();
            tmpExample += " | test" + (counter++).toString();
            tmpExample += " | " + randomString(status);
            tmpExample += " | " + randomString(productGroup);
            tmpExample += " | " + randomString(productManager);
            tmpExample += " | none";
            tmpExample += " | 2016-Oct-31";
            tmpExample += " | 2036-Oct-31";
            tmpExample += " | none";
            tmpExample += " | " + randomString(currency);
            tmpExample += " | 1";
            tmpExample += " | 20";
            tmpExample += " | " + randomString(timePeriod);

            if (rate.equals("sr")) {
                tmpExample += " | " + itemRate[0];
            } else {
                tmpExample += " | " + itemRate[1];
            }

            if (type.equals("otp")) {
                tmpExample += " | " + rateType[0];
            } else if (type.equals("rp")) {
                tmpExample += " | " + rateType[1];
            } else if (type.equals("up")) {
                tmpExample += " | " + rateType[2];
            }

            tmpExample += " | " + randomString(priceGroup);
            tmpExample += " | " + randomString(priceType);
            tmpExample += " | 10 ";
            tmpExample += " | " + priceUnit;
            tmpExample += " | " + randomString(ac_code);
            tmpExample += " | " + randomString(pricePeriod);
            tmpExample += " | " + randomString(aggrQuantity);
            tmpExample += " | " + randomString(aggrLevel);
            tmpExample += " | 3 ";
            tmpExample += " | http://www.autobill.com/ ";
            tmpExample += " | none |\n";
            example.add(tmpExample);
        }
        generateJSON(counter, jsonFileLoc);
        return example;
    }

    public static String randomString(String[] data) {
        int index = new Random().nextInt(data.length);
        return data[index];
    }

    private static void writeExample(List<String> data) throws Exception {
        stringBuffer.delete(0, stringBuffer.length());
        for (int i = 0; i < data.size(); i++) {
            stringBuffer.append(data.get(i));
        }
        writeFile(exampleFileLoc);
    }

    private static void writeFile(String location) throws Exception {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(location)));
        bufferedWriter.write(stringBuffer.toString());
        bufferedWriter.flush();
        bufferedWriter.close();
        System.out.println(" Example File creation Complete ! ");
    }

    public static void generateJSON(Integer value, String fileLoc) {
        JSONObject obj = new JSONObject();
        obj.put("CurrentProduct", Integer.toString(value));
        try {
            FileWriter fileWriter = new FileWriter(fileLoc);
            fileWriter.write(obj.toJSONString());
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println(" Example JSON creation complete ! ");
    }

    public static void readJSON(String fileLoc) throws Exception {
        JSONParser parser = new JSONParser();
        Object object = parser.parse(new FileReader(fileLoc));
        jsonObject = (JSONObject) object;
    }
}

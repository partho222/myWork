package com.tariq.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Created by tariq on 01-Feb-17.
 */
public class FileDownloader {
    private String linkUrl;

    public void setURL(String url) {
        this.linkUrl = url;
    }

    private String getFileName(URL url) {
        String path = url.getPath();
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public void downloadFile() {
        try {
            URL website = new URL(linkUrl);
            ReadableByteChannel readableByteChannel = Channels.newChannel(website.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(getFileName(website));
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            System.out.println("File download success !");
        } catch (Exception ex) {
            System.out.println("File download failed !");
            System.out.println(ex.getMessage());
        }
    }

    public void downloadAsText(HttpServletResponse response) {
        // TODO: implementation needed for generating text
        String test = "Poo poo ooo !!";
        try {
            // TODO: change accordingly
            final byte[] bytes = test.getBytes("UTF-8");

            InputStream inputStream = new ByteArrayInputStream(bytes);
            response.setHeader("Content-Type", "text/plain");
            response.setHeader("Content-Disposition", "attachment; filename=data.txt");
            response.setContentLength(bytes.length);
            OutputStream outputStream = response.getOutputStream();
            byte [] buffer = new byte[4096];
            for (int len = 0; (len = inputStream.read(buffer)) > 0;) {
                outputStream.write(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

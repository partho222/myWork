package com.tariq.utils;

import com.tariq.security.CryptoGraphy;

/**
 * Created by tariq on 10/3/2016.
 */
public class CryptoRunner {
        static String operation;
        static String source;
        static String destination;

    public static void main ( String[] args ) {
        try {
            CryptoGraphy cryptoGraphy = new CryptoGraphy();

            if ( args.length == 3 ) {
                operation = args[0];
                source = args[1];
                destination = args[2];
                if ( operation.equals( "encrypt" ) ) {
                    cryptoGraphy.encrypt(source, destination);
                } else if ( operation.equals( "decrypt" ) ) {
                    cryptoGraphy.decrypt(source, destination);
                }
            } else if ( args.length == 2 ) {
                operation = args[0];
                String data = args[1];
                if ( operation.equals( "encrypt" ) ) {
                    cryptoGraphy.encrypt(data);
                } else if ( operation.equals( "decrypt" ) ) {
                    cryptoGraphy.decrypt(data);
                }
            } else {
                System.out.println( "Use - java -jar crypto.jar [operation_type] [source] [destination]" );
                System.out.println( "Use - java -jar crypto.jar [operation_type] [string]" );
            }

            //System.out.println( ByteGenerator.generateByteString(16) );


        } catch ( Exception ex ) {
            ex.printStackTrace();
        }

    }
}

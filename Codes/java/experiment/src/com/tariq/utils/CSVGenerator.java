package com.tariq.utils;


import java.io.*;
import java.util.Random;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by tariq on 10/9/2016.
 */
public class CSVGenerator {

    static int section1Start = 0;
    static int section2Start = 200;
    static int section3Start = 300;
    static int section1End = 199;
    static int section2End = 299;
    static int section3End = 399;
    static String newStartDay = null;
    static String newEndDay = null;
    static String initialStartDay = "30/10/2016";
    static String initialEndtDay = "31/10/2016";
    static JSONObject jsonObject = new JSONObject();
    static StringBuffer stringBuffer = new StringBuffer();
    static String firstName = "Ftest";
    static String lastName = "Ltest";
    static String email = "__car@test.test";
    static String phone = "123456";
    static String password = "12345678";
    static String time = "12:00";
    static String birthDay = "12";
    static String birthMonth = "12";
    static String birthYear = "1994";
    static String gender = "Male";
    static String city = "DHAKA";
    static String reference = "NONE";
    static String airLine = "10";
    static String flightNo = "555";
    static String age = "23";
    static int noOfAccessories = 0;
    static int totalRentDay = 1;

    static String writeFilelocation = "carhood-user.csv";
    static String jsonFileLocation = "data.json";
    static String serverFileLocation = "/opt/jmeter/apache-jmeter-3.0/bin/";

    public static void main ( String[] args ) {

        File file = new File(jsonFileLocation);
        if ( ! file.isFile()  ) {
            generateJSON ( section1Start, section1End, section2Start, section2End, section3Start, section3End, initialStartDay, initialEndtDay );
        }

        if (args.length == 0){
           System.out.println("Example : java -jar experiment.jar no_of_rows blank/server");
            return;
        }

        int rows = 0;
        if (args.length == 1) {
            rows = Integer.valueOf(args[0]);
        }
        String place = "";
        if (args.length == 2) {
            place = args[1];
        }

        if ( place.equals("server") ) {
            writeFilelocation = serverFileLocation + writeFilelocation;
        }
            try {
                readJSON();
            } catch ( Exception ex ) {
                System.out.println("JSON parsing Error ! " + ex.getMessage());
            }
            try {
                generateCSV(rows);
            } catch ( Exception ex ) {
                System.out.println("CSV generation Error ! " + ex.getMessage());
            }
            try {
                writeJSONFile();
            } catch ( Exception ex ) {
                System.out.println("JSON File writing Error ! " + ex.getMessage());
            }
            try {
                writeCSVFile();
            } catch ( Exception ex ) {
                System.out.println("CSV File writing Error ! " + ex.getMessage());
            }
    }

    public static void generateJSON ( int s1ST, int s1ED, int s2ST, int s2ED, int s3ST, int s3ED, String rentStar, String rentEnd ) {
        JSONObject obj = new JSONObject();
        obj.put( "Section1Last", Integer.toString(s1ST) );
        obj.put( "Section2Last", Integer.toString(s2ST) );
        obj.put( "Section3Last", Integer.toString(s3ST) );

        for ( int i = s1ST ; i <= s3ED ; i++ ) {
            obj.put( Integer.toString(i), rentEnd );
        }

        obj.put("LastEmail", "0");

        try {
            FileWriter fileWriter = new FileWriter(jsonFileLocation);
            fileWriter.write(obj.toJSONString());
            fileWriter.flush();
            fileWriter.close();
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
        System.out.println(" JSON creation complete ! ");
    }


    public static void readJSON () throws Exception {
        JSONParser parser = new JSONParser();
        Object object = parser.parse( new FileReader(jsonFileLocation) );
        jsonObject = (JSONObject) object;
    }

    public static void generateCSV ( int numberOfRow ) throws IOException {
        String lastUsedEmail = (String) jsonObject.get("LastEmail") ;
        Integer startFrom =  Integer.valueOf( lastUsedEmail );
        Integer endTo = startFrom + numberOfRow;

        for ( ; endTo > startFrom ;  startFrom += 3) {
            assignCarForSection1();
            assignCarForSection2();
            assignCarForSection3();
        }
        jsonObject.put("LastEmail", Integer.toString(endTo));
    }

    private static void writeJSONFile () throws Exception {
        FileWriter fileWriter = new FileWriter(jsonFileLocation);
        fileWriter.write(jsonObject.toJSONString());
        fileWriter.flush();
        fileWriter.close();
        System.out.println(" JSON update complete ! ");
    }

    private static void writeCSVFile () throws Exception {
        BufferedWriter bufferedWriter = new BufferedWriter( new FileWriter ( new File( writeFilelocation ) ) );
        bufferedWriter.write( stringBuffer.toString() );
        bufferedWriter.flush();
        bufferedWriter.close();
        System.out.println ( " CSV creation Complete ! " );
    }


    public static void checkDate ( String currentDate ) {
        String[] results = currentDate.split("/");
        int currentDay = Integer.valueOf(results[0]);
        int currentMonth = Integer.valueOf(results[1]);
        int currentYear = Integer.valueOf(results[2]);

        if ( currentDay < 31 ) {
            currentDay += 1;
        } else {
            currentDay = 1;
            if ( currentMonth < 12 ) {
                currentMonth += 1;
            } else {
                currentMonth = 1;
                currentYear += 1;
            }
        }
        newStartDay = currentDay + "/" + currentMonth + "/" + currentYear;
        newEndDay = currentDay+1 + "/" + currentMonth + "/" + currentYear;
    }

    public static void randomAccessoryValue () {
        Random random = new Random();
        noOfAccessories = random.nextInt(5 - 0 + 1) + 0;
    }

    public static void appendToCSV ( String carID, String section ) {
        String emailIdCounter = (String) jsonObject.get("LastEmail");
        randomAccessoryValue();
        stringBuffer.append( firstName + emailIdCounter + ","
                    + lastName + emailIdCounter + ","
                    + "test" + emailIdCounter + email + ","
                    + phone + ","
                    + password + ","
                    + birthDay + "," + birthMonth + "," + birthYear + ","
                    + gender + ","
                    + city + ","
                    + reference + ","
                    + section + ","
                    + newStartDay + ","
                    + time + ","
                    + newEndDay + ","
                    + time + ","
                    + age + ","
                    + carID + ","
                    + totalRentDay + ","
                    + noOfAccessories + ","
                    + airLine + ","
                    + flightNo + "\n"
        );
        Integer newEmailIDCounter = Integer.valueOf(emailIdCounter) + 1;
        jsonObject.put("LastEmail", Integer.toString(newEmailIDCounter) );
    }

    public static void assignCarForSection1 ( ) {

        String lastCar = (String) jsonObject.get("Section1Last");

        int carID = Integer.valueOf( lastCar );
        if (carID <= section1End) {
            String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
            checkDate(currentRentEndDay);
            jsonObject.put( Integer.toString(carID), newEndDay );
            appendToCSV( Integer.toString(carID), "1");
            carID++;
        } else {
                carID = section1Start;
                String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
                checkDate(currentRentEndDay);
                jsonObject.put( Integer.toString(carID), newEndDay );
                appendToCSV( Integer.toString(carID), "1");
                carID++;
        }
        jsonObject.put("Section1Last", Integer.toString(carID) );
    }

    public static void assignCarForSection2 ( ) {

        String lastCar = (String) jsonObject.get("Section2Last");

        int carID = Integer.valueOf( lastCar );
        if (carID <= section2End) {
            String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
            checkDate(currentRentEndDay);
            jsonObject.put( Integer.toString(carID), newEndDay );
            appendToCSV( Integer.toString(carID), "2");
            carID++;
        } else {
                carID = section2Start;
                String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
                checkDate(currentRentEndDay);
                jsonObject.put( Integer.toString(carID), newEndDay );
                appendToCSV( Integer.toString(carID), "2");
                carID++;
        }
        jsonObject.put("Section2Last", Integer.toString(carID) );
    }

    public static void assignCarForSection3 ( ) {

        String lastCar = (String) jsonObject.get("Section3Last");

        int carID = Integer.valueOf( lastCar );
        if (carID <= section3End) {
            String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
            checkDate(currentRentEndDay);
            jsonObject.put( Integer.toString(carID), newEndDay );
            appendToCSV( Integer.toString(carID), "4");     // using 4 instead of 3
            carID++;
        } else {
                carID = section3Start;
                String currentRentEndDay = (String) jsonObject.get( Integer.toString(carID) );
                checkDate(currentRentEndDay);
                jsonObject.put( Integer.toString(carID), newEndDay );
                appendToCSV( Integer.toString(carID), "4");
                carID++;
        }
        jsonObject.put("Section3Last", Integer.toString(carID) );
    }
}
package com.tariq.security;

/**
 * Created by tariq on 10/2/2016.
 */

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.security.spec.KeySpec;


public class AESFileDecryption {
    private static BufferedReader buffreader = new BufferedReader(new InputStreamReader(System.in));
    private static FileInputStream inFile = null;       /* file to be encrypted */
    private static FileOutputStream outFile = null;     /* encrypted file */
    private static byte[] salt = new byte[8];
    private static byte[] iv = new byte[16];
    private static String password = null;

    public static void main(String[] args) throws Exception {
        inFile = new FileInputStream("C:\\Users\\tariq\\Desktop\\EncTest\\encryptedfile.bak");          // input file
        outFile = new FileOutputStream("C:\\Users\\tariq\\Desktop\\EncTest\\file_decrypted.zip");       // output file
        setKeys();

        /*** Password for Decrypting ***/
        System.out.println("Password : ");
        password = buffreader.readLine();


        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        /*** File Decryption ***/
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));

        byte[] in = new byte[64];
        int read;
        while ((read = inFile.read(in)) != -1) {
            byte[] output = cipher.update(in, 0, read);
            if (output != null) {
                writeToFile(outFile, output);
            }
        }

        byte[] output = cipher.doFinal();
        if (output != null) {
            writeToFile(outFile, output);
        }

        closeFiles(inFile, outFile);
        System.out.println("File Decrypted.");
    }

    private static void writeToFile (FileOutputStream file, byte[] data) throws Exception {
        file.write(data);
    }

    private static void closeFiles (FileInputStream inFile, FileOutputStream outFile) throws Exception {
        inFile.close();
        outFile.flush();
        outFile.close();
    }

    private static void setKeys () throws Exception  {
        iv = "gf#s7I%4Cdy$I&ni".getBytes();
        salt = "nAtKU2J^".getBytes();
    }
}
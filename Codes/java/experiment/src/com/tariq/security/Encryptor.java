package com.tariq.security;

/**
 * Created by tariq on 10/3/2016.
 */
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.security.spec.KeySpec;

public class Encryptor {
    private static BufferedReader buffreader = new BufferedReader(new InputStreamReader(System.in));
    private static FileInputStream inFile = null;
    private static FileOutputStream outFile = null;
    private static byte[] salt = new byte[8];
    private static byte[] iv = new byte[16];
    private static String password = null;
    private static String result = null;

    public static void encryptFile ( String source, String destination ) throws Exception {
        inFile = new FileInputStream(source);
        outFile = new FileOutputStream(destination);
        setKeys();

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKey secretKey = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(secretKey.getEncoded(), "AES");

        /*** File Encryption ***/
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));

        byte[] input = new byte[64];
        int bytesRead;

        while ((bytesRead = inFile.read(input)) != -1) {
            byte[] output = cipher.update(input, 0, bytesRead);
            if (output != null) {
                writeToFile(outFile, output);
            }
        }

        byte[] output = cipher.doFinal();
        if (output != null) {
            writeToFile(outFile, output);
        }

        closeFiles(inFile, outFile);
        System.out.println("File Encrypted.");
    }

    public static void decryptFile ( String source, String destination ) throws Exception {
        inFile = new FileInputStream(source);
        outFile = new FileOutputStream(destination);
        setKeys();

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        /*** File Decryption ***/
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));

        byte[] in = new byte[64];
        int read;
        while ((read = inFile.read(in)) != -1) {
            byte[] output = cipher.update(in, 0, read);
            if (output != null) {
                writeToFile(outFile, output);
            }
        }

        byte[] output = cipher.doFinal();
        if (output != null) {
            writeToFile(outFile, output);
        }

        closeFiles(inFile, outFile);
        System.out.println("File Decrypted.");
    }

    public static String encryptString ( String data ) throws Exception {
        System.out.println("Given Plaintext String : " + data);
        setKeys();

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        /*** Data Encryption ***/
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));

        byte[] encryptedTextBytes = cipher.doFinal(String.valueOf(data).getBytes("UTF-8"));

        result = DatatypeConverter.printBase64Binary(encryptedTextBytes);
        return new String("Encrypted String : " + result);
    }

    public static String decryptString ( String data ) throws Exception {
        System.out.println("Given Encrypted String : " + data);

        byte[] encryptedTextBytes = DatatypeConverter.parseBase64Binary(new String(data));
        setKeys();

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKey tmp = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        /*** Data Decryption ***/
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));

        byte[] decryptedTextBytes = null;

        try {
            decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
        }   catch ( IllegalBlockSizeException | BadPaddingException e ) {
            e.printStackTrace();
        }

        result = new String(decryptedTextBytes);
        return new String("Plaintext String : " + result);
    }

    private static void writeToFile (FileOutputStream file, byte[] data) throws Exception {
        file.write(data);
    }

    private static void closeFiles (FileInputStream inFile, FileOutputStream outFile) throws Exception {
        inFile.close();
        outFile.flush();
        outFile.close();
    }

    private static void setKeys () throws Exception  {
        iv = "gf#s7I%4Cdy$I&ni".getBytes();
        salt = "nAtKU2J^".getBytes();
        password = "tariq";

        /*** password to encrypt/decrypt the File/Data ***/
//        System.out.print("Password : ");
//        password = buffreader.readLine();
    }
}

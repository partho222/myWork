package com.tariq.security;

/**
 * Created by tariq on 10/3/2016.
 */
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;


public class CryptoGraphy {

    public String ENCRYPTION_KEY = "FiH17RS^m8EM%s^E";
    public String INIT_VECTOR = "TVqCQ#rW9P407#eU";
    private final String AES = "AES";
    private final String ENCODING = "UTF-8";
    private final String CIPHER_INSTANCE_NAME = "AES/CBC/PKCS5PADDING";


    private byte[] encryptDecrypt(byte[] content, int mode){
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes(ENCODING));
            SecretKeySpec skeySpec = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING), AES);
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE_NAME);
            cipher.init(mode, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(content);

            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return  null;
    }


    private boolean encryptDecrypt(int mode, String source, String destination) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes(ENCODING));
            SecretKeySpec skeySpec = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING), AES);
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE_NAME);
            cipher.init(mode, skeySpec, iv);

            FileInputStream inFile = new FileInputStream(source);
            FileOutputStream outFile = new FileOutputStream(destination);

            byte[] input = new byte[64];
            int bytesRead;

            while ((bytesRead = inFile.read(input)) != -1) {
                byte[] output = cipher.update(input, 0, bytesRead);
                if (output != null) {
                    outFile.write(output);
                }
            }

            byte[] output = cipher.doFinal();
            if (output != null) {
                outFile.write(output);
            }

            inFile.close();
            outFile.flush();
            outFile.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return  true;
    }


    public void encrypt ( String content ) {
        byte[] data = content.getBytes();
        byte[] result = encryptDecrypt(data, Cipher.ENCRYPT_MODE);
        System.out.println( "Encrypted String : " + Base64.getEncoder().encodeToString(result) );
    }


    public void decrypt ( String content ) {
        byte[] data = Base64.getDecoder().decode( content );
        byte[] result = encryptDecrypt(data, Cipher.DECRYPT_MODE);
        System.out.println( "Plain Text String : " + new String(result) );
    }

    public void encrypt ( String source, String destination ) {
        if ( encryptDecrypt( Cipher.ENCRYPT_MODE, source, destination ) ) {
            System.out.println( "File Encrypted !!" );
        } else {
            System.out.println( "File Encryption Error !!" );
        }
    }


    public void decrypt ( String source, String destination ) {
        if ( encryptDecrypt( Cipher.DECRYPT_MODE, source, destination ) ) {
            System.out.println( "File Decrypted !!" );
        } else {
            System.out.println( "File Decryption Error !!" );
        }
    }

}

package com.tariq;

import com.tariq.utils.FileDownloader;

/**
 * Created by tariq on 01-Feb-17.
 */
public class Main {
    public static void main(String[] args) {
        FileDownloader fileDownloader = new FileDownloader();
        fileDownloader.setURL("http://samplecsvs.s3.amazonaws.com/Sacramentorealestatetransactions.csv");
        fileDownloader.downloadFile();
    }
}

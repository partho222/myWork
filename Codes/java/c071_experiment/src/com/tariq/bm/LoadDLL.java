package com.tariq.bm;

/**
 * Created by tariq on 09-Jan-17.
 */
public class LoadDLL {

    /** using JNI **/
    static {
        System.loadLibrary("Riss.Devices");
    }

    public native void loadDLL();

    public static void main (String[] args) {
        System.setProperty("java.library.path", "C:\\Users\\tariq\\Desktop\\myGit\\Codes\\java\\c071_experiment\\lib\\");
        new LoadDLL().loadDLL();
    }
}

package grailskb

import com.sr.ws.ChatroomEndpoint

import javax.websocket.server.ServerContainer

class BootStrap {

    def init = { servletContext ->
        ServerContainer serverContainer = servletContext.getAttribute("javax.websocket.server.ServerContainer")
        serverContainer.addEndpoint(ChatroomEndpoint)
    }
    def destroy = {
    }
}

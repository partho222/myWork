package com.bitmascot.tariq.mailchimp.v3_0.lists

import com.bitmascot.tariq.mailchimp.MailChimpAPIVersion
import com.bitmascot.tariq.mailchimp.MailChimpMethod
import com.bitmascot.tariq.mailchimp.anotation.Field
import com.bitmascot.tariq.mailchimp.anotation.Method

@Method(name = "lists/merge-vars", version = MailChimpAPIVersion.v3_0)
class MergeVarListMethod extends MailChimpMethod {

    @Field
    public String[] id;

}

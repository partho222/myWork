package com.bitmascot.common;


import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by touhid on 3/01/2016.
 */
public class AppUtil {

    public static String concatLocation(String first, String second){
        if (first.endsWith("/") || first.endsWith("\\")){
            return first + second;
        }else {
           return first + File.separator + second;
        }
    }


    public static void bluePrintln(String message){
        System.out.println("\033[34m" + message);
    }

    public static void redPrintln(String message){
        System.out.println("\033[31m" + message);
    }

    public static void yellowPrintln(String message){
        System.out.println("\033[33m" + message);
    }

    public static void whitePrintln(String message){
        System.out.println("\033[37m" + message);
    }



    public static void errorLog(String message){
        AppUtil.redPrintln("=====================================================================");
        AppUtil.whitePrintln(message);
        AppUtil.redPrintln("=====================================================================");
        AppUtil.whitePrintln("");
    }


    public static void yellowSeparator(){
        AppUtil.yellowPrintln("=====================================================================");
        AppUtil.whitePrintln("");
    }

    public static void newLine(){
        System.out.println("\n");
    }

    public static void heading(String message){
        AppUtil.yellowPrintln(AppConstant.ANSI_GREEN + "###### " + message + " " +  AppConstant.ANSI_WHITE);
    }

    public static void infoLog(String message){
        AppUtil.yellowPrintln("=====================================================================");
        AppUtil.whitePrintln(message);
        AppUtil.yellowPrintln("=====================================================================");
        AppUtil.whitePrintln("");
    }

    public static Long timeDeviation(Long start, Long end){
        return Math.max((end - start),0);
    }

    public static String getTmpLocation(){
        return AppUtil.concatLocation(ConfigLoader.getUpdateTmp(), System.currentTimeMillis() + "");
    }

    public static String getDatabasePassHash(){
        String timeStamp = System.currentTimeMillis() + "";
        String hash = "";
        try {
            hash = DatatypeConverter.printBase64Binary(timeStamp.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hash.replaceAll("=|/+|//","");
    }

    public static String formattedDateTime(){
        Date now = new Date( );
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("dd MMMM yyyy hh:mm:ss a zzz");
        return simpleDateFormat.format(now);
    }

    public static String underScoreFormattedDateTime(){
        Date now = new Date( );
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("dd_MMMM_yyyy_hh_mm_ss_a_zzz");
        return simpleDateFormat.format(now);
    }

    public static void accessToNginx(){
        String serverAppOperationalLocation = ConfigLoader.getDownloadableResourceHolder();
//        ShellFile.chconR(serverAppOperationalLocation);
    }

    public static String getUUID(){
        return UUID.randomUUID().toString().toUpperCase();
    }

    public static String concatURL(String first, String second){
        if (first.endsWith("/")){
            return first + second;
        }else {
            return first + "/" + second;
        }
    }

    public static String nginxConfigFileName(String name){
        return name + AppConstant.CONFIG_EXTENSION;
    }

    public static String httpdConfFileName(String name){
        return nginxConfigFileName(name);
    }

    public static String instanceWarLogFile(String instanceROOT){
        return AppUtil.concatLocation(instanceROOT,"WEB-INF/" + AppConstant.NAM_LOG_DATA);
    }

    public static String instanceUpdateLogFile(String instanceROOT){
        return AppUtil.concatLocation(instanceROOT,"WEB-INF");
    }

}

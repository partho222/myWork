package com.bitmascot.common;

import java.io.*;
import java.util.Properties;

/**
 * Created by touhid on 27/12/2015.
 */
public class ConfigLoader {

    public static String configProperties = "config.properties";

    public static String getProperties(){
        return null;
    }



    public static String getPropertiesById(String name){
        Properties properties = new Properties();

        File file = new File(configProperties);
        System.out.println(" == " + file.getAbsolutePath());
        if (!file.exists()){
            return null;
        }else{
            try {
                InputStream inputStream = new FileInputStream(file);
                properties.load(inputStream);
                inputStream.close();
                return properties.getProperty(name,null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getROOT(String domainName){
        String name =  getPropertiesById("instance.root");
        if (name != null){
            name = name.replace("instance_domain",domainName);
        }else{
            return null;
        }
      return name;
    }

    public static String getInstanceTomcatConfig(String domainName){
        String name =  getPropertiesById("instance.tomcat.config");
        if (name != null){
            name = name.replace("instance_domain",domainName);
        }else{
            return null;
        }
        return name;
    }

    public static String getInstanceTomcatContextXML(String domainName){
        String name =  getPropertiesById("instance.tomcat.context");
        if (name != null){
            name = name.replace("instance_domain",domainName);
        }else{
            return null;
        }
        return name;
    }

    public static String getInstanceLoc(String domainName){
        String name =  getPropertiesById("instances");
        if (name != null){
            name = AppUtil.concatLocation(name,domainName);
        }else{
            return null;
        }
        return name;
    }

    public static String getNginxHome(String instanceName){
        String name =  getPropertiesById("instances");
        if (name != null){
            name = AppUtil.concatLocation(name,instanceName);
            name = AppUtil.concatLocation(name,AppConstant.NGINX);
        }else{
            return null;
        }
        return name;
    }


    public static String getInstanceWCPluginLoc(String domainName){
        String root = ConfigLoader.getROOT(domainName);
        if (root == null){
            return null;
        }else{
            String name =  getPropertiesById("instance.wc.plugin");
            if (name != null){
                return AppUtil.concatLocation(root,name);
            }else{
                return null;
            }
        }
    }


    public static String getWCPluginLoc(){
            String name = getPropertiesById("instance.wc.plugin");
            if (name != null){
               return name;
            }else{
                return null;
            }
    }

    public static String provisioningServerUrl(){
        String name = getPropertiesById("provisioning.server");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String appName(){
        String name = getPropertiesById("app.name");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String iManagerEmergencyRestart(){
        String name = getPropertiesById("imanager.restart.command");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String ssoServerUrl(){
        String name = getPropertiesById("sso.server");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPluginsSysRes(){
            String name =  getPropertiesById("instance.plugins.sys.res");
            if (name != null){
                return name;
            }else{
                return null;
            }
    }

    public static String getRootPluginsLoc(){
        String name =  getPropertiesById("instance.root.plugins");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }


    public static String provisioningFeedbackURL(){
        String name =  getPropertiesById("provisioning.feedback.url");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getProviBinaryMakerInstance(){
        String name =  getPropertiesById("build.provi.war.instance");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getLog(String domainName){
        String name =  getPropertiesById("instance.catalina");
        if (name != null){
            name = name.replace("instance_domain",domainName);
        }else{
            return null;
        }
        return name;
    }

    public static String getInstanceTomcat(String domainName){
        String name =  getPropertiesById("instance.tomcat");
        if (name != null){
            name = name.replace("instance_domain",domainName);
        }else{
            return null;
        }
        return name;
    }

    public static String getCodeSource(){
        String name =  getPropertiesById("source.code");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getDNSURL(){
        String name =  getPropertiesById("dns.server");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPortalServer(){
        String name =  getPropertiesById("portal.server");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getScssLocation(){
        String name =  getPropertiesById("scss.location");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }




    public static String getUpdateSQLLoc(){
        String source =  getPropertiesById("source.code");
        if (source != null){
            String sqlLoc = getPropertiesById("update.sql.loc");
            if (sqlLoc != null){
                return AppUtil.concatLocation(source,sqlLoc);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getInitSQLLoc(){
        String source =  getPropertiesById("source.code");
        if (source != null){
            String sqlLoc = getPropertiesById("init.sql.loc");
            if (sqlLoc != null){
                return AppUtil.concatLocation(source,sqlLoc);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getInstanceInitSQLLoc(String instanceName){
        String root =  getROOT(instanceName);
        return getInstanceInitSQLLoc(root,AppConstant.TOMCAT);
    }

    public static String getInstanceInitSQLLoc(String location, String type){
        if (location != null){
            String sqlLoc = getPropertiesById("app.init.sql.loc");
            if (type.equals(AppConstant.TOMCAT)){
                sqlLoc = getPropertiesById("app.init.sql.loc");
            }else if (type.equals(AppConstant.HTML)){
                sqlLoc = getPropertiesById("portal.init.sql.loc");
            }
            if (sqlLoc != null){
                return AppUtil.concatLocation(location,sqlLoc);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }



    public static String getAliveURL(){
        String http =  getPropertiesById("build.provi.war.instance.http");
        String url =  getPropertiesById("build.provi.war.instance.url");
        if (http != null && url != null){
            String instance = getPropertiesById("build.provi.war.instance");
            if (instance != null){
                return http + "://"  + instance + url;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getPluginSource(){
        String source =  getPropertiesById("source.code");
        if (source != null){
            String plugin = getPropertiesById("plugin.location");
            if (plugin != null){
               return AppUtil.concatLocation(source,plugin);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getAppUpdateSQLLoc(){
        String name =  getPropertiesById("app.update.sql.loc");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPluginUpdateSQLLoc(){
        String name =  getPropertiesById("plugin.update.sql.loc");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getBinaryDownloadLink(){
        String name =  getPropertiesById("download.binary.zip.link");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getBinaryDownloadLink(String postfix){
        String name =  getPropertiesById("download.binary.zip.link");
        if (name != null){
            return AppUtil.concatLocation(name,postfix);
        }else{
            return null;
        }
    }

    public static String getDownloadableResourceHolder(){
        String resource =  getOperationalResourceLocation();
        if (resource != null){
            String name =  getPropertiesById("downloadable.resource.location");
            if (name != null){
                return AppUtil.concatLocation(resource,name);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getBinaryRepo(){
        String resource =  getOperationalResourceLocation();
        if (resource != null){
            String name =  getPropertiesById("wc.binary.repo");
            if (name != null){
                return AppUtil.concatLocation(resource,name);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getUpdateTmp(){
        String name =  getPropertiesById("update.tmp");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String serverAppUser(){
        String name =  getPropertiesById("server.app.user");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getJavaHome(){
        String name =  getPropertiesById("java.home");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getGrailsXmX(){
        String name =  getPropertiesById("grails.xmx");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getSkippInstances(){
        String name =  getPropertiesById("skipp.instances");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getBashHome(){
        String name =  getPropertiesById("bash.home");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getInstanceIndexName(){
        String name =  getPropertiesById("instance.index.name");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getSAType(){
        String name =  getPropertiesById("sa.type");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static Integer getInstanceHttpPort(){
        String name =  getPropertiesById("instance.http.port");
        if (name != null){
            return Integer.parseInt(name);
        }else{
            return null;
        }
    }

    public static Integer getInstanceHttpsPort(){
        String name =  getPropertiesById("instance.https.port");
        if (name != null){
            return Integer.parseInt(name);
        }else{
            return null;
        }
    }

    public static Integer getInstanceShutdownPort(){
        String name =  getPropertiesById("instance.shutdown.port");
        if (name != null){
            return Integer.parseInt(name);
        }else{
            return null;
        }
    }


    public static String getSoftLinkLocation(){
        String name =  getPropertiesById("soft.link.location");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String concatSoftLinkFileAfterRoot(String location){
            String name =  getPropertiesById("instance.soft.link.file");
            if (name != null){
                return AppUtil.concatLocation(location,name);
            }else{
                return null;
            }
    }

    public static String concatPortalSoftLinkFile(String location){
        String name =  getPropertiesById("portal.soft.link.file");
        if (name != null){
            return AppUtil.concatLocation(location,name);
        }else{
            return null;
        }
    }

    public static String concatPortalPluginLocation(String location, String structure){
        String path =  getPropertiesById("portal.plugin.location");
        if (path != null){
            path = AppUtil.concatLocation(location,path);
            return AppUtil.concatLocation(path,structure);
        }else{
            return null;
        }
    }


    public static String getInstanceSoftLinkFile(String instanceName){
        String root = ConfigLoader.getROOT(instanceName);
        if (root != null){
            String name =  getPropertiesById("instance.soft.link.file");
            if (name != null){
                return AppUtil.concatLocation(root,name);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static boolean isProductionMode(){
        String name =  getPropertiesById("is.production.mode");
        if (name != null){
            return isTrueConfig(name);
        }else{
            return false;
        }
    }

    public static boolean isTrueConfig(String value){
        if (value.equals("true")){
            return true;
        }else{
            return false;
        }
    }


    public static String downloadAllBinaryLink(){
        String value =  getPropertiesById("download.all.binary.by.version");
        if (value != null){
            return value;
        }else{
            return null;
        }
    }

    public static String portalApiUrl(){
        String value =  getPropertiesById("portal.api.server");
        if (value != null){
            return value;
        }else{
            return null;
        }
    }

    public static String downloadWarLink(){
        String value =  getPropertiesById("download.war.by.version");
        if (value != null){
            return value;
        }else{
            return null;
        }
    }


    public static String downloadSchemaLink(){
        String value =  getPropertiesById("download.schema.by.version");
        if (value != null){
            return value;
        }else{
            return null;
        }
    }

    public static String downloadPluginLink(){
        String value =  getPropertiesById("download.plugin.by.version.and.name");
        if (value != null){
            return value;
        }else{
            return null;
        }
    }

    public static boolean isRemoteBinary(){
        String isRemoteBinary =  getPropertiesById("is.remote.binary");
        if (isRemoteBinary != null){
            return isTrueConfig(isRemoteBinary);
        }else{
            return false;
        }
    }

    public static String backupLocation(String backupName){
        String name =  getOperationalResourceLocation();
        if (name != null){
            String location = getPropertiesById("before.backup.location");
            name = AppUtil.concatLocation(name,location);
            return AppUtil.concatLocation(name,backupName);
        }else{
            return null;
        }
    }

    public static String removeBackupLocation(String backupName){
        String name =  getOperationalResourceLocation();
        if (name != null){
            String location = getPropertiesById("remove.backup.location");
            name = AppUtil.concatLocation(name,location);
            return AppUtil.concatLocation(name,backupName);
        }else{
            return null;
        }
    }


    public static String backupTransferLocation(String backupName){
        String name =  getOperationalResourceLocation();
        if (name != null){
            String location = getPropertiesById("backup.transfer.location");
            name = AppUtil.concatLocation(name,location);
            return AppUtil.concatLocation(name,backupName);
        }else{
            return null;
        }
    }

    public static String populateBackupLocation(String backupName){
        String name =  getOperationalResourceLocation();
        if (name != null){
            String location = getPropertiesById("before.populate.backup.location");
            name = AppUtil.concatLocation(name,location);
            return AppUtil.concatLocation(name,backupName);
        }else{
            return null;
        }
    }


    public static String getDatabasePrefix(){
        String name =  getPropertiesById("database.prefix");
        if (name != null){
            return name;
        }else{
            return "";
        }
    }

    public static String getDatabaseUsername(){
        String name =  getPropertiesById("database.user");
        if (name != null){
            return name;
        }else{
            return "";
        }
    }

    public static String getProvisionDomainPostfix(){
        String name =  getPropertiesById("provision.domain.postfix");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getOperationalResourceLocation(){
        String name =  getPropertiesById("server.app.operational.resource");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getSchemaOperationLocation(){
        String temp =  getOperationalResourceLocation();
        if (temp != null){
            String schema = getPropertiesById("sa-schema");
            if (schema != null){
                return AppUtil.concatLocation(temp,schema);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public static String getDatabasePassword(){
        String name =  getPropertiesById("database.password");
        if (name != null){
            return name;
        }else{
            return "";
        }
    }

    public static String getInstancesLoc(){
        String name =  getPropertiesById("instances");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getCatalinaOutLoc(String instanceName){
        String instances =  getInstancesLoc();
        if (instances != null){
            String instance = AppUtil.concatLocation(instances,instanceName);
            String log = getPropertiesById("catalina.out");
            if (log != null){
                return AppUtil.concatLocation(instance,log);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }


    public static String getVersionLogFile(String version){
        String repo =  getBinaryRepo();
        if (repo != null){
            String versionInside = AppUtil.concatLocation(repo,version);
            return AppUtil.concatLocation(versionInside,AppConstant.NAM_LOG_DATA);
        }else{
            return null;
        }
    }


    public static String getInstanceLogFile(String domainName){
        String repo =  AppUtil.instanceUpdateLogFile(getROOT(domainName));
        if (repo != null){
            return AppUtil.concatLocation(repo,AppConstant.NAM_INSTANCE_UPDATE_LOG_DATA);
        }else{
            return null;
        }
    }

    public static String getInstanceServerConfigLoc(String instanceName){
        String config =  getPropertiesById("instance.server.config");
        if (config != null){
            return AppUtil.concatLocation(config,instanceName);
        }else{
            return null;
        }
    }

    public static String isSudo(){
        String config =  getPropertiesById("is.sudo.command");
        if (config != null){
            return config + " ";
        }else{
            return null;
        }
    }


    public static String getTomcatServerXML() {
        String resource = getSAStaticResourceLocation();
        if (resource != null) {
            String name = getPropertiesById("tomcat.server.xml");
            if (name == null) {
                return null;
            }
            return AppUtil.concatLocation(resource, name);
        } else {
            return null;
        }
    }

    public static String getNginxConfig(String appName){
        String resource = getSAStaticResourceLocation();
        if (resource != null) {
            return AppUtil.concatLocation(resource, appName + "-" + AppConstant.NGINX + AppConstant.CONFIG_EXTENSION);
        } else {
            return null;
        }
    }

    public static String getSANginxConfig(){
        String name =  getPropertiesById("nginx.sa.config");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getWCNginxConfig(){
        String name =  getPropertiesById("nginx.wc.config");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getHttpsNginxConfig(){
        String name =  getPropertiesById("nginx.https.config");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getNginxConfigD(){
        String name =  getPropertiesById("nginx.conf.d");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getSAStaticResourceLocation(){
        String name =  getPropertiesById("sa.static.resource");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getMyIdentity(){
        String name =  getPropertiesById("my.identity");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getDNSEntryType(){
        String name =  getPropertiesById("dns.entry.type");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPublicAccessibleDomainPostfix(){
        String name =  getPropertiesById("public.accessible.domain.postfix");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getHttpdConfDLoc(){
        String name =  getPropertiesById("httpd.conf.d");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPortalDomainPostfix(){
        String name =  getPropertiesById("portal.domain.postfix");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPortalConfLoc(){
        String name =  getPropertiesById("portal.conf.loc");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPortalTmpLoc(){
        String name =  getPropertiesById("portal.tmp.loc");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getPortalAppDownloadURL(){
        String name =  getPropertiesById("portal.app.download.url");
        if (name != null){
            return name;
        }else{
            return null;
        }
    }

    public static String getSADBConfig(String instanceName){
        String root =  getROOT(instanceName);
        if (root != null){
            return AppUtil.concatLocation(root,"WEB-INF/classes/" + AppConstant.SA_DB_CONFIG);
        }else{
            return null;
        }
    }

    public static String getWCDBConfig(String instanceName){
        String root =  getROOT(instanceName);
        if (root != null){
            return AppUtil.concatLocation(root,"WEB-INF/" + AppConstant.WC_DB_CONFIG);
        }else{
            return null;
        }
    }

    public static String getABDBConfig(String instanceName){
        String root =  getROOT(instanceName);
        if (root != null){
            return AppUtil.concatLocation(root,"WEB-INF/" + AppConstant.AB_DB_CONFIG);
        }else{
            return null;
        }
    }

    public static String getAB2DBConfig(String instanceName){
        String root =  getROOT(instanceName);
        if (root != null){
            return AppUtil.concatLocation(root,"WEB-INF/" + AppConstant.AB2_DB_CONFIG);
        }else{
            return null;
        }
    }


}

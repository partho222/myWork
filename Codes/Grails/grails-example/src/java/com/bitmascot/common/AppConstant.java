package com.bitmascot.common;

/**
 * Created by touhid on 27/12/2015.
 */
public class AppConstant {

    public static final String NAM_LOG_NAME = "buildLog.dat";
    public static final String NAM_LOG_DATA = "versionLog.dat";
    public static final String NAM_INSTANCE_UPDATE_LOG_DATA = "instanceUpdateLog.dat";
    public static final String NAM_CORE_CONFIG_FILE = "appConfig.properties";
    public static final String NAM_APPLICATION_PROPERTIES = "application.properties";
    public static final String NAM_TARGET = "target";
    public static final String NAM_JSON_ARRAY = "NAM_JSON_ARRAY";
    public static final String NAM_JSON_OBJECT = "NAM_JSON_OBJECT";
    public static final String NAM_ROOT = "ROOT";
    public static final String NAM_APP_WAR = "app.war";
    public static final String NAM_DOWNLOAD_BINARY = "download.zip";
    public static final String NAM_DOWNLOAD_TAR = "download.tar";
    public static final String NAM_ROOT_PLUGINS = "plugins";
    public static final String NAM_DATABASE_LAST_BACKUP = "database_backup.sql";
    public static final String NAM_IS_DIRECTORY = "NAM_IS_DIRECTORY";
    public static final String NAM_IS_FILE = "NAM_IS_FILE";
    public static final String WEB_APP_AND_INF = "web-app/WEB-INF";

    public static final String SYS_SEPERATOR = ":::";
    public static final String DATABASE_PREFIX = "wc_";
    public static final String WEB_INF = "WEB-INF";
    public static final String START = "START";
    public static final String STOP = "STOP";
    public static final String RESTART = "RESTART";
    public static final String ENABLE = "ENABLE";
    public static final String DISABLE = "DISABLE";
    public static final String RELOAD = "RELOAD";

    public static final String TOMCAT_SERVICE_NAME = "-tomcat.service";
    public static final String WC_APP = "WebCommander";
    public static final String AB_APP = "AutoBill";
    public static final String SA_APP = "ServerApp";
    public static final String OA_APP = "OtherApp";
    public static final String PA_APP = "PortalApp";

    public static final String APP_VERSION = "APP_VERSION";
    public static final String PLUGIN_NAME = "PLUGIN_NAME";
    public static final String PLUGIN_VERSION = "PLUGIN_VERSION";
    public static final String PLUGIN_META_LIST = "PLUGIN_META_LIST";
    public static final String APP_META_FILE = "APP_META_FILE.DAT";
    public static final String PROVY_BACKUP_FILE = "PROVY_BACKUP_FILE.DAT";



    public static Boolean IS_GLOBAL_LOG = true;
    public static Boolean IS_CLEAN_ALL = true;

    public static Integer NUMBER_OF_THREAD = 8;
    public static Integer NUMBER_OF_BUILD_LOOP = 3;
    public static Integer THREAD_RUNNING = 0;
    public static Integer THREAD_SLEEP_THREE_SECOND = 3000;
    public static Integer CHECK_STATUS_LOOP = 100;

    public static Integer THREAD_SECOND_IN_MILI = 3000;
    public static Integer FIVE_MIN_IN_MILI = 300000;



//    COLOR
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";


    public static final String PLUGIN_EXTENSION = "zip";
    public static final String WAR_EXTENSION = "war";
    public static final String SQL_FILE_EXTENSION = ".sql";
    public static final String JSON_EXTENSION = ".json";
    public static final String HTML_EXTENSION = ".html";
    public static final String ZIP_EXTENSION = ".zip";
    public static final String CONFIG_EXTENSION = ".conf";
    public static final String CSS_EXTENSION = ".css";
    public static final String SCSS_EXTENSION = ".scss";
    public static final String SERVICE_EXTENSION = ".service";
    public static final String TAR_EXTENTION = ".tar.gz";

    public static final String PLUGIN_INSTALLATION_STATUS_EXT = "EXT_OF_PLUGIN";
    public static final String NGINX = "nginx";
    public static final String HTML = "html";
    public static final String HTML_BACKUP = "html_backup";
    public static final String TOMCAT = "tomcat";
    public static final String APP_ZIP = "app.zip";


    public static final String CACHE_MODELS = "cache/models";
    public static final String CACHE_PERSISTENT = "cache/persistent";
    public static final String PORTAL_TMP = "logs";




    public static final String BACKUP_INSTANCE_PROVY_SQL = "SELECT config_key, type, value FROM %s.site_config WHERE type = 'license'";
    public static final String BACKUP_INSTANCE_AB_PROVY_SQL = "SELECT config_key, type, value FROM %s.site_config WHERE type = 'autobill_provision';";

    public static final String PROVY_CONDITION_SQL = "SELECT config_key, type, value FROM site_config WHERE type = '%s' AND config_key = '%s';";
    public static final String PROVY_IF_TRUE_SQL = "UPDATE site_config SET value = '%s' WHERE type = '%s' AND config_key = '%s';";
    public static final String PROVY_IF_FALSE_SQL = "INSERT INTO site_config (config_key, type, value) VALUES ('%s','%s','%s');";

    public static final String OPERATOR_CONDITION_SQL = "SELECT * FROM offline_query_queue WHERE arg0 = '%s' AND method_name = 'createUser';";
    public static final String OPERATOR_IF_TRUE_SQL = "";
    public static final String OPERATOR_IF_FALSE_SQL = "INSERT INTO `offline_query_queue` (`arg0`, `arg1`, `arg2`, `arg3`, `method_name`) VALUES ('%s', '%s', '%s', '%s', 'createUser');";
    public static final String OPERATOR_SELECT = "SELECT email, full_name, 'changeMe' AS password, uuid FROM operator;";


    public static final String CMD_BUILD_AND_PACK = "buildPack";
    public static final String CMD_UPDATE = "update";
    public static final String CMD_BACKUP = "backup";
    public static final String CMD_DEPLOY = "deploy";
    public static final String CMD_SA = "sa";
    public static final String CMD_POPULATE = "populate";
    public static final String CMD_GENERATE_SCHEMA = "sgenerate";
    public static final String CMD_COMPARE_SCHEMA = "scompare";
    public static final String CMD_VERSION_WAR = "vwar";
    public static final String CMD_PORTAL = "pd";

    public static final String DOWNLOAD_SCHEMA = "download-schema";
    public static final String DOWNLOAD_HABIJABI = "download-habijabi";
    public static final String DOWNLOAD_PROVISIONING_BINARY = "download-binary";

    public static final String ADD = "ADD";
    public static final String REMOVE = "REMOVE";
    public static final String UPDATE = "UPDATE";
    public static final String EDIT = "EDIT";
    public static final String DELETE = "DELETE";

    public static final String SA_STATIC_USER = "tomcat";
    public static final String APACHE_USER = "apache";
    public static final String ROOT_USER = "root";




    public static final String APP_NAME_CONFIG = "APP_NAME";
    public static final String APP_VERSION_CONFIG = "APP_VERSION";
    public static final String SITE_INDEX_CONFIG = "SITE_INDEX";
    public static final String HTTP_HOST_CONFIG = "HTTP_HOST";
    public static final String SHUTDOWN_PORT_CONFIG = "SHUTDOWN_PORT";
    public static final String HTTP_PORT_CONFIG = "HTTP_PORT";
    public static final String HTTPS_PORT_CONFIG = "HTTPS_PORT";
    public static final String SITE_USER_CONFIG = "SITE_USER";
    public static final String DB_HOST_CONFIG = "DB_HOST";
    public static final String DB_NAME_CONFIG = "DB_NAME";
    public static final String DB_USER_CONFIG = "DB_USER";
    public static final String DB_PASS_CONFIG = "DB_PASS";
    public static final String INSTANCE_SERVER_CONFIG_SEPARATOR = ":::";
    public static final String NEW_LINE = System.getProperty("line.separator");


    // Schema Generator
    public static final String SCHEMA = "schema";
    public static final String PLUGIN_SCHEMA = SCHEMA + "_plugin";
    public static final String SITE_CONFIG = "site_config";
    public static final String PLUGIN_SITE_CONFIG = SITE_CONFIG + "_plugin";
    public static final String MAIN_SCHEMA_NAME = "mainSchema";
    public static final String MAIN_SITE_CONFIG_NAME = "site_config_init";
    public static final String EXPORT_SCHEMA_DIR = "export";
    public static final String REPORT_DIR = "report";
    public static final String COMPARE_SCHEMA_HOLDER = "compare-schema";


    public static final String LB_ERROR_MESSAGE = " .Error Message: ";

    public static final String SCHEMA_GENERATION_LOG_ID_EX = "_SCHEMA_GENERATION_LOG_ID_EX";

//    public static final String SERVER_APP_SERVER = "SERVER_APP_SERVER";
//    public static final String WEB_COMMANDER_SERVER = "WEB_COMMANDER_SERVER";
//    public static final String AUTO_BILL_SERVER = "AUTO_BILL_SERVER";
//    public static final String I_MANAGER_SERVER = "I_MANAGER_SERVER";
//    public static final String SSO_SERVER = "SSO_SERVER";

    public static final String SA_TOMCAT = "sa_tomcat";
    public static final String AB_TOMCAT = "ab_tomcat";
    public static final String WC_TOMCAT = "wc_tomcat";
    public static final String OA_TOMCAT = "oa_tomcat";
    public static final String BASIC_PORTAL = "basic";
    public static final String AUTOBILL_PORTAL = "autobill";
    public static final String PORTAL_PLUGIN_LOC = "app/Plugin";

    public static final String SA_DB_CONFIG = "config.properties";
    public static final String AB_DB_CONFIG = "db_config.properties";
    public static final String AB2_DB_CONFIG = "config.properties";
    public static final String WC_DB_CONFIG = "appConfig.properties";
    public static final String PORTAL_DB_CONFIG = "database.php";
    public static final String PORTAL_API_CONFIG = "apiConfig.xml";
    public static final String PORTAL_HTTPD_CONFIG = "portal-httpd.conf";
    public static final String TOMCAT_CONTEXT = "context.xml";

    public static final String DEPLOY_ACTION = "deploy";
    public static final String UPDATE_ACTION = "update";
    public static final String REMOVE_ACTION = "remove";

    public static final String PORTAL_SOFT_LINK = "PORTAL_SOFT_LINK";
    public static final String GRAILS_SOFT_LINK = "GRAILS_SOFT_LINK";

    public static final String DOWNLOAD_FROM_URL = "from_url";





}

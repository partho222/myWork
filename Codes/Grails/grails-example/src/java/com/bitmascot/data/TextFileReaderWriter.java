package com.bitmascot.data;



import java.io.*;


public class TextFileReaderWriter {

    public TextFileData textFileData = new TextFileData();

    public boolean isSuccess() {
        return textFileData.getSuccess();
    }

    public TextFileData fileToString(String location) {
        textFileData.setSuccess(false);
        File locationFile = new File(location);
        if (!locationFile.exists()) {
            textFileData.setErrorMessage("File Not Found");
            return textFileData;
        }

        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(locationFile));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.getProperty("line.separator"));
                textFileData.addLine(line);
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            textFileData.setErrorMessage("FileNotFoundException message: " + e.getMessage());
        } catch (IOException e) {
            textFileData.setErrorMessage("IOException message: " + e.getMessage());
        }
        textFileData.setText(stringBuilder.toString());
        textFileData.setTotalLine(textFileData.getLine().size());
        return textFileData;
    }

//
//    public String getLastLine() {
//        if (textFileData.getTotalLine() > 0) {
//            return textFileData.getLine(textFileData.getTotalLine() - 1);
//        } else {
//            return textFileData.getLine(0);
//        }
//    }
//
//    public String lastLineRemoveWhiteLine() {
//        for (int i = textFileData.getTotalLine() - 1; i > 0; i--) {
//            if (!textFileData.getLine(i).equals("")) {
//                return textFileData.getLine(i);
//            }
//        }
//        return null;
//    }
//
//    public GeneralResponseData addIndex(String instance) {
//        String location = ConfigLoader.getInstanceServerConfigLoc(ConfigLoader.getInstanceIndexName());
//        fileToString(location);
//        GeneralResponseData generalResponseData = new GeneralResponseData();
//        generalResponseData.setSuccess(false);
//        Integer index = 0;
//
//        String newLine = "";
//        String lastLine = getLastLine();
//        if (lastLine != null && lastLine.equals("")) {
//            lastLine = lastLineRemoveWhiteLine();
//        }
//        if (textFileData.getTotalLine() == 0 || lastLine == null) {
//            newLine = "0 " + instance;
//        } else {
//
//            lastLine = lastLine.trim();
//            String[] numInstance = lastLine.split(" +");
//            if (numInstance.length != 2) {
//                generalResponseData.setMessage("Last index length not match");
//                return generalResponseData;
//            }
//            index = Integer.parseInt(numInstance[0]);
//            if (index == null) {
//                generalResponseData.setMessage("Index Not found");
//                return generalResponseData;
//            } else {
//                index = index + 1;
//                newLine = index + " " + instance;
//            }
//        }
//
//        textFileData.addLine(newLine);
//        Boolean isSuccess = saveIndex();
//        if (isSuccess) {
//            generalResponseData.setSuccess(true);
//            generalResponseData.setIndex(index);
//        } else {
//            generalResponseData.setMessage("Can't Able to Save Index File");
//        }
//        return generalResponseData;
//    }
//
//    public boolean saveIndex() {
//        StringBuilder stringBuilder = new StringBuilder();
//        String line = "";
//        String currentLine = "";
//        for (int i = 0; i < textFileData.getLine().size(); i++) {
//            line = "";
//            currentLine = textFileData.getLine(i);
//            if (!currentLine.equals("")) {
//                currentLine = currentLine.trim();
//                String[] numInstance = currentLine.split(" +");
//                if (numInstance.length == 2) {
//                    line = numInstance[0] + " " + numInstance[1];
//                }
//                stringBuilder.append(line);
//                stringBuilder.append(System.getProperty("line.separator"));
//            }
//        }
//        FileDirectoryOperation fileDirectoryOperation = new FileDirectoryOperation();
//        return fileDirectoryOperation.export(stringBuilder.toString(), ConfigLoader.getInstanceIndexName(), ConfigLoader.getInstanceServerConfigLoc(""));
//    }

}

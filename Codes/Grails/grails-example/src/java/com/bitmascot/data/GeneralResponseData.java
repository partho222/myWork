package com.bitmascot.data;

/**
 * Created by touhid on 5/03/2016.
 */
public class GeneralResponseData {

    Boolean isSuccess;
    String message;
    Boolean isEnd;
    String comments;
    String port;
    Integer index;


    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public GeneralResponseData(Boolean isSuccess){
        this.isSuccess = isSuccess;
    }

    public GeneralResponseData(Boolean isSuccess, String message){
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public GeneralResponseData(){

    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getEnd() {
        return isEnd;
    }

    public void setEnd(Boolean end) {
        isEnd = end;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

import groovy.json.JsonSlurper

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.Statement

class Wc3VersionUpgrade {
    // srv3.cmdr.bitmascot.com/wc_c2d3f36f_cmdr_bitmascot_com?characterEncoding=utf8&autoReconnect=true
    // srv3.cmdr.bitmascot.com/wc_d31212dc_cmdr_bitmascot_com?characterEncoding=utf8&autoReconnect=true
    final static String DATABASE_SERVER = "srv3.cmdr.bitmascot.com"
    final static String DATABASE_NAME = "wc_0c1e90f5_cmdr_bitmascot_com"
    // sadmin
    final static String USER = "sadmin"
    // webcommander
    final static String PASSWORD = "webcommander"
    final static String FROM_VERSION = "2.2.0"
    final static String TO_VERSION = "3.0.0"
    final static String CODE_PATH = "C:\\Users\\tariq\\Desktop\\Projects\\grails\\extreme_v3_bit_bucket"
    final static List PLUGINS = ["abandoned-cart","loyalty-point","afterpay-payment-gateway","mobile-store","anything-slider","multi-level-pricing",
                                 "anz-payment-gateway","myob","ask-question","my-shopping","auto-product-load-on-scroll","nab-payment-gateway",
                                 "awaiting-payment-notification","news","awstats","order-custom-fields","blog","page-heading","braintree-payment-gateway",
                                 "payway-payment-gateway","cash-on-delivery","plugin-template","cdn","popup","comment-in-checkout","pos",
                                 "commweb-payment-gateway","product-asset-manager","compare-product","product-custom-fields","directone-payment-gateway",
                                 "product-custom-information","discount","product-image-swapping","discount-coupon","product-quick-view",
                                 "discount-per-product-variation","product-review","ebay-listing","quote","embedded-page","rackspace-files-cdn",
                                 "enterprise-variation","referboard","eparcel-order-export","reorder","epath-payment-gateway","rss-feed","event-management",
                                 "save-cart","eway-payment-gateway","section-slider","facebook","securepay-payment-gateway","filter","ship-bob","flash-widget",
                                 "shipment-calculator","form-editor","shop-by-brand","gallery-flip-book","simplified-event-management","gallery-galleriffic",
                                 "snippet","gallery-owl-carousel","standard-variation","general-event","star-track","get-price","stripe-payment-gateway",
                                 "gift-card","subscription-from-checkout","gift-certificate","swipe-box-slider","gift-registry","tab-accordion",
                                 "google-analytics-reporting","variation","google-map","visitor-listing","google-product","wish-list","google-trusted-store",
                                 "xero","jssor-slider","youtube","live-chat"]

    static def getResult(Connection connection, String sql) {
        Statement statement = connection.createStatement();
        Boolean executeResult = statement.execute(sql);
        if(executeResult) {
            ResultSet resultSet = statement.getResultSet()
            List<Map> list = []
            ResultSetMetaData metaData = resultSet.getMetaData();
            Integer noOfColumn = metaData.getColumnCount()
            while (resultSet.next()) {
                Map map = [:]
                for (int i = 1; i <= noOfColumn; i++) {
                    map.put(metaData.getColumnName(i), resultSet.getString(i))
                }
                list.add(map)
            }
            return list
        } else  {
            return statement.getUpdateCount()
        }

    }

    public static runSql(Connection connection, Map sql, String developer) {
        try {
            def result = getResult(connection, sql.condition)
            String performSql
            if(result instanceof List && result.size()) {
                performSql = sql.isTrue
            } else  {
                performSql = sql.isFalse
            }
            def performSqlResult
            if(performSql) {
                performSqlResult = getResult(connection, performSql)
            }
            println('-------------------------------------------')
            println("Status: Success")
            println("Condition: ${sql.condition}")
            println("Perform SQL: ${performSql}")
            println("Result: " +  performSqlResult)
            println('-------------------------------------------')
        } catch (Exception ex) {
            println('-------------------------------------------')
            println("Status: error")
            println("Developer: ${developer}")
            println(sql)
            println('-------------------------------------------')
        }


    }

    public static runSqlFile(Connection connection, File coreJSONFile) {
        JsonSlurper slurper = new JsonSlurper()
        try {
            if(coreJSONFile.exists()) {
                println("\n\n************************************************")
                println("Executing File ${coreJSONFile.absolutePath}")
                println("************************************************\n\n")
                Map coreJson = slurper.parse(coreJSONFile)
                coreJson.developers.each {Map map ->
                    map.sql.each {Map sql ->
                        runSql(connection, sql, map.name)
                    }
                }
            } else {
                println("${coreJSONFile.absolutePath} not exist")
            }
        }
        catch (Exception ex) {
            println("${coreJSONFile.absolutePath} execution failed")
        }

    }

    public static void main(String[] args) {
        Class.forName("com.mysql.jdbc.Driver")
        String url = "jdbc:mysql://${DATABASE_SERVER}/${DATABASE_NAME}?characterEncoding=utf8&autoReconnect=true"
        Connection connection = DriverManager.getConnection(url, USER, PASSWORD)
        File coreJSONFile = new File("${CODE_PATH}\\src\\main\\webapp\\WEB-INF\\sql\\update", "${TO_VERSION}.json")
        runSqlFile(connection, coreJSONFile)
        PLUGINS.each {
            runSqlFile(connection, new File("${CODE_PATH}\\wc-plugins\\${it}\\src\\main\\webapp\\WEB-INF\\sql\\update", "${TO_VERSION}.json"))

        }
    }
}

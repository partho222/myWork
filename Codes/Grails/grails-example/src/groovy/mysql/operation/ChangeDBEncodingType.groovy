package mysql.operation
/** Change character encoding type of a Database (all table) **/

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.Statement

class ChangeDBEncodingType {
    public static Connection connection
    final static String DATABASE_SERVER = "localhost"
    final static String DATABASE_NAME = "extreme_v3"
    final static String USER = "root"
    final static String PASSWORD = ""
    final static String charaterType = "utf8"
    final static String collateType = "utf8_general_ci"

    final static String AllTableName = 'SELECT `TABLE_NAME` FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA`="' + DATABASE_NAME + ';'
    // table of DB which character type need to be converted -
    final static String UpdateTableEncodingType = 'ALTER TABLE ' + DATABASE_NAME +
            '.XXXXX CONVERT TO CHARACTER SET ' + charaterType + ' COLLATE ' + collateType + ';'

    private static List<String> runQuery(String query) {
        Statement statement = connection.createStatement()
        List<String> list = []
        if(statement.execute(query)) {
            ResultSet resultSet = statement.getResultSet()
            ResultSetMetaData metaData = resultSet.getMetaData()
            Integer noOfColumn = metaData.getColumnCount()
            while (resultSet.next()) {
                for (int i = 1; i <= noOfColumn; i++) {
                    list.add(resultSet.getString(i))
                }
            }
        }
        return list
    }

    private static void execute() {
        List<String> tables = runQuery(AllTableName)
        tables.each { table ->
            String sql = UpdateTableEncodingType.replace("XXXXX", table)
            runQuery(sql)
        }
    }

    public static void main(String[] args) {
        Class.forName("com.mysql.jdbc.Driver")
        String url = "jdbc:mysql://${DATABASE_SERVER}/${DATABASE_NAME}?characterEncoding=utf8&autoReconnect=true"
        connection = DriverManager.getConnection(url, USER, PASSWORD)
        execute()
    }
}

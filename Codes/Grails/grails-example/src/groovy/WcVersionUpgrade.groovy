import groovy.json.JsonSlurper

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.Statement

class WcVersionUpgrade {
    // srv3.cmdr.bitmascot.com/wc_c2d3f36f_cmdr_bitmascot_com?characterEncoding=utf8&autoReconnect=true
    final static String DATABASE_SERVER = "localhost"
    final static String DATABASE_NAME = "webcommander"
    // sadmin
    final static String USER = "root"
    // webcommander
    final static String PASSWORD = ""
    final static String FROM_VERSION = "2.2.0.1"
    final static String TO_VERSION = "2.2.1"
    final static String CODE_PATH = "C:\\Users\\tariq\\Desktop\\Projects\\grails\\extreme2"
    final static List PLUGINS = ["abandoned-cart","afterpay-payment-gateway","anything-slider","anz-payment-gateway","ask-question","auto-product-load-on-scroll","awaiting-payment-notification",
                                 "awstats","blog","braintree-payment-gateway","cash-on-delivery","cdn","comment-in-checkout","commweb-payment-gateway","compare-product","directone-payment-gateway",
                                 "discount-coupon","ebay-listing","embedded-page","enterprise-variation","eparcel-order-export","epath-payment-gateway","event-management","eway-payment-gateway",
                                 "facebook","filter","flash-widget","form-editor","gallery-flip-book","gallery-galleriffic","gallery-owl-carousel","general-event","get-price","gift-certificate",
                                 "gift-registry","google-analytics-reporting","google-map","google-product","google-trusted-store","jssor-slider","live-chat","loyalty-point","mobile-store","myob",
                                 "my-shopping","nab-payment-gateway","news","order-custom-fields","page-heading","payway-payment-gateway","popup","pos","product-asset-manager","product-custom-fields",
                                 "product-custom-information","product-image-swapping","product-quick-view","product-review","quote","rackspace-files-cdn","reorder","rss-feed","save-cart","scripts",
                                 "section-slider","securepay-payment-gateway","shipment-calculator","shop-by-brand","simplified-event-management","snippet","standard-variation","star-track",
                                 "stripe-payment-gateway","subscription-from-checkout","swipe-box-slider","tab-accordion","variation","visitor-listing","wish-list","xero","youtube"]

    static def getResult(Connection connection, String sql) {
        Statement statement = connection.createStatement();
        Boolean executeResult = statement.execute(sql);
        if(executeResult) {
            ResultSet resultSet = statement.getResultSet()
            List<Map> list = []
            ResultSetMetaData metaData = resultSet.getMetaData();
            Integer noOfColumn = metaData.getColumnCount()
            while (resultSet.next()) {
                Map map = [:]
                for (int i = 1; i <= noOfColumn; i++) {
                    map.put(metaData.getColumnName(i), resultSet.getString(i))
                }
                list.add(map)
            }
            return list
        } else  {
            return statement.getUpdateCount()
        }

    }

    public static runSql(Connection connection, Map sql, String developer) {
        try {
            def result = getResult(connection, sql.condition)
            String performSql
            if(result instanceof List && result.size()) {
                performSql = sql.isTrue
            } else  {
                performSql = sql.isFalse
            }
            def performSqlResult
            if(performSql) {
                performSqlResult = getResult(connection, performSql)
            }
            println('-------------------------------------------')
            println("Status: Success")
            println("Condition: ${sql.condition}")
            println("Perform SQL: ${performSql}")
            println("Result: " +  performSqlResult)
            println('-------------------------------------------')
        } catch (Exception ex) {
            println('-------------------------------------------')
            println("Status: error")
            println("Developer: ${developer}")
            println(sql)
            println('-------------------------------------------')
        }


    }

    public static runSqlFile(Connection connection, File coreJSONFile) {
        JsonSlurper slurper = new JsonSlurper()
        try {
            if(coreJSONFile.exists()) {
                println("\n\n************************************************")
                println("Executing File ${coreJSONFile.absolutePath}")
                println("************************************************\n\n")
                Map coreJson = slurper.parse(coreJSONFile)
                coreJson.developers.each {Map map ->
                    map.sql.each {Map sql ->
                        runSql(connection, sql, map.name)
                    }
                }
            } else {
                println("${coreJSONFile.absolutePath} not exist")
            }
        }
        catch (Exception ex) {
            println("${coreJSONFile.absolutePath} execution failed")
        }

    }

    public static void main(String[] args) {
        Class.forName("com.mysql.jdbc.Driver")
        String url = "jdbc:mysql://${DATABASE_SERVER}/${DATABASE_NAME}?characterEncoding=utf8&autoReconnect=true"
        Connection connection = DriverManager.getConnection(url, USER, PASSWORD)
        File coreJSONFile = new File("${CODE_PATH}\\web-app\\WEB-INF\\sql\\update", "${TO_VERSION}.json")
        runSqlFile(connection, coreJSONFile)
        PLUGINS.each {
            runSqlFile(connection, new File("${CODE_PATH}\\ref\\plugins\\${it}\\web-app\\WEB-INF\\sql\\update", "${TO_VERSION}.json"))

        }
    }
}

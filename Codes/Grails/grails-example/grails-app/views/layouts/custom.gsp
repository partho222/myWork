<%--
  Created by IntelliJ IDEA.
  User: tariq
  Date: 11/13/2016
  Time: 3:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>File Browser</title>
    <g:render template="/layouts/includeFiles"/>
</head>

<body>
<g:layoutBody/>
<g:render template="/layouts/scripts"/>
</body>
</html>
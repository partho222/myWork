<asset:javascript src="jquery-3.1.1.js"/>
<asset:javascript src="jstree/jstree.min.js"/>
<asset:javascript src="bootstarp.min.js"/>
<asset:javascript src="jquery.matchHeight-min.js"/>
<g:javascript>
    function getFileExtension(file) {
        var extension = file.split(".");
        return extension[extension.length - 1];
    }

    $(function () {
        $(window).resize(function () {
            var h = Math.max($(window).height() - 0, 420);
            $('#container, #data, #tree, #data .content').height(h).filter('.default').css('lineHeight', h + 'px');
        }).resize();

        $('#tree')
                .jstree({
            'core': {
                'data': {
                    'url': '?operation=get_node',
                    'data': function (node) {
                        return {'id': node.id};
                    }
                },
                'check_callback': function (o, n, p, i, m) {
                    if (m && m.dnd && m.pos !== 'i') {
                        return false;
                    }
                    if (o === "move_node" || o === "copy_node") {
                        if (this.get_node(n).parent === this.get_node(p).id) {
                            return false;
                        }
                    }
                    return true;
                },
                'force_text': true,
                'themes': {
                    'responsive': false,
                    'variant': 'small',
                    'stripes': true
                }
            },
            'sort': function (a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            // ** Custom Contex Menu for Download - TAK **
            'contextmenu': {
                'items': function (node) {
                    if (node.icon === 'file') {
                        return {
                            "create": {
                                "separator_before": false,
                                "separator_after": true,
                                "_disabled": false,
                                "label": "Download",
                                "action": function (data) {
                                    var inst = $.jstree.reference(data.reference), obj = inst.get_node(data.reference);
                                    window.location = 'download?id=' + obj.id;
                                }
                            }
                        }
                    }
                }
            },
            'types': {
                'default': {'icon': 'folder'},
                'file': {
                    'valid_children': [], 'icon': 'file'
                }
            },
            'unique': {
                'duplicate': function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
        })
                //         .on('delete_node.jstree', function (e, data) {
                //     $.get('?operation=delete_node', {'id': data.node.id})
                //             .fail(function () {
                //         data.instance.refresh();
                //     });
                // })
                //         .on('create_node.jstree', function (e, data) {
                //     $.get('?operation=create_node', {'type': data.node.type, 'id': data.node.parent, 'text': data.node.text})
                //             .done(function (d) {
                //         data.instance.set_id(data.node, d.id);
                //     })
                //             .fail(function () {
                //         data.instance.refresh();
                //     });
                // })
                //         .on('rename_node.jstree', function (e, data) {
                //     $.get('?operation=rename_node', {'id': data.node.id, 'text': data.text})
                //             .done(function (d) {
                //         data.instance.set_id(data.node, d.id);
                //     })
                //             .fail(function () {
                //         data.instance.refresh();
                //     });
                // })
                //         .on('move_node.jstree', function (e, data) {
                //     $.get('?operation=move_node', {'id': data.node.id, 'parent': data.parent})
                //             .done(function (d) {
                //         //data.instance.load_node(data.parent);
                //         data.instance.refresh();
                //     })
                //             .fail(function () {
                //         data.instance.refresh();
                //     });
                // })
                //         .on('copy_node.jstree', function (e, data) {
                //     $.get('?operation=copy_node', {'id': data.original.id, 'parent': data.parent})
                //             .done(function (d) {
                //         //data.instance.load_node(data.parent);
                //         data.instance.refresh();
                //     })
                //             .fail(function () {
                //         data.instance.refresh();
                //     });
                // })
                .on('changed.jstree', function (e, data) {
            if (data && data.selected && data.selected.length) {

                // ** Custom File extension check - TAK **
                if (data.node.icon !== 'folder') {
                    switch (getFileExtension(data.node.id)) {
                        case 'text':
                        case 'txt':
                        case 'md':
                        case 'htaccess':
                        case 'log':
                        case 'sql':
                        case 'js':
                        case 'json':
                        case 'css':
                        case 'groovy':
                        case 'html':
                        case 'java':
                            $.get('?operation=get_content&id=' + data.selected.join(':'), function (d) {
                                $("#codes").text(d.content);
                                // $('#data .content').hide();
                                // $('#data .code').show();
                                // $('#code').val(d.content);
                                // $('#data .default').html(d.content).show();
                            });
                    }
                }
            }
            else {
                $('#data .content').hide();
                $('#data .default').html('Select a file from the tree.').show();
            }
        });
    });
</g:javascript>
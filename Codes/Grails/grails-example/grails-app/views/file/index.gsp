<meta name="layout" content="custom">

<div id="container container-fluid" role="main">
    <div class="row">
        <div class="col-md-2">
            <div class="list-group">
                <div id="tree"></div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    File Text
                </div>

                <div class="panel-body">
                    <div id="data">
                        <pre id="codes"></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
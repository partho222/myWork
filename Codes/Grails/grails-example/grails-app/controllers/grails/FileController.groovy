package grails

import com.bitmascot.data.TextFileData
import com.bitmascot.data.TextFileReaderWriter
import com.bitmascot.io.FileListForJSTree

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class FileController {

    String directoryROOT = "C:\\Users\\tariq\\Desktop\\myGit\\Codes\\Grails\\grails-example";

    def index() {
        getData(params.operation, params.id);
    }

    def getData(String operation, String id) {
        if (operation.equals("get_node")) {
            if (id.equals("#")) {
                render(FileListForJSTree.getInfo("") as JSON);
            } else {
                render(FileListForJSTree.getInfo(id) as JSON);
            }
        } else if (operation.equals("get_content")) {
            try {
                TextFileReaderWriter textFileReaderWriter = new TextFileReaderWriter();
                TextFileData textFileData = textFileReaderWriter.fileToString(FileListForJSTree.concatLocation(directoryROOT, id));
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", "text");
                jsonObject.put("content", textFileData.getText());
                render(jsonObject as JSON);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    def download() {
        String fileLocation = FileListForJSTree.concatLocation(directoryROOT, params.id);
        File file = new File(fileLocation);
        if (file.exists()) {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=" + file.name);
            response.outputStream << file.bytes
        } else {
            render "Error!"
        }
    }
}

package com.tariq

import org.apache.log4j.Logger

import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener
import javax.websocket.CloseReason
import javax.websocket.OnClose
import javax.websocket.OnError
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.ServerEndpoint

/**
 * Created by tariq on 17-Apr-17.
 */

@ServerEndpoint("/chatroom")
@WebListener
class WebSocketEndPoint implements ServletContextListener {
    private static final Logger log = Logger.getLogger(WebSocketEndPoint.class)
    static final Set<Session> users = ([] as Set).asSynchronized()

    @Override
    void contextInitialized(ServletContextEvent servletContextEvent) {}

    @Override
    void contextDestroyed(ServletContextEvent servletContextEvent) {
    }


    @OnOpen
    public void onOpen(Session userSession) {
        users.add(userSession)
    }


    @OnMessage
    public void onMessage(String message, Session userSession) {
        String username = userSession.userProperties.get("username")

        if (!username) {
            userSession.userProperties.put("username", message)
            sendMessage(String.format("%s has joined the chatroom.", message))
            return
        }

        sendMessage(message, userSession)
    }


    @OnClose
    public void onClose(Session userSession, CloseReason closeReason) {
        String username = userSession.userProperties.get("username")
        users.remove(userSession)
        userSession.close()

        if (username) {
            sendMessage(String.format("%s has left the chatroom.", username))
        }
    }

    @OnError
    public void onError(Throwable t) {
        log.error(t.message, t)
    }

    private void sendMessage(String message, Session userSession=null) {
        if (userSession) {
            message = String.format(
                    "%s: %s", userSession.userProperties.get("username"), message)
        }
        Iterator<Session> iterator = users.iterator()
        while(iterator.hasNext()) {
            iterator.next().basicRemote.sendText(message)
        }
    }
}

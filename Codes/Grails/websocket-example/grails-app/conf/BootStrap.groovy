import com.tariq.WebSocketEndPoint

import javax.websocket.server.ServerContainer

class BootStrap {

    def init = { servletContext ->
        ServerContainer serverContainer = servletContext.getAttribute("javax.websocket.server.ServerContainer")
        serverContainer.addEndpoint(WebSocketEndPoint)
    }
    def destroy = {
    }
}
